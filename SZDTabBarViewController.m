//
//  SZDTabBarViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-22.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDTabBarViewController.h"

#import "SZDPoopStore.h" 
#import "SZDPoopEntry.h"
#import "SZDPoopEditViewController.h"
#import "ThemeManager.h"

@interface SZDTabBarViewController ()

@property (nonatomic) CGFloat mainButtonRadius;
@property (nonatomic) CGFloat submenuButtonRadius;
@property (nonatomic) CGFloat submenuRadius;
//@property (nonatomic, strong) NSArray *buttonSubmenu;     // Array of Title, Image
@property (nonatomic, strong) NSArray *submenuTitles;
@property (nonatomic, strong) NSArray *submenuImages;
@property (nonatomic) CGFloat startAngle;
@property (nonatomic) CGFloat endAngle;
@property (nonatomic) BOOL buttonsOnEndpoints;

@property (nonatomic) UIButton *startButton;
@property (nonatomic) UIButton *stopButton;
@property (nonatomic) UIButton *cancelButton;

@property (nonatomic) NSInteger popButtonControlMenuState;
@property (nonatomic) NSDate *startDate; // hold the entry's start date

typedef NS_ENUM(NSInteger, popButtonControlMenuState)
{
    popButtonControlMenuStateIdle = 0,
    popButtonControlMenuStateStarted,
};

@end


@implementation SZDTabBarViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.restorationIdentifier = NSStringFromClass([self class]);
        self.restorationClass = [self class];
        
    }
    return self;
}

+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
    SZDTabBarViewController *vc = [[SZDTabBarViewController alloc] init];
    NSLog(@"Restoring SZDTabBarViewController: %@", vc);
    return vc;
}

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    NSLog(@"SZDTabBarViewController: encodeRestorableStateWithCoder.");
    [coder encodeObject:self.startDate forKey:@"startDate"];
    [coder encodeInteger:self.popButtonControlMenuState forKey:@"popButtonControlMenuState"];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    NSLog(@"SZDTabBarViewController: decodeRestorableStateWithCoder.");
    self.button = [coder decodeObjectForKey:@"button"];
    self.startDate = [coder decodeObjectForKey:@"startDate"];
    self.popButtonControlMenuState = [coder decodeIntegerForKey:@"popButtonControlMenuState"];
}


- (UINavigationController *)navigationControllerWithClass:(Class)vcClass
                                                    title:(NSString *)title
                                                    image:(UIImage *)image
{
    id vc = [[vcClass alloc] init];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    nc.tabBarItem.title = title;
    nc.tabBarItem.image = image;
    nc.restorationIdentifier =[NSString stringWithFormat:@"NavC%@", NSStringFromClass(vcClass)];
    
    
    UIColor *barTintColour = [ThemeManager themeColorNamed:@"barTintColour"];
    UIColor *barTextColour = [ThemeManager themeColorNamed:@"barTextColour"];
    UIColor *backgroundColour = [ThemeManager themeColorNamed:@"backgroundColour"];
    nc.navigationBar.barTintColor = barTintColour;
    nc.navigationBar.tintColor = barTextColour;
    nc.view.backgroundColor = backgroundColour;
    [nc.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : barTextColour}];
    
    
    
    return nc;
}

/*
// Create a custom UIButton and add it to the center of our tab bar
- (UIButton *) addCenterButtonWithImage:(UIImage *)mainButtonImage
                         mainButtonRadius:(CGFloat)mainButtonRadius
                      submenuButtonRadius:(CGFloat)submenuButtonRadius
                          submenuRadius:(CGFloat)submenuRadius  // hypotenuse from mainButton center
                          submenuTitles:(NSArray *)submenuTitles
                          submenuImages:(NSArray *)submenuImages
                    startAngleInRadians:(CGFloat)startAngle
                      endAngleInRadians:(CGFloat)endAngle
                     buttonsOnEndpoints:(BOOL)buttonsOnEndpoints
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    //button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    button.frame = CGRectMake(0.0, 0.0, mainButtonRadius * 2.0f, mainButtonRadius * 2.0f);
    //button.backgroundColor = [UIColor blueColor];
    [button setBackgroundImage:mainButtonImage forState:UIControlStateNormal];
    //[button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    // stick to bottom
    CGFloat tabBarBottom = self.tabBar.frame.origin.y + self.tabBar.frame.size.height;
    CGPoint buttonOrigin = CGPointMake(self.tabBar.center.x - (button.frame.size.width/2.0f), tabBarBottom - button.frame.size.height);
    button.frame = CGRectMake(buttonOrigin.x, buttonOrigin.y, mainButtonRadius * 2.0f, mainButtonRadius * 2.0f);
    
    
    [button addTarget:self
               action:@selector(buttonTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
        
    self.button = button;
    [self.view addSubview:button];
    
    _mainButtonRadius = mainButtonRadius;
    _submenuButtonRadius = submenuButtonRadius;
    _submenuRadius = submenuRadius;
    _submenuTitles = submenuTitles;
    _submenuImages = submenuImages;
    _startAngle = startAngle;
    _endAngle = endAngle;
    _buttonsOnEndpoints = buttonsOnEndpoints;
    
    [self popUpButtonsForSubmenuTitles:_submenuTitles
                         submenuImages:_submenuImages
                      mainButtonRadius:_mainButtonRadius
                   submenuButtonRadius:_submenuButtonRadius
                         submenuRadius:_submenuRadius
                   startAngleInRadians:_startAngle
                     endAngleInRadians:_endAngle
                    buttonsOnEndpoints:_buttonsOnEndpoints
                      mainButtonCenter:button.center];
    
    return button;
}
*/

/*
- (void)popUpButtonsForSubmenuTitles:(NSArray *)submenuTitles
                       submenuImages:(NSArray *)submenuImages
                      mainButtonRadius:(CGFloat)mainButtonRadius
                   submenuButtonRadius:(CGFloat)submenuButtonRadius
                       submenuRadius:(CGFloat)submenuRadius
                 startAngleInRadians:(CGFloat)startAngle
                   endAngleInRadians:(CGFloat)endAngle
                  buttonsOnEndpoints:(BOOL)buttonsOnEndpoints
                    mainButtonCenter:(CGPoint)mainButtonCenter
{
    NSLog(@"startAngle: %f endAngle: %f", _startAngle, _endAngle);
    

    
    CGFloat twoPI = 2.0f * M_PI;
    
    NSInteger buttonCount = [submenuTitles count];
    
    CGFloat menuAngle;
    if (startAngle > endAngle) {
        menuAngle = twoPI - (startAngle - endAngle);
    } else {
        menuAngle = endAngle - startAngle;
    }
    NSLog(@"menuAngle: %f %f", menuAngle, twoPI);
    
    NSInteger numberOfDivisions;
    
    // To detect a full circle - precision to the hundredths is enough
    float roundedMenuAngle = roundf(menuAngle * 100) / 100.0;
    float roundedTwoPi = roundf(twoPI * 100) / 100.0;
    
    if (roundedMenuAngle == roundedTwoPi) { // if a full circle, can't place last button on endpoint
        numberOfDivisions = buttonCount;
        NSLog(@"Full circle");
    } else {
        numberOfDivisions = buttonCount + 1;
    }
    
    CGFloat divisionAngle = menuAngle / numberOfDivisions;
    CGFloat workingAngle = startAngle;
    workingAngle = workingAngle + divisionAngle;
    
    
    for (NSInteger i = 1; i < buttonCount+1; i++) {
        CGFloat relativeButtonAngle = workingAngle;
        if (relativeButtonAngle >= twoPI) {
            relativeButtonAngle -= twoPI;
        }
        //NSLog(@"%lu", [self r2d:buttonAngle]);
        CGFloat qx = 1;
        CGFloat qy = 1;
        CGFloat x, y;
        
        // Determine quadrant
        if ((0 <= relativeButtonAngle && relativeButtonAngle <= M_PI_2)) {  // 0 to PI/2
            NSLog(@"Q1: %f", [self r2d:relativeButtonAngle]);
        } else if (M_PI_2 < relativeButtonAngle && relativeButtonAngle <= M_PI) { // PI/2 to PI
            relativeButtonAngle = M_PI - relativeButtonAngle;
            qx = -1;
            NSLog(@"Q2: %f", [self r2d:relativeButtonAngle]);
        } else if (M_PI < relativeButtonAngle && relativeButtonAngle < 3.0f*M_PI_2) {  // PI to 3PI/2
            relativeButtonAngle = relativeButtonAngle - M_PI;
            qx = -1;
            qy = -1;
            NSLog(@"Q3: %f", [self r2d:relativeButtonAngle]);
        } else if (3.0f*M_PI_2 <= relativeButtonAngle && relativeButtonAngle < twoPI) {  // 3PI/2 to 2PI
            relativeButtonAngle = twoPI - relativeButtonAngle;
            qy = -1;
            NSLog(@"Q4: %f", [self r2d:relativeButtonAngle]);
        }
        
        CGFloat a = submenuRadius * cosf(relativeButtonAngle);
        CGFloat b = submenuRadius * sinf(relativeButtonAngle);
        //NSLog(@"%f %f", cosf(buttonAngle), triangleHypotenuse);
        
        x = a * qx;
        y = b * qy;
        NSLog(@"a:%f b:%f x:%f y:%f", a, b, x, y);
        
        CGFloat buttonX = mainButtonCenter.x - submenuButtonRadius + x;
        CGFloat buttonY = mainButtonCenter.y - submenuButtonRadius - y;
        
        CGPoint buttonStartPoint = CGPointMake(mainButtonCenter.x - submenuButtonRadius, mainButtonCenter.y - submenuButtonRadius);
        //CGPoint buttonEndPoint = CGPointMake(buttonX, buttonY);
        
        //CGRect buttonStart = CGRectMake(buttonStartPoint.x, buttonStartPoint.y, submenuButtonRadius * 2.0f, submenuButtonRadius * 2.0f);
        CGRect buttonEnd = CGRectMake(buttonX, buttonY, submenuButtonRadius * 2.0f, submenuButtonRadius * 2.0f);
        
        
        UIButton *button = [[UIButton alloc] initWithFrame:buttonEnd];
        //button.backgroundColor = [UIColor greenColor];
        button.hidden = YES;
        UIImage *backgroundImage = [UIImage imageNamed:submenuImages[i-1]];
        [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        [button setTitle:submenuTitles[i-1] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        

        [button addTarget:self
                   action:@selector(popButtonTouchUpInside:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTag:i];
        [self.view addSubview:button];
        
        workingAngle = workingAngle + divisionAngle;
        
        if([button.titleLabel.text isEqualToString:@"Start"]) {
            _startButton = button;
        } else if ([button.titleLabel.text isEqualToString:@"Stop"]) {
            _stopButton = button;
            _stopButton.enabled = NO;
        } else if ([button.titleLabel.text isEqualToString:@"Cancel"]) {
            _cancelButton = button;
            _cancelButton.enabled = NO;
        }
        
    }
    if (_popButtonControlMenuState == popButtonControlMenuStateIdle) {
        NSLog(@"Idle");
        _startButton.enabled = YES;
        _stopButton.enabled = NO;
        _cancelButton.enabled = NO;
        [self.button setBackgroundImage:[UIImage imageNamed:@"PoopPopControl_start.png"] forState:UIControlStateNormal];
        
    } else if (_popButtonControlMenuState == popButtonControlMenuStateStarted) {
        NSLog(@"Started");
        _startButton.enabled = NO;
        _stopButton.enabled = YES;
        _cancelButton.enabled = YES;
        [self.button setBackgroundImage:[UIImage imageNamed:@"PoopPopControl_stop.png"] forState:UIControlStateNormal];
    }
}
*/

/*
- (void)buttonTouchUpInside:(id)sender
{
    
    UIButton *mainButton = sender;
    NSLog(@"Button Touched up Inside %d", self.button.selected);
    mainButton.selected = !mainButton.selected;
    
    
    for (UIView *subview in self.view.subviews) {
        if([subview isKindOfClass:[UIButton class]] && subview.tag >= 1) {
            
            UIButton *submenuButton = (UIButton *)subview;
            CGRect positionFrame = submenuButton.frame;
            
            if(mainButton.selected) {   // position and unhide buttons
                submenuButton.frame = CGRectMake(self.button.center.x - submenuButton.frame.size.width/2.0f,
                                                 self.button.center.y - submenuButton.frame.size.height/2.0f,
                                                 submenuButton.frame.size.width,
                                                 submenuButton.frame.size.height);
                submenuButton.hidden = NO;
                
                [UIView animateWithDuration:0.3f
                                      delay:0.f
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     submenuButton.frame = positionFrame;
                                     submenuButton.alpha = 1.0f;
                                 }
                                 completion:^(BOOL finished){
                                 }];
            } else {
                // collect buttons and hide
                [UIView animateWithDuration:0.3f
                                      delay:0.f
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     submenuButton.frame = CGRectMake(self.button.center.x - submenuButton.frame.size.width/2.0f,
                                                                      self.button.center.y - submenuButton.frame.size.height/2.0f,
                                                                      submenuButton.frame.size.width,
                                                                      submenuButton.frame.size.height);
                                     submenuButton.alpha = 0.0f;
                                 }
                                 completion:^(BOOL finished){
                                     submenuButton.hidden = YES;
                                     submenuButton.frame = positionFrame;
                                 }];
            }
            
        }
    }
}

- (void)popButtonTouchUpInside:(id)sender
{
    NSLog(@"Button Touched up Inside %d", self.button.selected);
    
    UIButton *button = (UIButton *)sender;

    NSLog(@"Button %lu touched: %@", (long)[sender tag], button.titleLabel.text);
    
    NSString *buttonTitle = button.titleLabel.text;
    
    // remember to disable buttons if something is or isnt in progress
    if([buttonTitle isEqualToString:@"Start"]) {
        // add new entry with current date
        // Show POOP IN PROGRESS somewhere at the top. do not add to table view 
        [self addEntry];
        _startButton.enabled = NO;
        _stopButton.enabled = YES;
        _cancelButton.enabled = YES;
        _popButtonControlMenuState = popButtonControlMenuStateStarted;
        [self.button setBackgroundImage:[UIImage imageNamed:@"PoopPopControl_stop.png"] forState:UIControlStateNormal];
    } else if ([buttonTitle isEqualToString:@"Stop"]) {
        // save new entry with endDate as current date
        // open up editVC
        [self stopEntry];
        _stopButton.enabled = NO;
        _startButton.enabled = YES;
        _cancelButton.enabled = NO;
        _popButtonControlMenuState = popButtonControlMenuStateIdle;
        [self.button setBackgroundImage:[UIImage imageNamed:@"PoopPopControl_start.png"] forState:UIControlStateNormal];
    } else if ([buttonTitle isEqualToString:@"Cancel"]) {
        // delete new entry
        [self cancelEntry];
        _startButton.enabled = YES;
        _stopButton.enabled = NO;
        _cancelButton.enabled = NO;
        _popButtonControlMenuState = popButtonControlMenuStateIdle;
        [self.button setBackgroundImage:[UIImage imageNamed:@"PoopPopControl_start.png"] forState:UIControlStateNormal];
    }
    
}

- (void)addEntry
{
    // record the start date only and add entry on stop
    self.startDate = [NSDate date];
    NSLog(@"Recorded start date %@", self.startDate); 
}

- (void)stopEntry
{
    // Create the new entry on stop
    SZDPoopEntry *newEntry = [[SZDPoopStore sharedStore] createEntry];
    newEntry.date = self.startDate;
    newEntry.endDate = [NSDate date];
    self.startDate = nil;   // we don't want to keep the date with the control anymore
    
    NSLog(@"Stopped and added new entry with date: %@ %@", newEntry.date, newEntry.endDate);
    
    // Let user choose whether or not this pops up. If not, just reload the table somehow
    SZDPoopEditViewController *stopEditViewController = [[SZDPoopEditViewController alloc] initWithEntry:newEntry isNewEntry:YES];
    stopEditViewController.dismissBlock = ^{
        //[self.tableView reloadData];
    };
    //self.entry = nil; // remove from control - it is in memory now
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:stopEditViewController];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)cancelEntry
{
    NSLog(@"Canceled with date: %@", self.startDate);
    self.startDate = nil;
}

- (CGFloat)r2d:(CGFloat)radian
{
    return (radian * 180.0) / M_PI;
}
 */
@end
