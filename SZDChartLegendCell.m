//
//  SZDChartLegendCell.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-23.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDChartLegendCell.h"

@implementation SZDChartLegendCell

- (void)awakeFromNib {
    // Initialization code
    self.colour.image = [self.colour.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
