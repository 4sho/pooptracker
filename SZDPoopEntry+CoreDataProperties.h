//
//  SZDPoopEntry+CoreDataProperties.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-16.
//  Copyright © 2016 sandzapps. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SZDPoopEntry.h"

NS_ASSUME_NONNULL_BEGIN

@interface SZDPoopEntry (CoreDataProperties)

@property (nullable, nonatomic, strong) NSDate *date;
@property (nullable, nonatomic, strong) NSDate *endDate;
@property (nonatomic) int32_t duration;

/*@property (nonatomic) int32_t type;
@property (nonatomic) int32_t colour;
@property (nonatomic) int32_t execution;
@property (nonatomic) int32_t amount;*/

@property (nonatomic) int32_t tpCount;

@property (nullable, nonatomic, strong) NSString *comments;

@property (nonatomic, strong) NSManagedObject *type;
@property (nonatomic, strong) NSManagedObject *colour;


@property (nonatomic, strong) NSManagedObject *exertion;
@property (nonatomic, strong) NSManagedObject *amount; 
@property (nonatomic, strong) NSManagedObject *smell;
@property (nonatomic, strong) NSManagedObject *fart;


@end

NS_ASSUME_NONNULL_END
