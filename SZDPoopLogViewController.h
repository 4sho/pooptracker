//
//  SZDPoopLogViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FSCalendar;

@interface SZDPoopLogViewController : UITableViewController <UIViewControllerRestoration>

@property (nonatomic, strong) FSCalendar *calendar; 

- (void)setUpTable:(NSArray *)sortedArrayByDate; 

@end
