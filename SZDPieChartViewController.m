//
//  SZDPieChartViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "SZDPieChartViewController.h"
#import "XYPieChart.h"
#import "SZDPoopStats.h"
#import "SZDPoopAttributes.h"
#import "SZDPoopStore.h"
#import "SZDStatsCollectionViewCell.h"
#import "ThemeManager.h"
#import "SZDChartLegendCell.h"

@interface SZDPieChartViewController ()


typedef NS_ENUM(NSInteger, SZDPieChartStateNone)
{
    SZDPieChartStateNoneDisabled = 0,
    SZDPieChartStateNoneEnabled
};
@property (nonatomic) NSInteger pieChartStateNone;

@property (nonatomic) NSInteger selectedAttribute;

@end

@implementation SZDPieChartViewController


- (void)viewDidAppear:(BOOL)animated
{
    //[self.pieChart reloadData];
    //[self.pieChartLegend reloadData];
    
    NSInteger totalNumberOfPoops = [[SZDPoopStats sharedStats] totalNumberOfEntries];
    self.totalPoopsLabel.text = [NSString stringWithFormat:@"Total: %ld Poops", (long)totalNumberOfPoops];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.pieChart setDelegate:self];
    [self.pieChart setDataSource:self];
    
    self.pieChart.selectedSliceOffsetRadius = 10;
    self.pieChart.selectedSliceStroke = 0;
    [self.pieChart setStartPieAngle:M_PI_2];	//optional
    [self.pieChart setAnimationSpeed:1.0];	//optional
    //[self.pieChart setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:24]];	//optional
    //[self.pieChart setLabelColor:[UIColor whiteColor]];	//optional, defaults to white
    //[self.pieChart setLabelShadowColor:[UIColor blackColor]];	//optional, defaults to none (nil)
    [self.pieChart setLabelRadius:60];	//optional
    [self.pieChart setShowPercentage:NO];	//optional

    [self.pieChart setPieBackgroundColor:[UIColor clearColor]];	//optional
    //[self.pieChart setPieCenter:CGPointMake(150, 150)];	//optional
    
    //[self.pieChart setPieCenter:CGPointMake(240, 240)];
     [self.pieChart setShowPercentage:NO];
     //[self.pieChart setLabelColor:[UIColor blackColor]];
    
    [self.pieChart setShowLabel:YES];
    
    NSLog(@"pieChart: %@", self.pieChart);
    
    self.attributeSelector.tag = 1; 
    self.attributeSelector.backgroundColor = [UIColor clearColor];
    self.attributeSelector.delegate = self;
    self.attributeSelector.dataSource = self;
    self.attributeSelector.allowsSelection = YES;
    self.attributeSelector.allowsMultipleSelection = NO;
    self.attributeSelector.tintColor = [UIColor lightGrayColor];
    
    NSIndexPath *first = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.attributeSelector selectItemAtIndexPath:first animated:NO
                                   scrollPosition:UICollectionViewScrollPositionNone];
    [self collectionView:self.attributeSelector didSelectItemAtIndexPath:first];
    
    

    UINib *nib = [UINib nibWithNibName:@"SZDStatsCollectionViewCell" bundle:nil];
    [self.attributeSelector registerNib:nib forCellWithReuseIdentifier:@"SZDStatsCollectionViewCell"];
    UINib *nib2 = [UINib nibWithNibName:@"SZDChartLegendCell" bundle:nil];
    [self.pieChartLegend registerNib:nib2 forCellWithReuseIdentifier:@"SZDChartLegendCell"];
    
    //[self setUpPieLegend];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.pieChartLegend.tag = 2;
    self.pieChartLegend.backgroundColor = [UIColor clearColor];
    self.pieChartLegend.delegate = self;
    self.pieChartLegend.dataSource = self;
    self.pieChartLegend.allowsSelection = YES;
    
    self.pieChartStateNone = SZDPieChartStateNoneDisabled;
    [self.toggleNoneButton setImage:[[UIImage imageNamed:@"toilet_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.toggleNoneButton.tintColor = [ThemeManager themeColorNamed:@"textColour"];
    [self.toggleNoneButton addTarget:self action:@selector(toggleNone) forControlEvents:UIControlEventTouchUpInside];
    self.noneLabel.textColor = [ThemeManager themeColorNamed:@"textColour"];

    self.totalPoopsLabel.textColor = [ThemeManager themeColorNamed:@"textColour"];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pieChart.pieCenter = CGPointMake(self.pieChart.frame.size.width * 0.5f, self.pieChart.frame.size.height * 0.5f);
}


- (void)toggleNone
{
    self.pieChartStateNone = !self.pieChartStateNone;
    if (self.pieChartStateNone == SZDPieChartStateNoneDisabled) {   // OFF
        [self.toggleNoneButton setImage:[[UIImage imageNamed:@"toilet_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    } else if (self.pieChartStateNone == SZDPieChartStateNoneEnabled) { // ON
        [self.toggleNoneButton setImage:[[UIImage imageNamed:@"toilet_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    [self.pieChart reloadData];
    [self.pieChartLegend reloadData];
}


/******************************************************************
 *  Pie Chart
 ******************************************************************/

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart {
    return [[[SZDPoopStats sharedStats] attributeCountsFor:self.selectedAttribute] count];
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index {
    
    NSArray *data = [[SZDPoopStats sharedStats] attributeCountsFor:self.selectedAttribute];
    CGFloat value = [data[index] floatValue];
    
    if (self.pieChartStateNone == SZDPieChartStateNoneDisabled && index == 0) {
        value = 0.0;
    }
    
    return value;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index {
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute];
    UIColor *colour = [[allTypes objectAtIndex:index] valueForKey:@"colour"];
    return colour;
}

- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index {
    // attribute's title
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute];
    NSString *title = [[allTypes objectAtIndex:index] valueForKey:@"title"];
    
    return title;
}

- (NSInteger)selectedAttribute
{
    NSInteger selectedAttribute = [[[self.attributeSelector indexPathsForSelectedItems] firstObject] row];
    return selectedAttribute;
}

- (void)setUpPieLegend
{
    NSString *legend;
    // get all the titles of the attribute
    NSDictionary *allTypes = [[SZDPoopAttributes sharedInstance] dictionaryForAttribute:self.selectedAttribute];

    
    for (NSInteger i = 0; i < [[allTypes allKeys] count]; i++) {
        NSArray *allObjects = [allTypes objectForKey:[NSNumber numberWithInteger:i]];
        NSString *title = [allObjects objectAtIndex:0];
        
        legend = [NSString stringWithFormat:@"%@ - %@",legend, title];
    }
    
    //self.pieLegend.text = legend;
    //self.pieLegend.textColor = [ThemeManager themeColorNamed:@"textColour"];
}

/******************************************************************
*  Attribute Selector
******************************************************************/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 1) {
        return SZDPoopAttributesCount;
    } else if (collectionView.tag == 2) {
        return [[[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute] count]; 
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    if (collectionView.tag == 1) {
        return 20.0;
    }
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView.tag == 1) {
        return UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0);
    }
    if (collectionView.tag == 2) {
        return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    }
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView.tag == 1) {
        CGSize size = CGSizeMake(40, 80);
        return size;
    }
    
    NSString *title = [self legendString:indexPath.row];
    NSString *percent = [self legendPercent:indexPath.row];
    NSString *titlePercent = [NSString stringWithFormat:@"%@%@", title, percent];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f]};
    CGSize titleSize = [titlePercent sizeWithAttributes:attributes];
    // |-4-[Colour=10]-4-[Label=titleSize.width]-4-[Image=25]-4]
    CGFloat imageWidth = 20;
    if (![self legendImage:indexPath.row]) {
        imageWidth = -4;
    }
    titleSize = CGSizeMake(4 + 10 + 4 + titleSize.width + 4 + imageWidth + 4, 22);
    
    return titleSize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 1) {  // attribute selector
        SZDStatsCollectionViewCell *cell = [self.attributeSelector dequeueReusableCellWithReuseIdentifier:@"SZDStatsCollectionViewCell" forIndexPath:indexPath];
        
        NSArray *attributeObjects = [[[SZDPoopAttributes sharedInstance] attributeDictionary] objectForKey:[NSNumber numberWithInteger:indexPath.row]];
        cell.topLabel.text = [attributeObjects objectAtIndex:SZDPoopAttributeDictionaryObjectTitle];
        
        for (id subview in [cell.centerView subviews]) {
            [subview removeFromSuperview];
        }
        UIImage *centerImage = [[attributeObjects objectAtIndex:SZDPoopAttributeDictionaryObjectIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *center = [[UIImageView alloc] initWithImage:centerImage];
        center.frame = CGRectMake(0, 0, 40, 40);
        /*if (cell.selected) {
            NSLog(@"tinting");
            center.tintColor = [UIColor brownColor];
        }*/
        //NSLog(@"tinty? %d", cell.selected);
        
        [cell.centerView addSubview:center];
        
        cell.bottomLabel.text = @"";
        return cell;
    } else if (collectionView.tag == 2) {   // legend
        SZDChartLegendCell *cell = [self.pieChartLegend dequeueReusableCellWithReuseIdentifier:@"SZDChartLegendCell" forIndexPath:indexPath];
        
        NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute];
        UIColor *colour = [[allTypes objectAtIndex:indexPath.row] valueForKey:@"colour"];
        cell.colour.tintColor = colour;
        
        // Content
        NSString *text = [self legendString:indexPath.row];
        NSString *percent = [self legendPercent:indexPath.row];
        
        cell.label.text = [NSString stringWithFormat:@"%@%@", text, percent];
        
        if ([text length] > 0) {    // label + (percent)
            cell.imageView.image = [self legendImage:indexPath.row];
        } else {                    // (percent) + image
            cell.imageView.image = [self legendImage:indexPath.row];
        }
        
        cell.label.textColor = [ThemeManager themeColorNamed:@"textColour"];
        
        // Grey out ones not in pie
        NSArray *allCounts = [[SZDPoopStats sharedStats] attributeCountsFor:self.selectedAttribute];
        if ([allCounts[indexPath.row] intValue] == 0 || (indexPath.row == 0 && self.pieChartStateNone == SZDPieChartStateNoneDisabled)) {
            cell.colour.tintColor = [UIColor lightGrayColor];
            cell.label.textColor = [UIColor lightGrayColor];
        }
        
        if (cell.selected ) {
            UIColor *color = cell.label.textColor;
            cell.label.layer.shadowColor = [color CGColor];
            cell.label.layer.shadowRadius = 3.0f;
            cell.label.layer.shadowOpacity = 0.5;
            cell.label.layer.shadowOffset = CGSizeZero;
            cell.label.layer.masksToBounds = NO;
        }
        return cell;
    }
    
    return nil; 
}

- (NSString *)legendPercent:(NSInteger)index
{
    // returns (XX%) or empty string
    
    NSString *percentString = @"";
    
    NSArray *counts = [[SZDPoopStats sharedStats] attributeCountsFor:self.selectedAttribute];
    NSInteger count = [[counts objectAtIndex:index] integerValue];
    
    CGFloat totalNumberOfPoops = [[SZDPoopStats sharedStats] totalNumberOfEntries];
    if (self.pieChartStateNone == SZDPieChartStateNoneDisabled) {
        totalNumberOfPoops -= [counts[0] floatValue];
    }
    
    CGFloat percentage = count / (float)totalNumberOfPoops * 100;
    if (percentage > 0 &&
        ((index == 0 && self.pieChartStateNone == SZDPieChartStateNoneEnabled) ||   // none
         (index != 0)))
    {  // all other types
        percentString = [NSString stringWithFormat:@" (%d%%)", (int)percentage];
    }
    
    return percentString;
}

- (NSString *)legendString:(NSInteger)index
{
    // returns the type title which could be empty string 
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute];
    NSString *legendString = [[allTypes objectAtIndex:index] valueForKey:@"title"];
    
    return legendString;
}

- (UIImage *)legendImage:(NSInteger)index
{
    // returns the type image or nil
    if (index == 0 || self.selectedAttribute == SZDPoopAttributesColour) {
        return nil;
    }
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.selectedAttribute];
    UIImage *legendImage = [[allTypes objectAtIndex:index] valueForKey:@"image"];
    
    return legendImage;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"selected %@", indexPath);
    
    if (collectionView.tag == 1) {  // attribute selector
        [self.pieChart reloadData];
        [self.pieChartLegend reloadData];
        
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        SZDStatsCollectionViewCell *mycell = (SZDStatsCollectionViewCell *)cell;
        UIImageView *iv = (UIImageView *)mycell.centerView;
        iv.tintColor = [ThemeManager themeColorNamed:@"textColour"];
        //NSLog(@"cell %@", cell);
    } else if (collectionView.tag == 2) {
        // select slice
        [self pieChart:self.pieChart willSelectSliceAtIndex:indexPath.row];
        [self.pieChart setSliceSelectedAtIndex:indexPath.row];
        
    }
    
}

- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
{
    // Deselect all slices first

    NSInteger numberOfSlicesInPieChart = [self numberOfSlicesInPieChart:self.pieChart];
    for (NSInteger i = 0; i < numberOfSlicesInPieChart; i++) {
        [self.pieChart setSliceDeselectedAtIndex:i];
    }
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 1) {  // attribute selector
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        SZDStatsCollectionViewCell *mycell = (SZDStatsCollectionViewCell *)cell;
        UIImageView *iv = (UIImageView *)mycell.centerView;
        iv.tintColor = [UIColor lightGrayColor];
        //NSLog(@"detinting, cellselected: %d", cell.selected);
    }
}




@end
