//
//  SZDPoopTableViewCell.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-22.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZDPoopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *typeColourIcon;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeColourLabel;
@property (weak, nonatomic) IBOutlet UILabel *tpCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *executionLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIView *dateWrapperView;

@end
