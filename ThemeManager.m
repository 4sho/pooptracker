//
//  ThemeManager.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-23.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "ThemeManager.h"

@implementation ThemeManager

+ (ThemeManager *)sharedManager
{
    static ThemeManager *sharedManager = nil;
    if (!sharedManager)
    {
        sharedManager = [[ThemeManager alloc] init];
    }
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     
        // theme
        NSString *themeName = [defaults objectForKey:@"theme"] ?: @"default";
        NSString *path = [[NSBundle mainBundle] pathForResource:themeName ofType:@"plist"];
        self.styles = [NSDictionary dictionaryWithContentsOfFile:path];
        
        // iconTheme
        NSString *iconThemeName = [defaults objectForKey:@"iconTheme"] ?: @"iconTheme_scribble";
        NSString *iconThemePath = [[NSBundle mainBundle] pathForResource:iconThemeName ofType:@"plist"];
        self.icons = [NSDictionary dictionaryWithContentsOfFile:iconThemePath];
        
    }
    return self;
}

+ (NSString *)getTheme
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"theme"];
}

+ (void)setTheme:(NSString *)themeName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:themeName forKey:@"theme"];
    NSLog(@"Setting theme to: %@", themeName);
}

+ (void)setIconTheme:(NSString *)iconThemeName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:iconThemeName forKey:@"iconTheme"];
    NSLog(@"Setting iconTheme to: %@", iconThemeName);
}

+ (UIColor *)themeColorNamed:(NSString *)key
{
    NSDictionary *styles = [[ThemeManager sharedManager] styles];
    NSString *colourString = [styles objectForKey:key];
    UIColor *colour = [ThemeManager colorFromHexString:colourString];
    return colour;
}

+ (UIImage *)iconNamed:(NSString *)key
{
    NSDictionary *icons = [[ThemeManager sharedManager] icons];
    UIImage *icon = [[UIImage imageNamed:[icons objectForKey:key]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return icon;
}

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    //[scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
