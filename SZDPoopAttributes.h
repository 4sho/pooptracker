//
//  SZDPoopAttributes.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SZDPoopAttributes : NSObject

typedef NS_ENUM(NSInteger, SZDPoopAttribute)
{
    SZDPoopAttributesType = 0,
    SZDPoopAttributesColour,
    SZDPoopAttributesExertion,
    SZDPoopAttributesAmount,
    SZDPoopAttributesSmell,
    SZDPoopAttributesFart,
    
    SZDPoopAttributesCount
    
};

typedef NS_ENUM(NSInteger, SZDPoopAttributeDictionaryObject)
{
    SZDPoopAttributeDictionaryObjectKey = 0,
    SZDPoopAttributeDictionaryObjectEntity,
    SZDPoopAttributeDictionaryObjectTitle,
    SZDPoopAttributeDictionaryObjectIcon,
    SZDPoopAttributeDictionaryObjectDescription
};

typedef NS_ENUM(NSInteger, SZDAttributeObjectType)
{
    SZDAttributeObjectTypeTitle = 0,
    SZDAttributeObjectTypeImage,
    SZDAttributeObjectTypeColour,
    SZDAttributeObjectTypeDescription, 
    SZDAttributeObjectTypeImageOutline 
};

@property (nonatomic, readonly, strong) NSDictionary *poopTypeDictionary;
@property (nonatomic, readonly, strong) NSDictionary *poopColourDictionary;
@property (nonatomic, readonly, strong) NSDictionary *poopExertionDictionary;
@property (nonatomic, readonly, strong) NSDictionary *poopAmountDictionary;
@property (nonatomic, readonly, strong) NSDictionary *poopSmellDictionary;
@property (nonatomic, readonly, strong) NSDictionary *poopFartDictionary;

@property (nonatomic, readonly, strong) NSDictionary *attributeDictionary;


- (NSDictionary *)dictionaryForAttribute:(NSInteger)attribute; 

+ (instancetype)sharedInstance; 

- (id)objectInAttributeDictionaryAtIndex:(NSInteger)objectIndex forAttribute:(NSInteger)attribute;

// Get all of the attribute kinds of a specific objectType (ie all images for poop type)
//- (NSArray *)objectsOfType:(NSInteger)objectType forAttribute:(NSInteger)attribute;

//- (NSArray *)objectsArrayForSegmentedIndex:(NSInteger)segmentedIndex forAttribute:(NSInteger)attribute;

@end
