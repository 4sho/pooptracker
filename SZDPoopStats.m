//
//  SZDPoopStats.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-04.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopStats.h"

#import "SZDPoopStore.h"
#import "SZDPoopAttributes.h"




@interface SZDPoopStats ()



@property (nonatomic, strong) SZDPoopStore *ps;

@property (nonatomic, strong) NSMutableDictionary *statsDictionary;

@end

@implementation SZDPoopStats

@synthesize ps = _ps;
@synthesize statsDictionary = _statsDictionary;

- (SZDPoopStore *)ps
{
    if (!_ps) {
        _ps = [SZDPoopStore sharedStore];
    }
    return _ps;
}

+ (instancetype)sharedStats
{
    static SZDPoopStats *sharedStats = nil;
    
    // Initialize the sharedStore once thread-safely.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStats = [[self alloc] initPrivate];
    });
    
    return sharedStats;
}
- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[SZDPoopStats]sharedStats!!!"
                                 userInfo:nil];
    return nil;
}
- (instancetype)initPrivate
{
    self = [super init];
    
    return self;
}


+ (NSArray *)statsDataForKey:(NSInteger)cellIndex
{
    SZDPoopStats *ps = [SZDPoopStats sharedStats];
    
    return [[ps statsDictionary] objectForKey:@(cellIndex)];
}

// Create a dictionary
// Each key is a cell
// Each value is an array of values for the cell

// Have a method for each cellgroup
// Have a method for each cell

// keep values until poopstore refreshes them
// have a refresh values method

- (void)refreshStatsDictionary
{
    _statsDictionary = nil;
}

- (NSMutableDictionary *)statsDictionary
{
    if (!_statsDictionary) {
        NSLog(@"statsDictionary refresh");
        
        NSMutableDictionary *statsDictionary = [[NSMutableDictionary alloc] init];
        
        [statsDictionary setObject:[self daysSinceLastPoopData] forKey:@(SZDStatsCellDaysSinceLastPoop)];
        [statsDictionary setObject:[self totalNumberData] forKey:@(SZDStatsCellTotalNumber)];
        [statsDictionary setObject:[self averagePerDayData] forKey:@(SZDStatsCellAveragePerDay)];
        [statsDictionary setObject:[self averageDaysBetweenData] forKey:@(SZDStatsCellAverageDaysBetween)];
        [statsDictionary setObject:[self totalTPData] forKey:@(SZDStatsCellTotalTP)];
        [statsDictionary setObject:[self averageTPData] forKey:@(SZDStatsCellAverageTP)];
        for (NSInteger attribute = SZDPoopAttributesType; attribute < SZDPoopAttributesCount; attribute++) {
            NSInteger key = SZDStatsCellMostCommonType + attribute;
            [statsDictionary setObject:[self mostCommonData:attribute] forKey:@(key)];
        }
        [statsDictionary setObject:[self mostCommonPartOfDayData] forKey:@(SZDStatsCellMostCommonPartOfDay)];
        
        _statsDictionary = statsDictionary;
    }
    
    return _statsDictionary;
}

- (NSArray *)daysSinceLastPoopData
{
    NSDate *newestEntryDate = [self newestEntryDate];
    // calculate difference in time between now and newest poop
    NSString *center = @"N/A";
    if (newestEntryDate) {
        NSInteger days = [self daysBetweenDate1:newestEntryDate andDate2:[NSDate date]];
        center = [NSString stringWithFormat:@"%ld days", (long)days];
    }
    
    NSString *top = @"Days since last poop";
    NSString *bottom = @"";
    return @[top, center, bottom];
}

- (NSArray *)totalNumberData
{
    NSInteger totalNumberOfEntries = [self totalNumberOfEntries];
    NSDate *oldestEntryDate = [self oldestEntryDate];
    
    NSString *top = @"Total number of poops";
    NSString *center = [NSString stringWithFormat:@"%ld", (long)totalNumberOfEntries];
    
    NSString *bottom = @"";
    if (oldestEntryDate) {
        bottom = [NSString stringWithFormat:@"since %@",
                            [NSDateFormatter localizedStringFromDate:oldestEntryDate
                                                           dateStyle:NSDateFormatterMediumStyle
                                                           timeStyle:NSDateFormatterNoStyle]];
    }
    return @[top, center, bottom];
}

- (NSArray *)averagePerDayData
{
    NSInteger totalNumberOfEntries = [self totalNumberOfEntries];
    CGFloat averagePerDay = 0;
    
    if(totalNumberOfEntries > 0) {
        NSDate *oldestEntryDate = [self oldestEntryDate];
        NSDate *now = [NSDate date];
        
        // nmber of poops / days from oldest entry to now
        NSInteger daysSinceOldestEntry = [self daysBetweenDate1:oldestEntryDate andDate2:now];
        
        if (daysSinceOldestEntry == 0)  {
            daysSinceOldestEntry = 1;
        }
        averagePerDay = (CGFloat)totalNumberOfEntries / (CGFloat)daysSinceOldestEntry;
    }
    
    NSString *top = @"On average,";
    NSString *center = [NSString stringWithFormat:@"%.2f", averagePerDay];
    NSString *bottom = @"poops per day";
    
    return @[top, center, bottom];
}

- (NSArray *)averageDaysBetweenData
{
    CGFloat averageDaysBetween;
    // number of days from first to last / number of poops - 1
    NSInteger totalNumberOfEntries = [self totalNumberOfEntries];
    
    if (totalNumberOfEntries > 0) {
        NSDate *oldestEntryDate = [self oldestEntryDate];
        NSDate *newestEntryDate = [self newestEntryDate];
        
        NSInteger daysBetweenOldestAndNewest = [self daysBetweenDate1:oldestEntryDate andDate2:newestEntryDate];
        if (totalNumberOfEntries == 0) {
            totalNumberOfEntries = 1;
        }
        averageDaysBetween = (CGFloat)daysBetweenOldestAndNewest / (CGFloat)totalNumberOfEntries;
    }
    
    NSString *top = @"On average, ";
    NSString *center = [NSString stringWithFormat:@"%.2f", averageDaysBetween];
    NSString *bottom = @"days between poops";
    
    return @[top, center, bottom];
}

- (NSArray *)totalTPData
{
    NSInteger totalTP = [self totalTPCount];
    NSInteger validTPEntryCount = [self validTPEntryCount];
    
    NSString *top = @"Amount of Toilet Paper Used:";
    NSString *center = [NSString stringWithFormat:@"%ld squares", (long)totalTP];
    NSString *bottom = [NSString stringWithFormat:@"Across %ld Poop", (long)validTPEntryCount];
    if (validTPEntryCount > 0) {
        bottom = [NSString stringWithFormat:@"%@s", bottom];
    }
    
    return @[top, center, bottom];
}

- (NSArray *)averageTPData
{
    NSInteger totalTP = [self totalTPCount];
    NSInteger validTPEntryCount = [self validTPEntryCount];
    
    if (validTPEntryCount == 0) {
        validTPEntryCount = 1;
    }
    CGFloat averageTP = totalTP / validTPEntryCount;
    
    NSString *top = @"You Usually Use";
    NSString *center = [NSString stringWithFormat:@"%.0f squares", averageTP];
    NSString *bottom = @"Per Poop";
    
    return @[top, center, bottom];
}

- (NSArray *)mostCommonPartOfDayData
{
    NSDictionary *mostCommonPartOfDay = [self mostCommonPartOfDay];
    
    if (mostCommonPartOfDay) {
        NSString *title = [mostCommonPartOfDay objectForKey:@"mostCommonPartOfDay"];
        NSInteger percentage = [[mostCommonPartOfDay objectForKey:@"percentage"] integerValue];
        
        NSString *top = @"You usually poop during the";
        
        NSString *center = [NSString stringWithFormat:@"%@", title];
        NSString *bottom = [NSString stringWithFormat:@"%ld%% of the time", (long)percentage];
        
        return @[top, center, bottom];
    } else {
        
        NSString *top = @"Poop more to see what part of the day you poop most!";
        return @[top, @"a", @"a"];
    }
    
}

- (NSDictionary *)mostCommonPartOfDay
{
    if ([self totalNumberOfEntries] == 0) {
        return nil;
    }
    
    NSDictionary *partsOfDay = [self.ps statForKey:SZDStatPartsOfDay];
    
    NSString *mostCommonPartOfDay;
    NSInteger mostCommonPartOfDayCount = 0;
    
    for (NSString *key in partsOfDay) {
        NSInteger partOfDayCount = [[partsOfDay objectForKey:key] integerValue];
        
        if (!mostCommonPartOfDay) {
            mostCommonPartOfDay = key;
            mostCommonPartOfDayCount = partOfDayCount;
        }
        
        if (partOfDayCount > mostCommonPartOfDayCount) {
            mostCommonPartOfDay = key;
            mostCommonPartOfDayCount = partOfDayCount;
        }
    }
    
    // percentage of poops
    NSInteger percentage = ((CGFloat)mostCommonPartOfDayCount / (CGFloat)[self totalNumberOfEntries]) * 100.0;
    
    return @{@"mostCommonPartOfDay" : mostCommonPartOfDay,
             @"percentage" : @(percentage)};
    
}

- (NSInteger)daysBetweenDate1:(NSDate *)date1 andDate2:(NSDate *)date2
{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *components = [c components:NSDayCalendarUnit fromDate:date1 toDate:date2 options:0];
    NSInteger diff = components.day;
    return diff;
}

- (NSInteger)totalNumberOfEntries
{
    NSNumber *totalNumberOfEntries = [self.ps statForKey:SZDStatTotalNumberOfEntries];
    return [totalNumberOfEntries integerValue];
}

- (NSDate *)oldestEntryDate
{
    NSDate *oldestEntryDate = [self.ps statForKey:SZDStatOldestEntryDate];
    return oldestEntryDate;
}

- (NSDate *)newestEntryDate
{
    NSDate *newestEntryDate = [self.ps statForKey:SZDStatNewestEntryDate];
    return newestEntryDate;
}

- (NSInteger)totalTPCount
{
    NSNumber *totalTPCount = [self.ps statForKey:SZDStatTotalTPCount];
    return [totalTPCount integerValue];
}

- (NSInteger)validTPEntryCount
{
    NSNumber *validTPEntryCount = [self.ps statForKey:SZDStatValidTPEntryCount];
    return [validTPEntryCount integerValue];
}

- (NSArray *)mostCommonData:(NSInteger)attribute
{
    NSDictionary *mostCommon = [self mostCommon:attribute];
    NSString *title = [[SZDPoopAttributes sharedInstance] objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectTitle forAttribute:attribute];
    
    NSString *top = [NSString stringWithFormat:@"Top %@", title];
    NSString *bottom = @"";
    id center;
    UIColor *colour;
    
    if (mostCommon) {
        NSInteger type = [[mostCommon valueForKey:@"type"] integerValue];
        NSInteger percentage = [[mostCommon valueForKey:@"percentage"] integerValue];
        bottom = [NSString stringWithFormat:@"%ld%% of Poops", (long)percentage];
        
        center = [[[[SZDPoopStore sharedStore] allTypesForAttribute:attribute] objectAtIndex:type] valueForKey:@"image"];
        
        if (attribute == SZDPoopAttributesColour) {
            colour = [[[[SZDPoopStore sharedStore] allTypesForAttribute:attribute] objectAtIndex:type] valueForKey:@"colour"];
        }
    } else {
        center = @"N/A";
    }
    
    if (colour) {
        return @[top, center, bottom, colour];
    } else {
        return @[top, center, bottom];
    }
}

- (NSDictionary *)mostCommon:(NSInteger)attribute
{
    NSArray *counts = [self attributeCountsFor:attribute];
    NSInteger totalNumberOfEntries = [self totalNumberOfEntries] - [counts[0] intValue];
    if (totalNumberOfEntries == 0) {
        return nil;
    }
    NSInteger mostCommon = 1;
    NSInteger mostCommonCount = [counts[1] integerValue];
    
    for (NSInteger i = 1; i < [counts count]; i++) {
        NSInteger typeCount = [counts[i] integerValue];
        
        if (typeCount > mostCommonCount) {  // check if max
            mostCommon = i;
            mostCommonCount = typeCount;
        }
    }
    NSInteger percentage = (CGFloat)mostCommonCount / (CGFloat)totalNumberOfEntries * 100.0;
    
    NSDictionary *values = @{@"type" : @(mostCommon), @"percentage" : @(percentage)};
    return values;
}

- (NSArray *)attributeCountsFor:(NSInteger)attribute
{
    return [[SZDPoopStore sharedStore] attributeCount:attribute];
}

/*

- (NSArray *)statsCollection
{
    
    
    // Total # of poops: Total # of poops | # | since: date
    NSInteger totalNumberOfPoopsInt = [[[SZDPoopStore sharedStore] allEntries] count];
    NSString totalNumberOfPoops = [NSString stringWithFormat:@"%lu", (long)totalNumberOfPoopsInt];
    
    // TYPE: Most pooped type | image | X% of the time
    NSString mostPoopedType =
    // COLOUR: Most pooped colour | coloured text | X % of the time
    // TYPE/COLOR Combination: Most pooped poop | image | X% of the time
    // EXECUTION: Execution scale: | # | X% of the time
    // AMOUNT: Amount scale: | # | X% of the time
    // Average TP Count: Average TP | # | across X poops
    // Average # of poops: Average # of poops | # | day/week/month/year
    //if (!_statsCollection) {
    SZDPoopStore *ps = [SZDPoopStore sharedStore];
    
    NSLog(@"total: %@, first entry: %@, last entry: %@, totaltpcount: %@, validtpentries: %@, mintime: %@, maxtime: %@, parts of day: %@" ,
          [ps statForKey:SZDStatTotalNumberOfEntries],
          [ps statForKey:SZDStatOldestEntryDate],
          [ps statForKey:SZDStatNewestEntryDate],
          [ps statForKey:SZDStatTotalTPCount],
          [ps statForKey:SZDStatValidTPEntryCount],
          [ps statForKey:SZDStatMinTimeBetween],
          [ps statForKey:SZDStatMaxTimeBetween],
          [ps statForKey:SZDStatPartsOfDay]);
    
    
    //NSMutableArray *statsCollection = [[NSMutableArray alloc] init];
    
    // TOTAL NUMBER OF POOPS
    NSString *totalNumberOfPoopsTop = @"Total # of Poops";
    NSString *totalNumberOfPoopsCenter = [NSString stringWithFormat:@"%ld", (long)self.totalNumberOfPoops];
    NSString *totalNumberOfPoopsBottom = [NSString stringWithFormat:@"Since %@",
                                          [NSDateFormatter localizedStringFromDate:self.trackingSince
                                                                         dateStyle:NSDateFormatterMediumStyle
                                                                         timeStyle:NSDateFormatterNoStyle]];
    NSArray *totalNumberOfPoops = @[totalNumberOfPoopsTop, totalNumberOfPoopsCenter, totalNumberOfPoopsBottom];
    [statsCollection addObject:totalNumberOfPoops];
    
    // AVERAGE NUMBER OF POOPS
    NSString *avgNumberOfPoopsTop = @"Avg # of Poops";
    NSString *avgNumberOfPoopsCenter = [NSString stringWithFormat:@"%d", (int)self.averagePoopsPerDay];
    NSString *avgNumberOfPoopsBottom = @"Per Day";
    NSArray *avgNumberOfPoops = @[avgNumberOfPoopsTop, avgNumberOfPoopsCenter, avgNumberOfPoopsBottom];
    [statsCollection addObject:avgNumberOfPoops];
    
    // AVERAGE TP COUNT
    NSString *tpAvgTop = @"Average TP Squares";
    NSString *tpAvgCenter = [NSString stringWithFormat:@"%d", (int)self.tpCountAverage];
    NSString *tpAvgBottom = @"Across X Poops";
    NSArray *tpAvg = @[tpAvgTop, tpAvgCenter, tpAvgBottom];
    [statsCollection addObject:tpAvg];
    
    // MOST FREQUENT ATTRIBUTE TYPES
    for (int i = 0; i < SZDPoopAttributesCount; i++) {
        NSArray *mostFrequentData = [self mostFrequent:i];
        
        NSString *attribute = [[[[SZDPoopAttributes sharedInstance] attributeDictionary] objectForKey:[NSNumber numberWithInt:i]] objectAtIndex:SZDPoopAttributeDictionaryObjectTitle];
        
        NSString *top = [NSString stringWithFormat:@"%@", attribute];
        NSString *bottom = [NSString stringWithFormat:@"(%d%% of Poops)", [mostFrequentData[1] intValue]];
        
        // find the type object with index
        NSNumber *mostFrequentIndex = mostFrequentData[0];
        
        NSDictionary *attributeDictionary = [[SZDPoopAttributes sharedInstance] dictionaryForAttribute:i];
        NSArray *attributeTypeArray = [attributeDictionary objectForKey:[NSNumber numberWithInt:[mostFrequentIndex intValue]]];
        UIImage *mostFrequentCenter = [attributeTypeArray objectAtIndex:SZDAttributeObjectTypeImage];
        
        NSArray *mostFrequent = @[top, mostFrequentCenter, bottom];
        
        [statsCollection addObject:mostFrequent];
    }
    
    _statsCollection = statsCollection;
    //}
    
    return _statsCollection;
}*/


@end
