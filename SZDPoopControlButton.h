//
//  SZDPoopControlButton.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-14.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPopCircleFloatingButton.h"

@class SZDPoopLogViewController;

@interface SZDPoopControlButton : SZDPopCircleFloatingButton

@property (nonatomic, strong) SZDPoopLogViewController *logVC;

@end
