//
//  SZDPoopCalendarViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-22.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopCalendarViewController.h"
#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "SZDPoopAttributes.h"
#import "SZDPoopLogTableViewCell.h"
#import "SZDPoopEditViewController.h"
#import "ThemeManager.h"
#import "SZDBarButtonItemAdd.h"


@interface SZDPoopCalendarViewController ()

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSArray *dayEntries;

@property (nonatomic, strong) NSDictionary *allEntriesByDate;

@end

@implementation SZDPoopCalendarViewController
@synthesize dayEntries = _dayEntries;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.navigationItem.title = @"Calendar";
        
        self.view.backgroundColor = [UIColor clearColor];
        
        SZDBarButtonItemAdd *add = [[SZDBarButtonItemAdd alloc] initWithViewController:self logViewController:self.logVC];
        self.navigationItem.rightBarButtonItem = add;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.calendar reloadData];
    [self.dayTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    UINib *nib = [UINib nibWithNibName:@"SZDPoopLogTableViewCell"
                                bundle:nil];
    [self.dayTable registerNib:nib
         forCellReuseIdentifier:@"SZDPoopLogTableViewCell"];
    
    self.calendar.dataSource = self;
    self.calendar.delegate = self;
    
    self.calendar.appearance.cellShape = FSCalendarCellShapeRectangle;
    self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0;
    self.calendar.clipsToBounds = YES;
    
    self.dayTable.dataSource = self;
    self.dayTable.delegate = self;
    
    self.dayTable.separatorColor = [ThemeManager themeColorNamed:@"tableSeparatorColour"];
    self.dayTable.backgroundColor = [UIColor clearColor];

    
    FSCalendar *calendar = self.calendar;
    
    UIColor *headerTextColour = [ThemeManager themeColorNamed:@"headerTextColour"];
    //UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    UIColor *calendarTodayColour = [ThemeManager themeColorNamed:@"calendarTodayColour"];
    UIColor *calendarSelectionColour = [ThemeManager themeColorNamed:@"calendarSelectionColour"];
    UIColor *calendarSelectionTextColour = [ThemeManager themeColorNamed:@"calendarSelectionTextColour"];
    
    calendar.backgroundColor = [UIColor clearColor];
    
    calendar.appearance.headerTitleColor = headerTextColour;
    calendar.appearance.weekdayTextColor = headerTextColour;
    
    calendar.appearance.titleDefaultColor = headerTextColour;   // days
    
    calendar.appearance.titleTodayColor = calendarSelectionTextColour; // today
    calendar.appearance.todayColor = calendarTodayColour;   // today box
    
    calendar.appearance.titleSelectionColor = calendarSelectionTextColour;
    calendar.appearance.selectionColor = calendarSelectionColour;
    calendar.appearance.borderSelectionColor = calendarSelectionTextColour;
}


- (NSDictionary *)allEntriesByDate
{
    // need to optimize this. this gets all entries by date EVERY single time it is called
    //if (!_allEntriesByDate) {
        _allEntriesByDate = [[SZDPoopStore sharedStore] allEntriesByDate];
    //}
    return _allEntriesByDate;
}

- (NSArray *)entriesForDay:(NSDate *)date
{
    NSDate *day = [[SZDPoopStore sharedStore] dateAtBeginningOfDayForDate:date];
    NSArray *dayEntries = [self.allEntriesByDate objectForKey:day];
    return dayEntries;
}

- (NSDate *)selectedDate
{
    if (!_selectedDate) {
        _selectedDate = self.calendar.today;
    }
    if (self.calendar.selectedDate) {
        _selectedDate = self.calendar.selectedDate;
    }
    return _selectedDate;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    [self.dayTable reloadData];
}

// Dot subtitle
- (BOOL)calendar:(FSCalendar *)calendar hasEventForDate:(NSDate *)date
{
    return NO;
}

- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date
{
    return @"";
}

// image subtitle
- (UIImage *)calendar:(FSCalendar *)calendar imageForDate:(NSDate *)date
{
    NSArray *poopEntries = [self entriesForDay:date];
    
    if (!poopEntries) {
        return nil;
    }
    
    SZDPoopEntry *firstEntry = [poopEntries firstObject];
    
    // get icon, outline, colour
    UIImage *icon = [firstEntry.type valueForKey:@"image"];
    UIImage *outline = [[[[SZDPoopAttributes sharedInstance]
                                   dictionaryForAttribute:SZDPoopAttributesType]
                                  objectForKey:[firstEntry.type valueForKey:@"index"]]
                                 objectAtIndex:SZDAttributeObjectTypeImageOutline];
    UIColor *colour = [firstEntry.colour valueForKey:@"colour"];
    UIImage *iconWithOutline = [self resizeIcon:icon withOutline:outline andColor:colour to:CGSizeMake(16,16) andShiftDown:7.0f];
    
    return iconWithOutline;
    
}

- (UIImage *)resizeIcon:(UIImage *)icon
            withOutline:(UIImage *)outline
               andColor:(UIColor *)colour
                     to:(CGSize)newSize
           andShiftDown:(CGFloat)shiftDown
{
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGSize canvas = CGSizeMake(newSize.width, newSize.height + shiftDown);
    CGRect rect = CGRectMake(0, 0, newSize.width, newSize.height);
    UIGraphicsBeginImageContextWithOptions(canvas, NO, scale);
    
    // image
    [icon drawInRect:rect];
    
    // tint
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(context, kCGBlendModeSourceAtop);
    [colour setFill];
    CGContextFillRect(context, rect);
    
    // outline
    [outline drawInRect:rect];
    
    UIImage *newIcon = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newIcon;
}

- (UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    
    CGSize extendedSize = CGSizeMake(newSize.width, newSize.height + 7.0);  // extend to fit inside the rectangle
    
    UIGraphicsBeginImageContextWithOptions(extendedSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    [img drawInRect:CGRectMake(5,5, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
    
}

- (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfEntries = [[self entriesForDay:self.selectedDate] count];
    
    if (numberOfEntries == 0) {
        numberOfEntries = 1;
    }
    return numberOfEntries;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 75.0;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:self.selectedDate dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];
    dateString = [NSString stringWithFormat:@"%@ Poops", dateString];
    
    return dateString;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    view.tintColor = [ThemeManager themeColorNamed:@"subBarTintColour"];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[ThemeManager themeColorNamed:@"subBarTextColour"]];
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *entriesForDay = [self entriesForDay:self.selectedDate];
    if ([entriesForDay count] == 0) {
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        cell.textLabel.text = @"No Poops :(";
        cell.textLabel.textColor = [ThemeManager themeColorNamed:@"textColour"];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    SZDPoopLogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SZDPoopLogTableViewCell"
                                                                    forIndexPath:indexPath];
    
    SZDPoopEntry *entry = [entriesForDay objectAtIndex:indexPath.row];
    
    [cell setUpCellWithEntry:entry];
    
    // extra setup
    cell.dateLabel.text = [NSDateFormatter localizedStringFromDate:entry.date
                                                         dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
    return cell;
}

// When a row is selected
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    /*NSArray *entriesForDay = [self entriesForDay:self.selectedDate];
    if ([entriesForDay count] == 0) {
        return;
    }
    
    SZDPoopEntry *entry = [entriesForDay objectAtIndex:indexPath.row];
    
    SZDPoopEditViewController *editViewController = [[SZDPoopEditViewController alloc] initWithEntry:entry isNewEntry:NO];
    editViewController.dismissBlock = ^{
        _allEntriesByDate = nil;    // reset the dictionary for date changes
        // add something here to only refetch dictionary if the date changes 
        [self.dayTable reloadData];
    };
    UINavigationController *editNavController = [[UINavigationController alloc] initWithRootViewController:editViewController];
    [self presentViewController:editNavController animated:YES completion:nil];
    */
}



@end
