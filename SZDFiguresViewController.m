//
//  SZDFiguresViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDFiguresViewController.h"
#import "SZDPoopStats.h"
#import "SZDStatsCollectionViewCell.h"
#import "ThemeManager.h"
#import "SZDPoopAttributes.h"
#import "RFQuiltLayout.h"

@interface SZDFiguresViewController ()

@end

@implementation SZDFiguresViewController
@synthesize figuresCollectionView = _figuresCollectionView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.figuresCollectionView];
    
    //UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*) self.figuresCollectionView.collectionViewLayout;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self.figuresCollectionView reloadData];
}

/*******************************************************
 *  Figures Collection View
 *******************************************************/
- (UICollectionView *)figuresCollectionView
{
    if (!_figuresCollectionView) {
        /*RFQuiltLayout *layout= [[RFQuiltLayout alloc] init];
        layout.direction = UICollectionViewScrollDirectionVertical;
        //layout.blockPixels = CGSizeMake(100, 100);*/
        //layout.delegate = self;
        RFQuiltLayout *layout = [[RFQuiltLayout alloc] init];
        layout.delegate = self;
        layout.direction = UICollectionViewScrollDirectionVertical;
        layout.blockPixels = CGSizeMake(self.view.frame.size.width / 4, 79);
        
        UICollectionView *figuresCollectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
        
        UINib *nib = [UINib nibWithNibName:@"SZDStatsCollectionViewCell" bundle:nil];
        [figuresCollectionView registerNib:nib
                forCellWithReuseIdentifier:@"SZDStatsCollectionViewCell"];
        
        [figuresCollectionView setDataSource:self];
        [figuresCollectionView setDelegate:self];
        
        figuresCollectionView.backgroundColor = [UIColor clearColor];
        figuresCollectionView.backgroundColor = [ThemeManager themeColorNamed:@"textColour"];
        
        figuresCollectionView.frame = CGRectMake(0,
                                                 0,
                                                 figuresCollectionView.frame.size.width,
                                                 figuresCollectionView.frame.size.height - 113);
        figuresCollectionView.showsVerticalScrollIndicator = NO;
        figuresCollectionView.bounces = NO;
        
        _figuresCollectionView = figuresCollectionView;
    }
    
    return _figuresCollectionView;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return SZDStatsCellCount; 
    //return [[[SZDPoopStats sharedStats] statsCollection] count];
}

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    NSInteger row = indexPath.row;
    
    if (row == SZDStatsCellDaysSinceLastPoop) {
        return CGSizeMake(4, 1);
    }
    if (row == SZDStatsCellTotalNumber) {
        return CGSizeMake(2, 2);
    }
    if (row == SZDStatsCellAveragePerDay || row == SZDStatsCellAverageDaysBetween) {
        return CGSizeMake(2, 1);
    }
    if (row == SZDStatsCellTotalTP || row == SZDStatsCellAverageTP) {
        return CGSizeMake(2, 1);
    }
    if (row == SZDStatsCellMostCommonType || row == SZDStatsCellMostCommonColour) {
        return CGSizeMake(2, 1);
    }
    if (row >= SZDStatsCellMostCommonExertion && row <= SZDStatsCellMostCommonFart) {
        return CGSizeMake(1, 1);
    }
    
    if (row == SZDStatsCellMostCommonPartOfDay) {
        return CGSizeMake(4, 1);
    }
    
    return CGSizeMake(4, 1);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //return UIEdgeInsetsZero;
    NSInteger w = 3;
    return UIEdgeInsetsMake(w, w, w, w);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SZDStatsCollectionViewCell *cell =
    [self.figuresCollectionView dequeueReusableCellWithReuseIdentifier:@"SZDStatsCollectionViewCell"
                                                          forIndexPath:indexPath];

    for (id subview in [cell.centerView subviews]) {
        [subview removeFromSuperview];
    }
    
    NSArray *data = [SZDPoopStats statsDataForKey:indexPath.row]; 
    
    //NSLog(@"data %@", data);
    
    //cell.backgroundColor = [UIColor yellowColor];
    //NSArray *data = [[[SZDPoopStats sharedStats] statsCollection] objectAtIndex:indexPath.row];
    
    cell.topLabel.text = data[0];
    cell.bottomLabel.text = data[2];
    
    //UIColor *backgroundColor = [UIColor colorWithRed:138.0/255.0 green:101.0/255.0 blue:51.0/255.0 alpha:1.0];
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    UIColor *backgroundColour = [ThemeManager themeColorNamed:@"backgroundColour"];
    cell.backgroundColor = backgroundColour;
    cell.topLabel.textColor = textColour;
    cell.bottomLabel.textColor = textColour;
    
    //cell.layer.borderWidth = 3.0f;
    cell.layer.borderColor = [ThemeManager themeColorNamed:@"textColour"].CGColor;
    cell.layer.cornerRadius = 3.0f;
    
    if ([data[1] isKindOfClass:[UIImage class]]) {
        UIImage *centerImage = data[1];
        
        UIImageView *center = [[UIImageView alloc] initWithImage:centerImage];
        center.frame = CGRectMake(cell.centerView.center.x - cell.centerView.frame.size.height * 0.5f,
                                  0,
                                  cell.centerView.frame.size.height,
                                  cell.centerView.frame.size.height);
        
        CGFloat height = cell.frame.size.height - cell.topLabel.frame.size.height - 8 - cell.bottomLabel.frame.size.height - 8 - 4 ;
        CGFloat y = 2;
        if (indexPath.row >= SZDStatsCellMostCommonExertion && indexPath.row <= SZDStatsCellMostCommonFart && height > 50) {
            y = y + (height /2.0 - 25);
            height = 50;
        }
        CGFloat x = cell.frame.size.width / 2.0 - height / 2.0;
        center.frame = CGRectMake(x, y, height, height);
        
        if (indexPath.row == SZDStatsCellMostCommonColour && [data count] > 3) {
            center.image = [center.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            // tint colour
            UIColor *colour = data[3];
            center.tintColor = colour;
        }
        [cell.centerView addSubview:center];
    } else {
        UILabel *center = [[UILabel alloc] init];
        center.text = data[1];
        center.textAlignment = NSTextAlignmentCenter;
        
        center.frame =
        CGRectMake(0, 0,
                   /*cellSize.width**/cell.frame.size.width,
                   /*cellSize.height**/cell.frame.size.height
                   - cell.topLabel.frame.size.height - 8
                   - cell.bottomLabel.frame.size.height - 8);
        center.font = [UIFont fontWithName:@"Avenir-Heavy" size:24.0];
        
        center.textColor = textColour;
        [cell.centerView addSubview:center];
    }
    return cell;
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.figuresCollectionView.collectionViewLayout invalidateLayout];
    //[self.figuresCollectionView reloadData];

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
    //return UIEdgeInsetsMake(0.0, 15.0, 100.0, 15.0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
    //return 30.0;
}

/*
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    NSLog(@"minimum interim");
    return 5.0;
}*/


@end
