//
//  SZDPoopEntry+CoreDataProperties.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-16.
//  Copyright © 2016 sandzapps. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SZDPoopEntry+CoreDataProperties.h"

@implementation SZDPoopEntry (CoreDataProperties)

@dynamic date;
@dynamic endDate;
@dynamic duration; 

/*@dynamic type;
@dynamic colour;
@dynamic execution;
@dynamic amount;*/

@dynamic tpCount; 

@dynamic comments;

@dynamic type; 
@dynamic colour;

@dynamic exertion;
@dynamic amount;
@dynamic smell;
@dynamic fart;

@end
