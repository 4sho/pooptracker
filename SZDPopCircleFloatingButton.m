//
//  SZDPopCircleButton.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-01.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPopCircleFloatingButton.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface SZDPopCircleFloatingButton ()

@property (nonatomic, strong) UIView *view;

@property (nonatomic) CGRect buttonFrame;

@end

@implementation SZDPopCircleFloatingButton

// init with a bunch of stuff
// disable designated initializer
// initialize 

/***************************************************************************
 *  init - create a floating action button
 *      properties:
 *      - frame - position in view, width, height
 *      - background image, title, background colour
 *      -
 *  method that gets called when you touch up the button
 *  initWithCircleSubmenu - create a floating action button and submenu 
 *  method that gets called when you touch up the submenu buttons
 *      properties: 
 *      -
 *      - startAngle
 *      - endAngle
 *      - buttonOnEndpoints
 *      - submenuButtonRadius
 *      - distanceFromMainButton
 *      - submenuImages/Titles
 ***************************************************************************/

+ (instancetype)buttonWithType:(UIButtonType)buttonType
{
    return [self buttonWithFrame:CGRectMake(0, 0, 50, 50)];
}

+ (instancetype)buttonWithFrame:(CGRect)frame
{
    SZDPopCircleFloatingButton *button = [super buttonWithType:UIButtonTypeCustom];
    
    if (button) {
        button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        button.frame = frame;
        
        [button setImage:[UIImage imageNamed:@"kitano.png"] forState:UIControlStateNormal];
        button.imageView.frame = button.frame;
        // Have a backgroundColor so that it appears even if no title or image is set
        //button.backgroundColor = [UIColor redColor];
        
        [button setTitle:@"PopCircleFloatingButton" forState:UIControlStateNormal];
        button.titleLabel.layer.opacity = 0.0f;
        
        [button addTarget:button action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        
        button.layer.zPosition = 100;
        
        button.buttonFrame = button.frame;
        
        
    }
    
    return button;
}

/*
+ (instancetype)initWithFrame:(CGRect)frame
             andSubmenuTitles:(NSArray *)submenuTitles
             andSubmenuImages:(NSArray *)submenuImages
{
    SZDPopCircleFloatingButton* button = [SZDPopCircleFloatingButton buttonWithType:UIButtonTypeCustom withFrame:frame];
    
    if (button) {
        [button addSubmenuWithRadiusDistance:100.0
                       startAngleInRadians:0.0f
                         endAngleInRadians:2.0f*M_PI
                         buttonOnEndpoints:YES
                             submenuTitles:submenuTitles
                              titlesHidden:NO
                             submenuImages:submenuImages
                       submenuButtonRadius:25.0f];
    }
    
    return button;
}*/

- (void)addSubmenuWithRadiusDistance:(CGFloat)submenuRadius
                 startAngleInRadians:(CGFloat)startAngle
                   endAngleInRadians:(CGFloat)endAngle
                   buttonOnEndpoints:(BOOL)buttonOnEndpoints
                       submenuTitles:(NSArray *)submenuTitles   /*required*/
                        titlesHidden:(BOOL)titlesHidden
                       submenuImages:(NSArray *)submenuImages   /*optional*/
                 submenuButtonRadius:(CGFloat)submenuButtonRadius
{
    NSMutableArray *submenuButtons = [[NSMutableArray alloc] init];
    
    CGFloat twoPI = 2.0f * M_PI;
    NSInteger buttonCount, numberOfDivisions;
    CGFloat menuAngle, divisionAngle, workingAngle; 
    /*CGPoint mainButtonCenter = CGPointMake(self.bounds.origin.x + 0.5f * self.bounds.size.width ,
                                           self.bounds.origin.y + 0.5f * self.bounds.size.height);*/
    CGPoint mainButtonCenter = CGPointMake(self.frame.origin.x + 0.5f * self.frame.size.width ,
                                           self.frame.origin.y + 0.5f * self.frame.size.height);
    
    buttonCount = [submenuTitles count];
    
    if (startAngle > endAngle) {
        menuAngle = twoPI - (startAngle - endAngle);
    } else {
        menuAngle = endAngle - startAngle;
    }
    
    // To detect a full circle - precision to the hundredths is enough
    float roundedMenuAngle = roundf(menuAngle * 100) / 100.0;
    float roundedTwoPi = roundf(twoPI * 100) / 100.0;
    
    if (!buttonOnEndpoints) {
        numberOfDivisions = buttonCount + 1;
    } else if (roundedMenuAngle == roundedTwoPi) { // if a full circle, can't place last button on endpoint
        numberOfDivisions = buttonCount;
        NSLog(@"Full circle");
    } else {
        numberOfDivisions = buttonCount - 1;
    }
    
    divisionAngle = menuAngle / numberOfDivisions;
    workingAngle = startAngle;
    if (!buttonOnEndpoints) {
        workingAngle = workingAngle + divisionAngle;
    }
    
    for (NSInteger i = 1; i < buttonCount+1; i++) {
        CGFloat relativeButtonAngle = workingAngle;
        if (relativeButtonAngle >= twoPI) {
            relativeButtonAngle -= twoPI;
        }

        CGFloat qx, qy, x, y, a, b;
        
        // Determine quadrant
        if ((0 <= relativeButtonAngle && relativeButtonAngle <= M_PI_2)) {  // 0 to PI/2
            qx = 1; qy = 1;
        } else if (M_PI_2 < relativeButtonAngle && relativeButtonAngle <= M_PI) { // PI/2 to PI
            relativeButtonAngle = M_PI - relativeButtonAngle;
            qx = -1; qy = 1;
        } else if (M_PI < relativeButtonAngle && relativeButtonAngle < 3.0f*M_PI_2) {  // PI to 3PI/2
            relativeButtonAngle = relativeButtonAngle - M_PI;
            qx = -1; qy = -1;
        } else if (3.0f*M_PI_2 <= relativeButtonAngle && relativeButtonAngle < twoPI) {  // 3PI/2 to 2PI
            relativeButtonAngle = twoPI - relativeButtonAngle;
            qx = 1; qy = -1;
        }
        
        a = submenuRadius * cosf(relativeButtonAngle) * qx;
        b = submenuRadius * sinf(relativeButtonAngle) * qy;
        
        x = mainButtonCenter.x - submenuButtonRadius + a;
        y = mainButtonCenter.y - submenuButtonRadius - b;

        CGRect buttonPosition = CGRectMake(x, y, submenuButtonRadius * 2.0f, submenuButtonRadius * 2.0f);
        
        workingAngle = workingAngle + divisionAngle;

        // Create the submenu button
        UIButton *button = [[UIButton alloc] initWithFrame:buttonPosition];
        button.hidden = YES;    // hide on initialization
        //button.backgroundColor = [UIColor greenColor];
        button.layer.zPosition = 100;
        
        if (submenuImages && [submenuImages count] >= i) {
            UIImage *backgroundImage = [UIImage imageNamed:submenuImages[i-1]];
            [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
            
        }
        if (submenuTitles && [submenuTitles count] >= i) {
            [button setTitle:submenuTitles[i-1] forState:UIControlStateNormal];
            if (titlesHidden) {
                button.titleLabel.alpha = 0.0f;
            }
        }
        
        [button addTarget:self action:@selector(submenuTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:i];
        [self.superview addSubview:button];
        //NSLog(@"%@", self.superview);
        [submenuButtons addObject:button];
        
    }
    
    _submenuButtons = submenuButtons;
}

- (void)touchUpInside:(SZDPopCircleFloatingButton *)button
{
    NSLog(@"touchUpInside: %@", button.titleLabel.text);
    
    if (self.submenuButtons) {
        [self toggleSubmenu];
    }
}

- (void)handleGesture:(id)sender {
    NSLog(@"handleGesture");
    [self toggleSubmenu];
}

- (void)toggleSubmenu
{
    NSLog(@"Toggling submenu.");
    
    
    for (UIButton *submenuButton in self.submenuButtons) {
        
        CGRect positionFrame = submenuButton.frame;
        CGPoint mainButtonCenter =
        CGPointMake(self.buttonFrame.origin.x + 0.5f * self.buttonFrame.size.width - 0.5f * submenuButton.frame.size.width,
                    self.buttonFrame.origin.y + 0.5f * self.buttonFrame.size.height - 0.5f * submenuButton.frame.size.height);
        CGRect mainButtonPosition = CGRectMake(mainButtonCenter.x, mainButtonCenter.y,
                                               submenuButton.frame.size.width,
                                               submenuButton.frame.size.height);
        
        if(self.submenuState == submenuStateClosed) {   // open submenu
            NSLog(@"opening submenu");
            submenuButton.frame = mainButtonPosition;
            submenuButton.hidden = NO;
            submenuButton.alpha = 0.0f;
            
            [UIView animateWithDuration:1.0f
                                  delay:0
                 usingSpringWithDamping:0.3f
                  initialSpringVelocity:0.0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 submenuButton.alpha = 1.0f;
                                 submenuButton.frame = positionFrame;
                             }
                             completion:^(BOOL finished) {
                             }];
            //[self becomeFirstResponder];
            
            /*UIView *view = [[UIView alloc] initWithFrame:self.superview.frame];
            view.backgroundColor = [UIColor yellowColor];
            [self.superview addSubview:view];
            UIGestureRecognizer *gr = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
            [view addGestureRecognizer:gr];
            self.view = view;
            view.layer.zPosition = 99;*/
            
            
            self.backgroundColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.3];
            self.frame = self.superview.frame;  // extend the button's frame
            self.imageView.frame = self.buttonFrame;    // keep the image where it is
            self.imageEdgeInsets = UIEdgeInsetsMake(self.buttonFrame.origin.y,
                                                      self.buttonFrame.origin.x,
                                                      0,
                                                      self.buttonFrame.origin.x);
            
        } else if (self.submenuState == submenuStateOpen) { // close submenu
            NSLog(@"closing submenu");
            
            // restore the button's frame whence it came
            self.frame = self.buttonFrame;
            self.backgroundColor = [UIColor clearColor];
            self.alpha = 1.0;
            self.imageEdgeInsets = UIEdgeInsetsZero; 
            
            //[self.view removeFromSuperview];
            
            [UIView animateKeyframesWithDuration:0.5f
                                           delay:0
                                         options:0
                                      animations:^{
                                          
                                          [UIView addKeyframeWithRelativeStartTime:0.4 relativeDuration:1.0 animations:^{
                                              submenuButton.alpha = 0.0f;
                                              submenuButton.frame = mainButtonPosition;
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          } ];
                                          [UIView addKeyframeWithRelativeStartTime:0.1 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.2 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.3 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.4 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          } ];
                                          [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.6 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                          [UIView addKeyframeWithRelativeStartTime:0.7 relativeDuration:0.1 animations:^{
                                              submenuButton.transform = CGAffineTransformRotate(submenuButton.transform, DEGREES_TO_RADIANS(90));
                                          }];
                                      }
                                      completion:^(BOOL finished) {
                                          submenuButton.frame = positionFrame;
                                          submenuButton.hidden = YES;
                                      }];
            //[self resignFirstResponder];
            
        }
    }
    
    self.submenuState = !self.submenuState;
}



- (void)submenuTouchUpInside:(id)submenuButton
{
    UIButton *button = (UIButton *)submenuButton;
    NSLog(@"submenuTouchUpInside: %@", button.titleLabel.text);
    
    [self toggleSubmenu];
}

@end
