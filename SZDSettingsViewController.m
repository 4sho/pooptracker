//
//  SZDSettingsViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-24.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDSettingsViewController.h"
#import "ThemeManager.h"
#import "SettingsManager.h"

@interface SZDSettingsViewController ()

typedef NS_ENUM(NSInteger, SZDSettingsSection)
{
    SZDSettingsSectionAppearance = 0,
    SZDSettingsSectionOtherSettings
};

typedef NS_ENUM(NSInteger, SZDSettingsRow)
{
    SZDSettingsRowTheme = 0,
    SZDSettingsRowIconTheme = 1,
    SZDSettingsRowEditOnStop = 0
};

@end

@implementation SZDSettingsViewController

- (instancetype)init
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Settings" bundle:[NSBundle mainBundle]];
    
    self = [storyboard instantiateViewControllerWithIdentifier:@"SZDSettingsViewController"];
    
    if(self)
    {
    }
    return  self;

}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    if (section == SZDSettingsSectionAppearance) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (row == SZDSettingsRowTheme)
        {
        } else if (row == SZDSettingsRowIconTheme) {
        }
    }
    
    if (section == SZDSettingsSectionOtherSettings) {
        if (row == SZDSettingsRowEditOnStop) {
            UISwitch *toggle = [[UISwitch alloc] initWithFrame:CGRectZero];
            [toggle addTarget:self action:@selector(editOnStopToggle:) forControlEvents:UIControlEventValueChanged];
            BOOL editOnStop = (BOOL)[SettingsManager boolSettingFor:@"editOnStop"];
            NSLog(@"editOnStop is %d", editOnStop);
            toggle.on = editOnStop;
            
            cell.accessoryView = toggle;
        }
    }
    return cell;
}

- (void)editOnStopToggle:(UISwitch *)toggleSwitch
{
    if (toggleSwitch.on) {
        [SettingsManager setSettingFor:@"editOnStop" withBool:YES];
    } else {
        [SettingsManager setSettingFor:@"editOnStop" withBool:NO];
        BOOL editOnStop = (BOOL)[SettingsManager boolSettingFor:@"editOnStop"];
        NSLog(@"editOnStop is changed to %d", editOnStop);
    }
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //NSUInteger section = [indexPath section];
    //NSUInteger row = [indexPath row];


}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
