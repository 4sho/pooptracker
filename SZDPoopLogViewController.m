//
//  SZDPoopLogViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZDPoopLogViewController.h"
#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "SZDPoopAttributes.h"
#import "SZDPoopLogTableViewCell.h"
#import "SZDPoopEditViewController.h"
#import "FSCalendar.h"
#import "ThemeManager.h"
#import "MGSwipeTableCell.h"
#import "SZDBarButtonItemAdd.h"

@interface SZDPoopLogViewController ()

// Holds the table section headers (NSString *)
@property (nonatomic, strong) NSMutableArray *tableViewSections;
// Holds all of the entries that go under each table section header (key)
@property (nonatomic, strong) NSMutableDictionary *tableViewCells;

@end

@implementation SZDPoopLogViewController

- (instancetype)init
{
    return [self initWithStyle:UITableViewStylePlain];
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Set up Navigation bar
        self.navigationItem.title = @"Log";
        
        /*UIBarButtonItem *newEntryBBI = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                        target:self
                                        action:@selector(addNewEntry:)];*/
        UIBarButtonItem *newEntryBBI = [[SZDBarButtonItemAdd alloc]
                                        initWithViewController:self logViewController:self];
        self.navigationItem.rightBarButtonItem = newEntryBBI;
        
        self.restorationIdentifier = NSStringFromClass([self class]);
        self.restorationClass = [self class];
        self.tableView.tintColor = [UIColor whiteColor];
        self.tableView.separatorColor = [ThemeManager themeColorNamed:@"tableSeparatorColour"];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.bounces = NO;
        self.tableView.scrollsToTop = YES;
        self.tableView.showsVerticalScrollIndicator = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    //[self.tableView registerClass:[SZDPoopTableViewCell class] forCellReuseIdentifier:@"SZDPoopTableViewCell"];
    
    UINib *nib = [UINib nibWithNibName:@"SZDPoopLogTableViewCell"
                                bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:@"SZDPoopLogTableViewCell"];
    
    [self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"viewWillAppear");
    //[self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
    //[self.tableView reloadData];
    [super viewWillAppear:animated];
    [self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableViewSections count];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    //return [[[SZDPoopStore sharedStore] allEntries] count];
    // Retrieve the header to get the array of entries belonging to that section
    id key = [self.tableViewSections objectAtIndex:section];
    NSArray *tableViewCellsForSection = [self.tableViewCells objectForKey:key];
    return [tableViewCellsForSection count];
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    return [self.tableViewSections objectAtIndex:section];
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UIColor *subBarTintColour = [ThemeManager themeColorNamed:@"subBarTintColour"];
    UIColor *subBarTextColour = [ThemeManager themeColorNamed:@"subBarTextColour"];
    view.tintColor = subBarTintColour;
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:subBarTextColour];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSString *sectionHeading = [self.tableViewSections objectAtIndex:indexPath.section];
    SZDPoopEntry *entry = [[self.tableViewCells objectForKey:sectionHeading] objectAtIndex:indexPath.row];

    if ([entry.comments length] > 0) {
        return 75.0;
    }
    return 60.0;
}

// Cell creation
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SZDPoopLogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SZDPoopLogTableViewCell"
                                                            forIndexPath:indexPath];
    
    //SZDPoopEntry *entry = [[[SZDPoopStore sharedStore] allEntries] objectAtIndex:indexPath.row];
    NSString *sectionHeading = [self.tableViewSections objectAtIndex:indexPath.section];
    SZDPoopEntry *entry = [[self.tableViewCells objectForKey:sectionHeading] objectAtIndex:indexPath.row];
   
    [cell setUpCellWithEntry:entry];
    
    cell.rightButtons = [self swipeButtons:indexPath];
    cell.rightSwipeSettings.transition = MGSwipeTransitionBorder;
    
    return cell;
}

- (NSArray *)swipeButtons:(NSIndexPath *)indexPath
{
    NSMutableArray *buttons = [[NSMutableArray alloc] init];
    
    UIColor *swipeButtonBackgroundColour = [ThemeManager themeColorNamed:@"subBarTintColour"];
    UIColor *swipeButtonColour = [ThemeManager themeColorNamed:@"subBarTextColour"];
    NSArray *icons = @[@"action_delete", @"action_edit", @"action_share"];
    
    for (int i = 0; i < [icons count]; i++) {
        UIImage *icon = [[UIImage imageNamed:icons[i]]
                         imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        NSInteger tag = i+1;
        MGSwipeButton *button = [MGSwipeButton buttonWithTitle:@""
                                                          icon:icon
                                               backgroundColor:swipeButtonBackgroundColour
                                                       padding:15
                                                      callback:^BOOL(MGSwipeTableCell *sender) {
                                                          [self swipeButtonAction:indexPath tag:tag];
                                                          return YES;
                                                      }];
        button.imageView.tintColor = swipeButtonColour;
        button.tag = tag;
        
        [buttons addObject:button];
    }
    return buttons;
}

- (void)swipeButtonAction:(NSIndexPath *)indexPath tag:(NSInteger)tag
{
    NSLog(@"swipeButtonAction: %ld", (long)tag);
    
    NSString *sectionHeading = [self.tableViewSections objectAtIndex:indexPath.section];
    SZDPoopEntry *entry = [[self.tableViewCells objectForKey:sectionHeading] objectAtIndex:indexPath.row];
    
    if (tag == 2) { // edit
        [self presentEditViewControllerForEntry:entry];
    }
    
    if (tag == 1) { // delete
        [self removeEntry:entry indexPath:indexPath];
    }
}

- (void)addNewEntry:(id)sender
{
    SZDPoopEntry *newEntry = [[SZDPoopStore sharedStore] createEntry];
    
    // Have Edit Controller pop up
    SZDPoopEditViewController *newEntryViewController = [[SZDPoopEditViewController alloc]
                                                         initWithEntry:newEntry
                                                         isNewEntry:YES];
    newEntryViewController.dismissBlock = ^{
        [self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
        [self.tableView reloadData];
    };
    
    UINavigationController *entryNavController = [[UINavigationController alloc]
                                                     initWithRootViewController:newEntryViewController];
    [self presentViewController:entryNavController animated:YES completion:nil];
}

- (void)removeEntry:(SZDPoopEntry *)entry indexPath:(NSIndexPath *)indexPath
{
    [[SZDPoopStore sharedStore] removeEntry:entry];
    [self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
    
    if ([self.tableView numberOfRowsInSection:indexPath.section] == 1) { // delete section as there are no more rows
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationTop];
    } else{
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    }  

    [self.tableView reloadData];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


// When a row is selected
- (void)tableView:(UITableView *)tableView
   didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    //NSString *sectionHeading = [self.tableViewSections objectAtIndex:indexPath.section];
    //SZDPoopEntry *entry = [[self.tableViewCells objectForKey:sectionHeading] objectAtIndex:indexPath.row];
    
    //[self presentEditViewControllerForEntry:entry];

}

- (void)presentEditViewControllerForEntry:(SZDPoopEntry *)entry
{
    SZDPoopEditViewController *editViewController = [[SZDPoopEditViewController alloc] initWithEntry:entry isNewEntry:NO];
    editViewController.dismissBlock = ^{
        [self setUpTable:[[SZDPoopStore sharedStore] allEntries]];
        [self.tableView reloadData];
    };
    UINavigationController *editNavController = [[UINavigationController alloc] initWithRootViewController:editViewController];
    [self presentViewController:editNavController animated:YES completion:nil];
}

// stackoverflow.com/questions/17330496/uitableview-section-headers-by-month-of-dates-in-array
- (void)setUpTable:(NSArray *)sortedArrayByDate
{
    NSLog(@"Setting up table"); 
    self.tableViewSections = [NSMutableArray arrayWithCapacity:0];
    self.tableViewCells = [NSMutableDictionary dictionaryWithCapacity:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.timeZone = calendar.timeZone;
    [dateFormatter setDateFormat:@"MMMM YYYY"];
    
    NSUInteger dateComponents = NSYearCalendarUnit | NSMonthCalendarUnit;
    NSInteger previousYear = -1;
    NSInteger previousMonth = -1;
    NSMutableArray *tableViewCellsForSection = nil;
    
    for (SZDPoopEntry *entry in sortedArrayByDate) {
        NSDate *date = entry.date;
        NSDateComponents *components = [calendar components:dateComponents fromDate:date];
        NSInteger year = [components year];
        NSInteger month = [components month];
        //NSLog(@"%lu %lu vs %lu %lu", previousMonth, previousYear, month, year);
        
        if (year != previousYear || month != previousMonth) {   // First in this Month/Year
            // Create a sectionHeading and tableViewCellsForSection array
            NSString *sectionHeading = [dateFormatter stringFromDate:date];
            [self.tableViewSections addObject:sectionHeading];
            tableViewCellsForSection = [NSMutableArray arrayWithCapacity:0];
            // In the dictionary tableViewCells, for the key which is a string for the section heading,
            // set the object to be the tableViewCellsForSection.
            [self.tableViewCells setObject:tableViewCellsForSection forKey:sectionHeading];
            previousYear = year;
            previousMonth = month;
        }
        // For all that are in this month, add the entry to the month's tableViewCellsForSection array
        [tableViewCellsForSection addObject:entry];
        //NSLog(@"Entry goes in: %lu %lu", year, month);
    }
    
    //NSLog(@"Headings: %@", self.tableViewSections);
    //NSLog(@"Cells: %@", self.tableViewCells);
    
    //[self.tableView reloadData]; // reload to get new table section/row number
}


+ (UIViewController *)viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
    NSLog(@"Restoring LogVC");
    return [[self alloc] init]; 
}



@end
