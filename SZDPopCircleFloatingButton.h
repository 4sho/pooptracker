//
//  SZDPopCircleButton.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-01.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZDPopCircleFloatingButton : UIButton

typedef NS_ENUM(BOOL, submenuState)
{
    submenuStateClosed = 0,
    submenuStateOpen,
};
@property (nonatomic) BOOL submenuState;


@property (nonatomic, strong) UIViewController *superViewController;
@property (nonatomic, strong) NSArray *submenuButtons;

+ (instancetype)buttonWithFrame:(CGRect)frame;

/*+ (instancetype)initWithFrame:(CGRect)frame
             andSubmenuTitles:(NSArray *)submenuTitles
             andSubmenuImages:(NSArray *)submenuImages;*/
- (void)toggleSubmenu; 

- (void)addSubmenuWithRadiusDistance:(CGFloat)submenuRadius
                 startAngleInRadians:(CGFloat)startAngle
                   endAngleInRadians:(CGFloat)endAngle
                   buttonOnEndpoints:(BOOL)buttonOnEndpoints
                       submenuTitles:(NSArray *)submenuTitles   /*required*/
                        titlesHidden:(BOOL)titlesHidden
                       submenuImages:(NSArray *)submenuImages   /*optional*/
                 submenuButtonRadius:(CGFloat)submenuButtonRadius; 

- (void)submenuTouchUpInside:(id)submenuButton; 

@end
