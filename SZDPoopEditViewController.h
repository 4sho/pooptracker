//
//  SZDPoopEditViewController2.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-01.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDPoopEntry;

@interface SZDPoopEditViewController : UITableViewController 

@property (nonatomic, strong) SZDPoopEntry *entry;
@property (nonatomic, strong) UITableView *logTableView;
@property (nonatomic, copy) void (^dismissBlock)(void);

- (instancetype)initWithEntry:(SZDPoopEntry *)entry isNewEntry:(BOOL)isNewEntry;

@end
