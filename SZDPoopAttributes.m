//
//  SZDPoopAttributes.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopAttributes.h"
#import <UIKit/UIKit.h>
#import "ThemeManager.h"

@interface SZDPoopAttributes ()
/*
typedef NS_ENUM(NSInteger, SZDPoopAttributeDictionaryIndex)
{
    SZDPoopAttributeDictionaryIndexTitle = 0,
    SZDPoopAttributeDictionaryIndexImage,
    SZDPoopAttributeDictionaryIndexColour,
    SZDPoopAttributeDictionaryIndexDescription
};*/

@property (nonatomic, strong) UIColor *colour0;
@property (nonatomic, strong) UIColor *colour1;
@property (nonatomic, strong) UIColor *colour2;
@property (nonatomic, strong) UIColor *colour3;
@property (nonatomic, strong) UIColor *colour4;
@property (nonatomic, strong) UIColor *colour5;
@property (nonatomic, strong) UIColor *colour6;
@property (nonatomic, strong) UIColor *colour7;
@property (nonatomic, strong) UIColor *colour8;



@end

@implementation SZDPoopAttributes

@synthesize poopTypeDictionary = _poopTypeDictionary;
@synthesize poopColourDictionary = _poopColourDictionary;
@synthesize poopExertionDictionary = _poopExertionDictionary;
@synthesize poopAmountDictionary = _poopAmountDictionary;
@synthesize poopSmellDictionary = _poopSmellDictionary;
@synthesize poopFartDictionary = _poopFartDictionary;

@synthesize attributeDictionary = _attributeDictionary;

+ (instancetype)sharedInstance
{
    static SZDPoopAttributes *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initPrivate];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[SZDPoopAttributes]sharedInstance"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        _colour0 = [self colorFromHexString:@"E8E1CA"];
        _colour1 = [self colorFromHexString:@"805112"];
        _colour2 = [self colorFromHexString:@"BA8B4A"];
        _colour3 = [self colorFromHexString:@"543304"];
        _colour4 = [self colorFromHexString:@"C7A556"];
        _colour5 = [self colorFromHexString:@"A66A17"];
        _colour6 = [self colorFromHexString:@"B57E41"];
        _colour7 = [self colorFromHexString:@"BA9E82"];
        _colour8 = [self colorFromHexString:@"6B5514"];
    }
    return self;
}

- (NSDictionary *)attributeDictionary
{
    if (!_attributeDictionary) {
        // key, entity, title, icon, description
        UIImage *typeIcon = [ThemeManager iconNamed:@"type0"];
        UIImage *colourIcon = [ThemeManager iconNamed:@"colour0"];
        UIImage *exertionIcon = [ThemeManager iconNamed:@"exertion0"];;
        UIImage *amountIcon = [ThemeManager iconNamed:@"amount0"];
        UIImage *smellIcon = [ThemeManager iconNamed:@"smell0"];
        UIImage *fartIcon = [ThemeManager iconNamed:@"fart0"];

        
        NSDictionary *attributeDictionary =
        @{ [NSNumber numberWithInteger:SZDPoopAttributesType] : @[@"type", @"SZDPoopType", @"Type", typeIcon],
           [NSNumber numberWithInteger:SZDPoopAttributesColour] : @[@"colour", @"SZDPoopColour", @"Colour", colourIcon],
           [NSNumber numberWithInteger:SZDPoopAttributesExertion] : @[@"exertion", @"SZDPoopExertion", @"Exertion", exertionIcon],
           [NSNumber numberWithInteger:SZDPoopAttributesAmount] : @[@"amount", @"SZDPoopAmount", @"Amount", amountIcon],
           [NSNumber numberWithInteger:SZDPoopAttributesSmell] : @[@"smell", @"SZDPoopSmell", @"Smell", smellIcon],
           [NSNumber numberWithInteger:SZDPoopAttributesFart] : @[@"fart", @"SZDPoopFart", @"Fart", fartIcon]
           };
        
        _attributeDictionary = attributeDictionary;
    }
    
    return _attributeDictionary;
}

- (id)objectInAttributeDictionaryAtIndex:(NSInteger)objectIndex forAttribute:(NSInteger)attribute
{
    NSArray *attributeArray = [self.attributeDictionary objectForKey:[NSNumber numberWithInteger:attribute]];
    id object = [attributeArray objectAtIndex:objectIndex];
    return object;
}

- (NSDictionary *)poopTypeDictionary
{
    if (!_poopTypeDictionary) {
        UIImage *none = [ThemeManager iconNamed:@"type0"];
        UIImage *type1 = [ThemeManager iconNamed:@"type1"];
        UIImage *type2 = [ThemeManager iconNamed:@"type2"];
        UIImage *type3 = [ThemeManager iconNamed:@"type3"];
        UIImage *type4 = [ThemeManager iconNamed:@"type4"];
        UIImage *type5 = [ThemeManager iconNamed:@"type5"];
        UIImage *type6 = [ThemeManager iconNamed:@"type6"];
        UIImage *type7 = [ThemeManager iconNamed:@"type7"];
        
        // outline
        UIImage *type1_outline = [ThemeManager iconNamed:@"type1_outline"];
        UIImage *type2_outline = [ThemeManager iconNamed:@"type2_outline"];
        UIImage *type3_outline = [ThemeManager iconNamed:@"type3_outline"];
        UIImage *type4_outline = [ThemeManager iconNamed:@"type4_outline"];
        UIImage *type5_outline = [ThemeManager iconNamed:@"type5_outline"];
        UIImage *type6_outline = [ThemeManager iconNamed:@"type6_outline"];
        UIImage *type7_outline = [ThemeManager iconNamed:@"type7_outline"];
        
        _poopTypeDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, _colour0, @"No type.", none],
           
           [NSNumber numberWithInteger:1] : @[@"Type 1", type1, _colour1,
                                              @"Separate hard lumps, like nuts (hard to pass).", type1_outline],
           
           [NSNumber numberWithInteger:2] : @[@"Type 2", type2, _colour2,
                                              @"Sausage-shaped, but lumpy.", type2_outline],
           
           [NSNumber numberWithInteger:3] : @[@"Type 3", type3, _colour3,
                                              @"Like a sausage but with cracks on its surface.", type3_outline],
           
           [NSNumber numberWithInteger:4] : @[@"Type 4", type4, _colour4,
                                              @"Like a sausage or snake, smooth and soft.", type4_outline],
           
           [NSNumber numberWithInteger:5] : @[@"Type 5", type5, _colour5,
                                              @"Soft blobs with clear cut edges (passed easily).", type5_outline],
           
           [NSNumber numberWithInteger:6] : @[@"Type 6", type6, _colour6,
                                              @"Fluffy pieces with ragged edges, a mushy stool.", type6_outline],
           
           [NSNumber numberWithInteger:7] : @[@"Type 7", type7, _colour7,
                                              @"Watery, no solid pieces, entirely liquid.", type7_outline] };
    }
    return _poopTypeDictionary;
}
- (NSDictionary *)poopColourDictionary
{
    if (!_poopColourDictionary) {
        NSString *noDescription = @"";
        
        UIImage *none = [ThemeManager iconNamed:@"colour0"];
        UIImage *square = [ThemeManager iconNamed:@"colour0"];
        _poopColourDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, [UIColor whiteColor], @"No Colour."],
           [NSNumber numberWithInteger:1] : @[@"Black", square, [UIColor blackColor], noDescription],
           [NSNumber numberWithInteger:2] : @[@"Brown", square, [UIColor brownColor], noDescription],
           [NSNumber numberWithInteger:3] : @[@"Green", square, [UIColor greenColor], noDescription],
           [NSNumber numberWithInteger:4] : @[@"Yellow", square, [UIColor yellowColor], noDescription],
           [NSNumber numberWithInteger:5] : @[@"White/Clay", square, [UIColor grayColor], noDescription],
           [NSNumber numberWithInteger:6] : @[@"Red", square, [UIColor redColor], noDescription],
           [NSNumber numberWithInteger:7] : @[@"Orange", square, [UIColor orangeColor], noDescription],
           [NSNumber numberWithInteger:8] : @[@"Blue", square, [UIColor blueColor], noDescription],
           [NSNumber numberWithInteger:9] : @[@"Purple", square, [UIColor purpleColor], noDescription]};
        
    }
    return _poopColourDictionary;
}

// "scale"
- (NSDictionary *)poopExertionDictionary
{
    if (!_poopExertionDictionary) {
        UIImage *none = [ThemeManager iconNamed:@"exertion0"];
        
        UIImage *level1 = [ThemeManager iconNamed:@"exertion1"];
        UIImage *level2 = [ThemeManager iconNamed:@"exertion2"];
        UIImage *level3 = [ThemeManager iconNamed:@"exertion3"];
        
        _poopExertionDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, _colour0, @"No rating."],
           [NSNumber numberWithInteger:1] : @[@"", level1, _colour1, @"Smooth sailing."],
           [NSNumber numberWithInteger:2] : @[@"", level2, _colour2, @"No biggie."],
           [NSNumber numberWithInteger:3] : @[@"", level3, _colour3, @"Used the force."]};
    }
    return _poopExertionDictionary;
}
- (NSDictionary *)poopAmountDictionary
{
    if (!_poopAmountDictionary) {
        UIImage *none = [ThemeManager iconNamed:@"amount0"];

        
        UIImage *level1 = [ThemeManager iconNamed:@"amount1"];
        UIImage *level2 = [ThemeManager iconNamed:@"amount2"];
        UIImage *level3 = [ThemeManager iconNamed:@"amount3"];
        
        _poopAmountDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, _colour0, @"No rating."],
           [NSNumber numberWithInteger:1] : @[@"", level1, _colour1, @"Particles."],
           [NSNumber numberWithInteger:2] : @[@"", level2, _colour2, @"The usual."],
           [NSNumber numberWithInteger:3] : @[@"", level3, _colour3, @"Baby."]};
    }
    return _poopAmountDictionary;
}
- (NSDictionary *)poopSmellDictionary
{
    if (!_poopSmellDictionary) {
        UIImage *none = [ThemeManager iconNamed:@"smell0"];
        
        UIImage *level1 = [ThemeManager iconNamed:@"smell1"];
        UIImage *level2 = [ThemeManager iconNamed:@"smell2"];
        UIImage *level3 = [ThemeManager iconNamed:@"smell3"];
        
        _poopSmellDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, _colour0, @"No rating."],
           [NSNumber numberWithInteger:1] : @[@"", level1, _colour1, @"Fresh."],
           [NSNumber numberWithInteger:2] : @[@"", level2, _colour2, @"Smelly."],
           [NSNumber numberWithInteger:3] : @[@"", level3, _colour3, @"Death."]};
    }
    return _poopSmellDictionary;
}
- (NSDictionary *)poopFartDictionary
{
    if (!_poopFartDictionary) {
        UIImage *none = [ThemeManager iconNamed:@"fart0"];
        
        UIImage *level1 = [ThemeManager iconNamed:@"fart1"];
        UIImage *level2 = [ThemeManager iconNamed:@"fart2"];
        UIImage *level3 = [ThemeManager iconNamed:@"fart3"];
        
        _poopFartDictionary =
        @{ [NSNumber numberWithInteger:0] : @[@"None", none, _colour0, @"No rating."],
           [NSNumber numberWithInteger:1] : @[@"", level1, _colour1, @"None."],
           [NSNumber numberWithInteger:2] : @[@"", level2, _colour2, @"Farts."],
           [NSNumber numberWithInteger:3] : @[@"", level3, _colour3, @"Nuclear!"]};
    }
    return _poopFartDictionary;
}
- (NSDictionary *)dictionaryForAttribute:(NSInteger)attribute
{
    NSDictionary *dictionary;
    
    if (attribute == SZDPoopAttributesType) {
        dictionary = self.poopTypeDictionary;
    } else if (attribute == SZDPoopAttributesColour) {
        dictionary = self.poopColourDictionary;
    } else if (attribute == SZDPoopAttributesExertion) {
        dictionary = self.poopExertionDictionary;
    } else if (attribute == SZDPoopAttributesAmount) {
        dictionary = self.poopAmountDictionary;
    } else if (attribute == SZDPoopAttributesSmell) {
        dictionary = self.poopSmellDictionary;
    } else if (attribute == SZDPoopAttributesFart) {
        dictionary = self.poopFartDictionary;
    } else {
        @throw [NSException exceptionWithName:@"objectsForAttribute:ofType:"
                                       reason:@"The SZDPoopAttribute attribute does not exist..."
                                     userInfo:nil];
    }
    
    return dictionary;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    //[scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


@end
