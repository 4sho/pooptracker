//
//  SZDPoopLogTableViewCell.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-15.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
@class SZDPoopEntry;

@interface SZDPoopLogTableViewCell : MGSwipeTableCell

@property (strong, nonatomic) SZDPoopEntry *entry;

@property (weak, nonatomic) IBOutlet UIImageView *typeColourImage;
@property (weak, nonatomic) IBOutlet UIImageView *typeColourImageOutline;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tpImage;
@property (weak, nonatomic) IBOutlet UILabel *tpCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *durationImage;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIView *attributeView;

- (void)setUpCellWithEntry:(SZDPoopEntry *)entry;

@end
