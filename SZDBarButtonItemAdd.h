//
//  SZDBarButtonItemAdd.h
//  PoopTracker
//
//  Created by Sandy House on 2016-04-06.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDPoopLogViewController;

@interface SZDBarButtonItemAdd : UIBarButtonItem

- (instancetype)initWithViewController:(UIViewController *)viewController
                     logViewController:(SZDPoopLogViewController *)logVC;
@end
