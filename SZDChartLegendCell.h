//
//  SZDChartLegendCell.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-23.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZDChartLegendCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *colour;
@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;


@end
