//
//  SZDStatsCollectionViewCell.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-14.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDStatsCollectionViewCell.h"
#import "ThemeManager.h"

@implementation SZDStatsCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    self.topLabel.textColor = textColour;
    self.bottomLabel.textColor = textColour;
    self.backgroundColor = [UIColor clearColor];
}


@end
