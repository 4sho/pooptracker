//
//  SZDPoopCalendarViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-22.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"

@class SZDPoopLogViewController; 

@interface SZDPoopCalendarViewController : UIViewController <FSCalendarDataSource, FSCalendarDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet FSCalendar *calendar;

@property (weak, nonatomic) IBOutlet UITableView *dayTable;  

@property (nonatomic, strong) SZDPoopLogViewController *logVC;


@end
