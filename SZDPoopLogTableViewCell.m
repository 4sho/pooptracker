//
//  SZDPoopLogTableViewCell.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-15.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopLogTableViewCell.h"
#import "SZDPoopEntry.h"
#import "ThemeManager.h"
#import "SZDPoopAttributes.h"

@implementation SZDPoopLogTableViewCell

- (void)awakeFromNib {
    // Initialization code
 
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    self.dateLabel.textColor = textColour;
    self.tpCountLabel.textColor = textColour;
    self.durationLabel.textColor = textColour;
    self.commentLabel.textColor = textColour;
    
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setUpCellWithEntry:(SZDPoopEntry *)entry
{
    self.entry = entry;
    
    
    
    self.dateLabel.text = [NSDateFormatter localizedStringFromDate:entry.date dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterShortStyle];
    
    if (entry.tpCount <= 0) {
        self.tpCountLabel.hidden = YES;
        self.tpImage.hidden = YES;
        //self.tpCountLabel.text = @"-";
        //self.tpImage.frame = CGRectMake(0, 0, 0, 0);
        //self.tpImage.contentMode = UIViewContentModeBottomLeft; // This determines position of image
        //self.tpImage.clipsToBounds = YES;
        //self.tpImage.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        //self.tpImage.image = nil;
    } else {
        self.tpCountLabel.text = [NSString stringWithFormat:@"%d", entry.tpCount];
        self.tpCountLabel.hidden = NO;
        self.tpImage.hidden = NO;
    }
    
    if (entry.duration <= 0) {
        self.durationLabel.hidden = YES;
        self.durationImage.hidden = YES;
        //self.durationLabel.text = @"-";
    } else {
        self.durationLabel.text = [NSString stringWithFormat:@"%dm", entry.duration];
        self.durationLabel.hidden = NO;
        self.durationImage.hidden = NO;
    }
    //self.commentLabel.text = @"I went to a chinese restaurant to buy a loaf of bread, bread, bread.";
    self.commentLabel.text = entry.comments;
    
    UIImage *typeImage = [entry.type valueForKey:@"image"];
    if (typeImage) {
        self.typeColourImage.image = [typeImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        UIImage *typeImageOutline = [[[[SZDPoopAttributes sharedInstance] dictionaryForAttribute:SZDPoopAttributesType] objectForKey:[entry.type valueForKey:@"index"]] objectAtIndex:SZDAttributeObjectTypeImageOutline];
        if (typeImageOutline) {
            self.typeColourImageOutline.image = typeImageOutline;
            //self.typeColourImageOutline.frame = self.typeColourImage.bounds;
            //self.typeColourImageOutline.tintColor = [UIColor lightGrayColor];
        }
    }
    UIColor *colour = [entry.colour valueForKey:@"colour"];
    if (![colour isEqual:[NSNull null]]) {
        self.typeColourImage.tintColor = colour;
    }

    CGFloat attributeViewHeight = self.attributeView.frame.size.height;
    
    for (UIView *subview in self.attributeView.subviews){
        [subview removeFromSuperview];
    }
    
    NSInteger visibleIconNumber = 0;
    for (NSInteger attribute = SZDPoopAttributesExertion; attribute < SZDPoopAttributesCount; attribute++) {
        NSString *selectorString = [[SZDPoopAttributes sharedInstance] objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectKey forAttribute:attribute];
        SEL attributeSelector = NSSelectorFromString(selectorString);
        
        if ([[[entry performSelector:attributeSelector] valueForKey:@"index"] integerValue] != 0) {
            UIImage *image = [[entry performSelector:attributeSelector] valueForKey:@"image"];
            if (image) {
                UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                CGFloat x = attributeViewHeight * visibleIconNumber;
                imageView.frame = CGRectMake(x, 0, attributeViewHeight, attributeViewHeight);
                
                [self.attributeView addSubview:imageView];
                visibleIconNumber++;
            }
        }
        
        
        
    }
    
    /*
    UIImage *tmp = [entry.exertion valueForKey:@"image"];
    for (UIView *subview in self.attributeView.subviews){
        [subview removeFromSuperview];
    }
    if (tmp) {
        UIImageView *tmpIV = [[UIImageView alloc] initWithImage:tmp];
        tmpIV.frame = CGRectMake(0, 0, 16, 16);
        
        [self.attributeView addSubview:tmpIV];
    }*/

    self.selectionStyle = UITableViewCellSelectionStyleNone;

}


@end
