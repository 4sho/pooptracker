//
//  SettingsManager.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-27.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SettingsManager.h"

@implementation SettingsManager

+ (SettingsManager *)sharedManager
{
    static SettingsManager *sharedManager = nil;
    if (!sharedManager)
    {
        sharedManager = [[SettingsManager alloc] init];
    }
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        // initialize settings
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        if (![defaults boolForKey:@"editOnStop"]) {
            NSLog(@"no default editOnStop");
            [defaults setBool:YES forKey:@"editOnStop"];
        }
        

    }
    return self;
}

+ (id)settingFor:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}

+ (BOOL)boolSettingFor:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:key];
}

+ (void)setSettingFor:(NSString *)key withBool:(BOOL)boolValue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"Setting %d for %@", boolValue, key); 
    [defaults setBool:boolValue forKey:key];
    
}

+ (void)setSettingFor:(NSString *)key withObject:(id)object
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:object forKey:key];
}

@end
