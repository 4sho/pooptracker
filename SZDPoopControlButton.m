//
//  SZDPoopControlButton.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-14.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopControlButton.h"
#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "SZDPoopEditViewController.h"
#import "SZDPoopLogViewController.h"
#import "SettingsManager.h"

@interface SZDPoopControlButton ()

typedef NS_ENUM(NSInteger, SZDPoopControlState)
{
    SZDPoopControlStateIdle = 0,
    SZDPoopControlStateStarted
    
};

@property (nonatomic, strong) NSDate *sessionDate;

@end

@implementation SZDPoopControlButton

+ (instancetype)buttonWithFrame:(CGRect)frame
{
    SZDPoopControlButton *button = [super buttonWithFrame:frame];
    
    NSLog(@"initializing button wth frame");
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc]
                                                           initWithTarget:button action:@selector(handleLongPressGesture:)];
    [button addGestureRecognizer:longPressGesture];

    [button setImage:[UIImage imageNamed:@"action_add.png"] forState:UIControlStateNormal];
    
    return button;
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)recognizer {
    
    //as you hold the button this would fire
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if (self.submenuState == submenuStateOpen) {    // Close submenu
            [self toggleSubmenu];
        }
        [self addNewEntry];
    }
}

- (void)addNewEntry
{
    SZDPoopEntry *newEntry = [[SZDPoopStore sharedStore] createEntry];
    
    // Have Edit Controller pop up
    SZDPoopEditViewController *newEntryViewController = [[SZDPoopEditViewController alloc]
                                                         initWithEntry:newEntry
                                                         isNewEntry:YES];
    newEntryViewController.dismissBlock = ^{
        [self.logVC setUpTable:[[SZDPoopStore sharedStore] allEntries]];
        [self.logVC.tableView reloadData];
    };
    
    UINavigationController *entryNavController = [[UINavigationController alloc]
                                                  initWithRootViewController:newEntryViewController];
    [self.superViewController presentViewController:entryNavController animated:YES completion:nil];
}

- (void)addSubmenuWithRadiusDistance:(CGFloat)submenuRadius
                 startAngleInRadians:(CGFloat)startAngle
                   endAngleInRadians:(CGFloat)endAngle
                   buttonOnEndpoints:(BOOL)buttonOnEndpoints
                       submenuTitles:(NSArray *)submenuTitles   /*required*/
                        titlesHidden:(BOOL)titlesHidden
                       submenuImages:(NSArray *)submenuImages   /*optional*/
                 submenuButtonRadius:(CGFloat)submenuButtonRadius
{
    [super addSubmenuWithRadiusDistance:submenuRadius startAngleInRadians:startAngle endAngleInRadians:endAngle buttonOnEndpoints:buttonOnEndpoints submenuTitles:submenuTitles titlesHidden:titlesHidden submenuImages:submenuImages submenuButtonRadius:submenuButtonRadius];
    
    [self controlMenuState:SZDPoopControlStateIdle];
}

- (void)controlMenuState:(NSInteger)state
{
    for (UIButton *b in self.submenuButtons) {
        b.enabled = NO;
        if ([b.titleLabel.text isEqualToString:@"Start"]) {
            if (state == SZDPoopControlStateIdle) {
                b.enabled = YES;
            }
        } else {    // Stop and Cancel
            if (state == SZDPoopControlStateStarted) {
                b.enabled = YES;
            }
        }
    }

}

- (void)submenuTouchUpInside:(id)submenuButton
{
    [super submenuTouchUpInside:submenuButton];
    
    UIButton *button = (UIButton *)submenuButton;
    
    if ([button.titleLabel.text isEqualToString:@"Start"]) {
        self.sessionDate = [NSDate date];
        
        [self controlMenuState:SZDPoopControlStateStarted];

    } else if ([button.titleLabel.text isEqualToString:@"Stop"]) {
        [self controlMenuState:SZDPoopControlStateIdle];
        NSDate *sessionEndDate = [NSDate date];
        SZDPoopStore *ps = [SZDPoopStore sharedStore];
        
        //SZDPoopEntry *entry = [ps createEntry];
        SZDPoopEntry *entry = [ps createEntryWithDate:self.sessionDate endDate:sessionEndDate];
        //entry.date = self.sessionDate;
        //entry.endDate = sessionEndDate;
        
        // editOnStop
        if ([SettingsManager boolSettingFor:@"editOnStop"]) {
            NSLog(@"editOnStop");
            SZDPoopEditViewController *newEntryViewController = [[SZDPoopEditViewController alloc]
                                                                 initWithEntry:entry
                                                                 isNewEntry:YES];
            newEntryViewController.dismissBlock = ^{
                [self.logVC setUpTable:[ps allEntries]];
                [self.logVC.tableView reloadData];
            };
            UINavigationController *entryNavController = [[UINavigationController alloc]
                                                          initWithRootViewController:newEntryViewController];
            [self.superViewController presentViewController:entryNavController animated:YES completion:nil];
            
        } else {
            [self.logVC setUpTable:[ps allEntries]];
            [self.logVC.tableView reloadData];
        }
        
    } else if ([button.titleLabel.text isEqualToString:@"Cancel"]) {
        
        [self controlMenuState:SZDPoopControlStateIdle];
        self.sessionDate = nil;
    }
}

@end
