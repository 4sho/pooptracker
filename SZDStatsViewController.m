//
//  SZDStatsViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDStatsViewController.h"
#import "SZDFiguresViewController.h"
#import "SZDPieChartViewController.h"
#import "SZDPoopStore.h"
#import "SZDBarButtonItemAdd.h"

@interface SZDStatsViewController ()

@end

@implementation SZDStatsViewController
@synthesize figuresViewController = _figuresViewController;
@synthesize pieChartsViewController = _pieChartsViewController;

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Stats", @"Charts"]];
        
        self.navigationItem.titleView = segmentedControl;
        self.segmentedControl = segmentedControl;
        segmentedControl.selectedSegmentIndex = 0;
        [segmentedControl addTarget:self action:@selector(selectedIndexChanged:) forControlEvents:UIControlEventValueChanged];
        self.view.backgroundColor = [UIColor clearColor];
        
        UIBarButtonItem *refreshStats = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshStats)];
        self.navigationItem.leftBarButtonItem = refreshStats;
        
        SZDBarButtonItemAdd *add = [[SZDBarButtonItemAdd alloc] initWithViewController:self logViewController:self.logVC];
        self.navigationItem.rightBarButtonItem = add; 
    }
    
    return self;
}

- (void)refreshStats
{
    [[SZDPoopStore sharedStore] refreshStats]; 
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.figuresViewController.view];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        [self.figuresViewController.figuresCollectionView reloadData];
    } else if (self.segmentedControl.selectedSegmentIndex == 1) {
        [self.pieChartsViewController.pieChart reloadData];
        [self.pieChartsViewController.pieChartLegend reloadData];
    }
}

- (SZDFiguresViewController *)figuresViewController
{
    if (!_figuresViewController) {
        NSLog(@"init: figuresViewController");
        SZDFiguresViewController *figuresViewController = [[SZDFiguresViewController alloc] init];
        
        figuresViewController.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height);
        
        
        _figuresViewController = figuresViewController;
    }
    
    return _figuresViewController;
}

- (SZDPieChartViewController *)pieChartsViewController
{
    if (!_pieChartsViewController) {
        NSLog(@"init: pieChartsViewController");
        SZDPieChartViewController *pieChartsViewController = [[SZDPieChartViewController alloc] init];
    
        //pieChartsViewController.view.backgroundColor = [UIColor clearColor];
        pieChartsViewController.view.frame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 20.0, self.view.bounds.size.width, self.view.bounds.size.height);
        
        NSLog(@"navcontroller: %f", self.navigationController.navigationBar.frame.size.height);
        
        _pieChartsViewController = pieChartsViewController;
    }
    
    return _pieChartsViewController;
}


- (void)selectedIndexChanged:(UISegmentedControl *)segmentedControl
{
    NSLog(@"StatsVC: selectedIndexChanged");
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        [self.pieChartsViewController.view removeFromSuperview];
        [self.view addSubview:self.figuresViewController.view];
        [self.figuresViewController.figuresCollectionView reloadData];
        //[self presentViewController:self.figuresViewController animated:NO completion:nil];
    } else if (segmentedControl.selectedSegmentIndex == 1) {
        [self.figuresViewController.view removeFromSuperview];
        [self.view addSubview:self.pieChartsViewController.view];
        [self.pieChartsViewController.pieChart reloadData];
        [self.pieChartsViewController.pieChartLegend reloadData];
        //[self presentViewController:self.pieChartsViewController animated:YES completion:nil];
    }
}


@end
