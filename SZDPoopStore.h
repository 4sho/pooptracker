//
//  SZDPoopStore.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SZDPoopEntry;

@interface SZDPoopStore : NSObject /*<NSCoding>*/

@property (nonatomic, readonly) NSArray *allEntries;

typedef NS_ENUM(NSInteger, SZDEntryState)
{
    SZDEntryStateNew = 0,
    SZDEntryStateExisting,
    SZDEntryStateDestroy
};

typedef NS_ENUM(NSInteger, SZDStat)
{
    SZDStatTotalNumberOfEntries = 0,
    SZDStatOldestEntryDate,
    SZDStatNewestEntryDate,
    SZDStatTotalTPCount,
    SZDStatValidTPEntryCount,
    SZDStatMinTimeBetween,
    SZDStatMaxTimeBetween,
    SZDStatPartsOfDay
};

+ (instancetype)sharedStore;    // Singleton of SZDPoopStore

- (SZDPoopEntry *)createEntryWithDate:(NSDate *)date endDate:(NSDate *)endDate;
- (SZDPoopEntry *)createEntry;
- (void)removeEntry:(SZDPoopEntry *)entry;
- (void)reInsertEntry:(SZDPoopEntry *)entry;

- (BOOL)saveChanges;

//- (NSInteger)entryCountWithPredicates:(NSString *)predicate;

- (NSArray *)allTypesForAttribute:(NSInteger)attribute;

@property (nonatomic) float tpCountAverage;

- (NSMutableArray *)attributeCount:(NSInteger)attribute;


/*- (void)updateEntry:(SZDPoopEntry *)entry withTPCount:(int32_t)tpCount;
- (void)updateEntry:(SZDPoopEntry *)entry forAttribute:(NSInteger)attribute withTypeIndex:(NSInteger)index; 
- (void)updateEntry:(SZDPoopEntry *)entry forAttribute:(NSInteger)attribute withTypeIndex:(NSInteger)index entryState:(NSInteger)entryState;*/

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;
- (NSDictionary *)allEntriesByDate;

//- (void)tmpAllStats;

- (id)statForKey:(NSInteger)key;

- (void)updateEntry:(SZDPoopEntry *)entry
         entryState:(NSInteger)entryState
               date:(NSDate *)date
            endDate:(NSDate *)endDate
           duration:(int32_t)duration
            tpCount:(int32_t)tpCount
           comments:(NSString *)comments
         attributes:(NSArray *)attributes;
- (void)refreshStats; 

@end
