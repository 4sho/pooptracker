//
//  SZDFiguresViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RFQuiltLayout.h"

@interface SZDFiguresViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, RFQuiltLayoutDelegate>

@property (nonatomic, strong) UICollectionView *figuresCollectionView; 

@end
