//
//  SZDPoopTypeViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-07.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class SZDPoopEntry;

@interface SZDPoopTypeViewController : UITableViewController

@property (nonatomic, strong) SZDPoopEntry *entry;
@property (nonatomic) NSInteger attribute;

@property (nonatomic) NSManagedObject *unsavedType;

@property (nonatomic, copy) void (^dismissBlock)(void);


@end
