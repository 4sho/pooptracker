//
//  SZDPoopTypeViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-07.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopTypeViewController.h"
#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "SZDPoopAttributes.h"
#import "ThemeManager.h"

@interface SZDPoopTypeViewController ()

@end

@implementation SZDPoopTypeViewController


- (instancetype)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        self.view.backgroundColor = [UIColor clearColor];
        self.tableView.bounces = NO;
        CGRect rect = self.navigationController.navigationBar.frame;
        float y = rect.size.height + rect.origin.y;
        self.tableView.contentInset = UIEdgeInsetsMake(y+10,0,0,0);
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.dismissBlock();
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SZDPoopStore *ps = [SZDPoopStore sharedStore];
    if (self.attribute == SZDPoopAttributesType) {
        return [[ps allTypesForAttribute:SZDPoopAttributesType] count];
    } else if (self.attribute == SZDPoopAttributesColour) {
        return [[ps allTypesForAttribute:SZDPoopAttributesColour] count];
    }
    return 0;   // should never occur
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"UITableViewCell"];
    }
    
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.attribute];
    NSManagedObject *type = allTypes[indexPath.row];
    
    cell.textLabel.text = [type valueForKey:@"title"];

    if (self.attribute == SZDPoopAttributesType) {
        cell.detailTextLabel.text = [type valueForKey:@"desc"];
        cell.detailTextLabel.textColor = [ThemeManager themeColorNamed:@"subTextColour"];
    }
    
    UIImage *icon = [type valueForKey:@"image"];
    if (self.attribute == SZDPoopAttributesType) {
        icon = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    } else {
        icon = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    cell.imageView.image = icon;
    cell.imageView.tintColor = [type valueForKey:@"colour"];
    
    
    if (type == self.unsavedType) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.textColor = [ThemeManager themeColorNamed:@"textColour"]; 
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    NSArray *allTypes = [[SZDPoopStore sharedStore] allTypesForAttribute:self.attribute];
    NSManagedObject *type = allTypes[indexPath.row];
    self.unsavedType = type;
    
    [self.tableView reloadData];    // refresh cells to uncheck checked 
    
    [self.navigationController popViewControllerAnimated:YES]; 
}

@end
