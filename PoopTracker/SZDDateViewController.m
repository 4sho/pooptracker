//
//  SZDDateViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-08.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDDateViewController.h"
#import "SZDPoopEntry.h"
#import "SZDPoopEditViewController.h"
#import "SZDPoopStore.h"
#import "ThemeManager.h"

@interface SZDDateViewController ()

typedef NS_ENUM(NSInteger, SZDDateField)
{
    SZDDateFieldDate = 0,
    SZDDateFieldEndDate,
    SZDDateFieldDuration
};

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIDatePicker *endDatePicker;
@property (nonatomic, strong) UIDatePicker *durationPicker;
@property (nonatomic, strong) UIToolbar *endDateToolbar;

@property (nonatomic, strong) UITableViewCell *dateCell;
@property (nonatomic, strong) UITableViewCell *endDateCell;
@property (nonatomic, strong) UITableViewCell *durationCell;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;


@end

@implementation SZDDateViewController
@synthesize endDateToolbar = _endDateToolbar;
@synthesize datePicker = _datePicker;
@synthesize endDatePicker = _endDatePicker;
@synthesize durationPicker = _durationPicker;

- (instancetype)init
{
    return [self initWithStyle:UITableViewStyleGrouped];
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorColor = [ThemeManager themeColorNamed:@"tableSeparatorColour"];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"UITableViewCell"];
    }
    
    // style
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    cell.textLabel.textColor = textColour;
    cell.detailTextLabel.textColor = textColour;
    cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row == SZDDateFieldDate) {
        self.dateCell = cell;
        cell.textLabel.text = @"Start Date";
        NSString* date = [NSDateFormatter localizedStringFromDate:self.unsavedDate
                                                      dateStyle:NSDateFormatterShortStyle
                                                      timeStyle:NSDateFormatterShortStyle];
        cell.detailTextLabel.text = date;
        
    } else if (indexPath.row == SZDDateFieldEndDate) {
        self.endDateCell = cell;
        cell.textLabel.text = @"End Date";
        if (self.unsavedEndDate) {
            NSString* endDate = [NSDateFormatter localizedStringFromDate:self.unsavedEndDate
                                                            dateStyle:NSDateFormatterShortStyle
                                                            timeStyle:NSDateFormatterShortStyle];
            cell.detailTextLabel.text = endDate;
        }

    } else if (indexPath.row == SZDDateFieldDuration) {
        self.durationCell = cell;
        cell.textLabel.text = @"Duration";
        if (self.unsavedEndDate) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%dm", self.unsavedDuration];
        }
        
        //cell.userInteractionEnabled = NO;
    }
    
    
    
    return cell;
}


- (void)removeDatePickers
{
    // remove any datepickers
    [self.datePicker removeFromSuperview];
    [self.endDatePicker removeFromSuperview];
    [self.endDateToolbar removeFromSuperview];
    [self.durationPicker removeFromSuperview];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeDatePickers];
    
    // update values
    [self updateUnsavedValues];
    
    if (indexPath.row == SZDDateFieldDate) {
        if (!self.datePicker) {
            self.datePicker = [self datePicker:SZDDateFieldDate];
        }
        self.datePicker.date = self.unsavedDate;
        [self.view addSubview:self.datePicker];
    }
    else if (indexPath.row == SZDDateFieldEndDate) {
        if (!self.endDatePicker) {  // refactor this
            self.endDatePicker = [self datePicker:SZDDateFieldEndDate];
            self.endDatePicker.datePickerMode = UIDatePickerModeTime;
        }
        
        if (self.unsavedEndDate) {
            self.endDatePicker.date = self.unsavedEndDate;
        } else {
            self.endDatePicker.date = self.unsavedDate;
        }
        
        [self.view addSubview:self.endDateToolbar];
        [self.view addSubview:self.endDatePicker];
    } else if (indexPath.row == SZDDateFieldDuration) {
        if (!self.durationPicker) {
            self.durationPicker = [self datePicker:SZDDateFieldDuration];
            self.durationPicker.datePickerMode = UIDatePickerModeCountDownTimer;
        }
        
        if (self.unsavedDuration) {
            [self.durationPicker setCountDownDuration:self.unsavedDuration*60];
        }
        
        [self.view addSubview:self.durationPicker];
    }
}

- (void)dateChanged:(id)sender
{
    if ([sender tag] == SZDDateFieldDate) {
        self.dateCell.detailTextLabel.text =
        [NSDateFormatter localizedStringFromDate:self.datePicker.date
                                       dateStyle:NSDateFormatterShortStyle
                                       timeStyle:NSDateFormatterShortStyle];
        
        // update duration
    }
    else if ([sender tag] == SZDDateFieldEndDate) {
        self.endDateCell.detailTextLabel.text =
        [NSDateFormatter localizedStringFromDate:self.endDatePicker.date
                                       dateStyle:NSDateFormatterShortStyle
                                       timeStyle:NSDateFormatterShortStyle];
        // update duration
        NSDate *date = [self.dateFormatter dateFromString:self.dateCell.detailTextLabel.text];
        NSDate *endDate = [self.dateFormatter dateFromString:self.endDateCell.detailTextLabel.text];
        
        int duration = [self calculateDurationFrom:date to:endDate];
        self.durationCell.detailTextLabel.text = [NSString stringWithFormat:@"%dm", duration];
        /*if (duration < 0) {
            self.durationCell.detailTextLabel.text = @"< 0m";
        }*/
    }
    else if ([sender tag] == SZDDateFieldDuration) {
        int durationInSeconds = self.durationPicker.countDownDuration;
        self.durationCell.detailTextLabel.text = [NSString stringWithFormat:@"%dm", durationInSeconds/60];
        
        // calculate end date
        NSDate *newEndDate = [self calculateEndDateFrom:self.unsavedDate
                                                   with:durationInSeconds];
        self.endDateCell.detailTextLabel.text =
        [NSDateFormatter localizedStringFromDate:newEndDate
                                       dateStyle:NSDateFormatterShortStyle
                                       timeStyle:NSDateFormatterShortStyle];
    }
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeDatePickers];
}

- (void)cancel
{
    // save nothing
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)save
{
    [self updateUnsavedValues];

    // check if we have a negative duration
    if (self.unsavedDuration < 0) {
        UIAlertController *negativeAlert = [UIAlertController alertControllerWithTitle:@"Negative duration" message:@"Time doesn't work like that" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
        [negativeAlert addAction:ok];
        [self presentViewController:negativeAlert animated:YES completion:nil];
    } else {
        self.dismissBlock();
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updateUnsavedValues
{
    NSString *dateString = self.dateCell.detailTextLabel.text;
    NSString *endDateString = self.endDateCell.detailTextLabel.text;
    NSString *durationString = self.durationCell.detailTextLabel.text;
    
    NSDate *date = [self.dateFormatter dateFromString:dateString];
    self.unsavedDate = date;
    
    if (endDateString > 0) {    // if there is an end date, there must be a duration
        NSDate *newEndDate = [self.dateFormatter dateFromString:endDateString];
        self.unsavedEndDate = newEndDate;
        self.unsavedDuration = [durationString intValue];
    } else {
        self.unsavedEndDate = nil;
        self.unsavedDuration = 0;
    }
    
}

- (void)noEndDate
{
    //self.entry.endDate = nil;
    self.endDateCell.detailTextLabel.text = nil;
    self.durationCell.detailTextLabel.text = nil;
}

- (NSDateFormatter *)dateFormatter
{
    if(!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [_dateFormatter setDateStyle:NSDateFormatterShortStyle];
    }
    return _dateFormatter;
}

- (int)calculateDurationFrom:(NSDate *)date to:(NSDate *)endDate
{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *components = [c components:NSMinuteCalendarUnit
                                        fromDate:date toDate:endDate options:0];
    NSInteger diff = components.minute;
    return (int)diff;
}

- (NSDate *)calculateEndDateFrom:(NSDate *)date with:(int)duration
{
    return [date dateByAddingTimeInterval:duration];
}

- (UIDatePicker *)datePicker:(NSInteger)tag
{
    CGRect dateFrame = CGRectMake(0, self.view.frame.size.height - 260,
                                  self.view.frame.size.width, 200);
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:dateFrame];
    datePicker.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.75];
    //datePicker.backgroundColor = [UIColor whiteColor];
    //datePicker.tintColor = [ThemeManager themeColorNamed:@"textColour"];
    //[datePicker setValue:[UIColor redColor] forKey:@"textColor"];
    
    [datePicker addTarget:self action:@selector(dateChanged:)
         forControlEvents:UIControlEventValueChanged];
    [datePicker setTag:tag];
    
    return datePicker;
}

- (UIToolbar *)endDateToolbar {
    if (!_endDateToolbar) {
        CGRect frame = CGRectMake(0, self.view.frame.size.height - 304,
                                  self.view.frame.size.width, 44);
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:frame];
        UIBarButtonItem *clearBBI =
        [[UIBarButtonItem alloc] initWithTitle:@"Clear"
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(noEndDate)];
        NSArray *barItems = @[clearBBI];
        [toolbar setItems:barItems animated:YES];
        
        toolbar.backgroundColor = [ThemeManager themeColorNamed:@"subBarTintColour"]; 
        toolbar.tintColor = [ThemeManager themeColorNamed:@"subBarTextColour"];
        
        _endDateToolbar = toolbar;
    }
    return _endDateToolbar;
}

@end
