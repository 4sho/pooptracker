//
//  AppDelegate.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "AppDelegate.h"
#import "SZDPoopEntry.h"
#import "SZDPoopStore.h"
#import "SZDPoopLogViewController.h"
//#import "SZDPoopStatsViewController.h"
#import "SZDPoopCalendarViewController.h"
#import "SZDTabBarViewController.h"
#import "SZDPoopControlButton.h"
#import "SZDPopCircleFloatingButton.h"
#import "SZDStatsViewController.h"
#import "ThemeManager.h"
#import "SZDSettingsViewController.h"

#import "SZDPoopAttributes.h"

@interface AppDelegate ()

@property (strong, nonatomic) UITabBarController *tabBar;
@property (nonatomic) NSInteger tabBarSelectedIndex;
@property (nonatomic, strong) SZDPopCircleFloatingButton *floatingButton;
@property (nonatomic, strong) UITapGestureRecognizer *floatingButtonGR;
@property (nonatomic, strong) UIView *poopActionButtonView;
@end

@implementation AppDelegate
NSString * const AppDelegateRootVCKey = @"AppDelegateRootVCKey";

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"willFinishLaunchingWithOptions");
    return YES;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"didFinishLaunchingWithOptions");
    
    if(!self.window) {
        NSLog(@"State restoration did not restore window.");
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.backgroundColor = [UIColor whiteColor];
    }
    
    if (!self.window.rootViewController) {
        NSLog(@"State restoration did not restore rootViewController");
        SZDTabBarViewController *tabBarController = [[SZDTabBarViewController alloc] init];
        self.window.rootViewController = tabBarController;
    }
    
    // temporarily don't save any of the viewControllers
    NSLog(@"Setting up the tabBarController");
    SZDTabBarViewController *tabBarController = (SZDTabBarViewController *)self.window.rootViewController;
    self.tabBar = tabBarController;
    
    if(!tabBarController.viewControllers) {
        NSLog(@"tabBarController viewControllers set up");
        tabBarController.viewControllers =
         [NSArray arrayWithObjects:
         [tabBarController navigationControllerWithClass:[SZDStatsViewController class] title:@"Stats" image:nil],
         [tabBarController navigationControllerWithClass:[SZDPoopLogViewController class] title:@"Log" image:nil],
         [tabBarController navigationControllerWithClass:[UIViewController class] title:@"Center" image:nil],
         [tabBarController navigationControllerWithClass:[SZDPoopCalendarViewController class] title:@"Calendar" image:nil],
         [tabBarController navigationControllerWithClass:[SZDSettingsViewController class] title:@"Settings" image:nil],
         nil];
        
    }
    
    [[tabBarController.tabBar.items objectAtIndex:2] setEnabled:NO];
    
    tabBarController.selectedIndex = self.tabBarSelectedIndex;
    
    tabBarController.tabBar.barTintColor = [ThemeManager themeColorNamed:@"barTintColour"];
    tabBarController.tabBar.tintColor = [ThemeManager themeColorNamed:@"barTextColour"];
    
    
    UINavigationController *logNC = [tabBarController.viewControllers objectAtIndex:1];
    SZDPoopLogViewController *logVC = [[logNC viewControllers] firstObject];
    
    SZDStatsViewController *statsVC = [[((UINavigationController *)[tabBarController.viewControllers objectAtIndex:0]) viewControllers] firstObject];
    statsVC.logVC = logVC;
    
    SZDPoopCalendarViewController *calVC = [[((UINavigationController *)[tabBarController.viewControllers objectAtIndex:3]) viewControllers] firstObject];
    calVC.logVC = logVC;
    
    NSLog(@"calVCLogvc %@", calVC.logVC);
    
    //UIView *poopActionButtonView = [[UIView alloc] initWithFrame:self.window.frame];
    //[self.window addSubview:poopActionButtonView];
    //poopActionButtonView.backgroundColor = [UIColor yellowColor];
    //poopActionButtonView.alpha = 0.1f;
    //poopActionButtonView.userInteractionEnabled = NO;
    
    // poopactionbutton frame - stick to bottom of screen
    CGFloat poopActionButtonWidth = 50.0f;
    CGRect tabBarFrame = tabBarController.view.frame;
    CGRect poopActionButtonFrame = CGRectMake(tabBarFrame.origin.x + 0.5f * tabBarFrame.size.width - 0.5f * poopActionButtonWidth,
                                              tabBarFrame.origin.y + tabBarFrame.size.height - poopActionButtonWidth,
                                              poopActionButtonWidth, poopActionButtonWidth);
    //CGRect poopActionButtonFrame = CGRectMake(100, 100, 50, 50);
    SZDPoopControlButton *floatingButton = [SZDPoopControlButton buttonWithFrame:poopActionButtonFrame];
    self.floatingButton = floatingButton;
    floatingButton.superViewController = tabBarController;
    //[tabBarController.view addSubview:floatingButton];
    
    //[tabBarController.view addSubview:poopActionButtonView];
    [tabBarController.view addSubview:floatingButton];
    //[floatingButton addTarget:self action:@selector(floatingButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [floatingButton addSubmenuWithRadiusDistance:75.0f
                             startAngleInRadians:0.0f
                               endAngleInRadians:M_PI
                               buttonOnEndpoints:NO
                                   submenuTitles:@[@"Cancel", @"Stop", @"Start"]
                                    titlesHidden:YES
                                   submenuImages:@[@"Cancel.png", @"Stop.png", @"Start.png"]
                             submenuButtonRadius:20.0];
    floatingButton.logVC = logVC;

    //self.floatingButtonGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    
    
    //[poopActionButtonView addGestureRecognizer:self.floatingButtonGR];
    //UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    //[poopActionButtonView.view addGestureRecognizer:gr];
    
    // Initialize all counts and attribute types on app start 
    for (int i = 0; i < SZDPoopAttributesCount; i++) {
        [[SZDPoopStore sharedStore] attributeCount:i];
        [[SZDPoopStore sharedStore] allTypesForAttribute:i];
    }
    
    
    [self.window makeKeyAndVisible];
    return YES;
}

/*
- (void)floatingButtonTouched:(id)sender
{
    // add UIView beneath button to cover existing view and open
    if (self.floatingButton.submenuState == submenuStateOpen) {
        
    }
}

- (void)handleGesture:(id)sender
{
    NSLog(@"handleGesture");
    if (self.floatingButton.submenuState == submenuStateOpen) {
        [self.floatingButton toggleSubmenu];
        [self.poopActionButtonView removeFromSuperview];
        //[self.poopActionButtonView removeGestureRecognizer:self.floatingButtonGR];
        //[self.poopActionButtonView setAlpha:0];
    } else if (self.floatingButt)
}*/

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    BOOL success = [[SZDPoopStore sharedStore] saveChanges];
    if (success) {
        NSLog(@"Saved all of the SZDPoopEntrys");
    } else {
        NSLog(@"Could not save any of the SZDPoopEntrys"); 
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    NSLog(@"shouldSaveApplicationState");
    return YES;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    NSLog(@"shouldRestoreApplicationState");

    return YES;
}

- (void)application:(UIApplication *)application willEncodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"willEncodeRestorableStateWithCoder");
    NSLog(@"encoding window");
    [coder encodeObject:self.window.rootViewController forKey:AppDelegateRootVCKey];
    [coder encodeObject:[NSString stringWithFormat:@"%lu",(unsigned long)self.tabBar.selectedIndex] forKey:@"tabBarSelectedIndex"];
}

- (void)application:(UIApplication *)application didDecodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"didDecodeRestorableStateWithCoder");
    NSLog(@"decoding window");
    
    // Grabs the preserved root view controller.
    UIViewController * vc = [coder decodeObjectForKey:AppDelegateRootVCKey];
    
    NSString *selectedStr = [coder decodeObjectForKey:@"tabBarSelectedIndex"];
    self.tabBarSelectedIndex = [selectedStr intValue];
    
    if (vc) {
        UIWindow * window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        window.rootViewController = vc;
        window.restorationIdentifier = NSStringFromClass([window class]);
        
        // The green color is just to make it obvious if our view didn't load properly.
        // It can be removed when you are finished debugging.
        window.backgroundColor = [UIColor greenColor];
        
        self.window = window;
    }
}


- (UIViewController *)application:(UIApplication *)application
viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents
                            coder:(NSCoder *)coder
{
    NSLog(@"viewControllerWithRestorationIdentifierPath");
    NSLog(@"%@", identifierComponents);
    
    if ([identifierComponents count] == 2) {    // Navigation Controller for our tabbar viewcontrollers
        NSLog(@"Restoring Navigation Controller"); 
        UINavigationController *navController = [[UINavigationController alloc] init];
        navController.restorationIdentifier = [identifierComponents lastObject];
        return navController;
    }
    
    return nil;
}
@end
