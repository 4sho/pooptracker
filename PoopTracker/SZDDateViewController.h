//
//  SZDDateViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-08.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDPoopEntry;

@interface SZDDateViewController : UITableViewController

@property (nonatomic, strong) SZDPoopEntry *entry;

@property (nonatomic, strong) NSDate *unsavedDate;
@property (nonatomic, strong) NSDate *unsavedEndDate;
@property (nonatomic) int32_t unsavedDuration;

@property (nonatomic, copy) void (^dismissBlock)(void);


@end
