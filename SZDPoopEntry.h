//
//  SZDPoopEntry.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-16.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SZDPoopEntry : NSManagedObject /*<NSCoding>*/ 

- (SZDPoopEntry *)copyOfEntry; 

@end

NS_ASSUME_NONNULL_END

#import "SZDPoopEntry+CoreDataProperties.h"
