//
//  SZDEditDateTableViewCell.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-14.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDEditDateTableViewCell.h"
#import "ThemeManager.h"

@implementation SZDEditDateTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    self.dateLabel.textColor = textColour;
    self.timeLabel.textColor = textColour; 
    self.durationLabel.textColor = textColour;
    self.tintColor = textColour;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
