//
//  SZDPieChartViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface SZDPieChartViewController : UIViewController <XYPieChartDelegate, XYPieChartDataSource, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet XYPieChart *pieChart;
// create a pie chart for every attribute 

@property (nonatomic, weak) IBOutlet UICollectionView *attributeSelector;

@property (nonatomic, weak) IBOutlet UICollectionView *pieChartLegend;
//@property (nonatomic, weak) IBOutlet UILabel *pieLegend;

@property (nonatomic, weak) IBOutlet UILabel *noneLabel;
@property (nonatomic, weak) IBOutlet UIButton *toggleNoneButton;
@property (nonatomic, weak) IBOutlet UILabel *totalPoopsLabel;
@end
