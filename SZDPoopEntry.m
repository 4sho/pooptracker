//
//  SZDPoopEntry.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-16.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopEntry.h"
#import "UIKit/UIKit.h"

#import "SZDPoopStore.h"
#import "SZDPoopAttributes.h"

@implementation SZDPoopEntry

// Called when object is initially inserted into the managed context 
- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    self.date = [NSDate date];

    self.endDate = nil;
    self.duration = 0;
    self.tpCount = 0;
    
    SZDPoopStore *pa = [SZDPoopStore sharedStore];
    
    self.type = [[pa allTypesForAttribute:SZDPoopAttributesType] objectAtIndex:0];
    self.colour = [[pa allTypesForAttribute:SZDPoopAttributesColour] objectAtIndex:0];
    self.amount = [[pa allTypesForAttribute:SZDPoopAttributesAmount] objectAtIndex:0];
    self.exertion = [[pa allTypesForAttribute:SZDPoopAttributesExertion] objectAtIndex:0];
    self.smell = [[pa allTypesForAttribute:SZDPoopAttributesSmell] objectAtIndex:0];
    self.fart = [[pa allTypesForAttribute:SZDPoopAttributesFart] objectAtIndex:0];
}

- (SZDPoopEntry *)copyOfEntry
{
    SZDPoopEntry *newEntry = [[SZDPoopEntry alloc] init];
    SZDPoopEntry *entry = self;
    
    newEntry.date = entry.date;
    newEntry.endDate = entry.endDate;
    newEntry.duration = entry.duration;
    newEntry.tpCount = entry.tpCount;
    newEntry.type = entry.type;
    newEntry.colour = entry.colour;
    newEntry.exertion = entry.exertion;
    newEntry.amount = entry.amount;
    newEntry.smell = entry.smell;
    newEntry.fart = entry.fart;
    
    return newEntry;
}

/*
- (void)encodeWithCoder:(NSCoder *)coder
{
    NSLog(@"SZDPoopEntry encodeWithCoder");
    [coder encodeObject:self.date forKey:@"entryDate"];
    [coder encodeInteger:self.type forKey:@"entryType"];
    [coder encodeInteger:self.colour forKey:@"entryColour"];
    [coder encodeInteger:self.execution forKey:@"entryExecution"];
    [coder encodeInteger:self.amount forKey:@"entryAmount"];
    [coder encodeInteger:self.tpCount forKey:@"entryTpCount"];
    [coder encodeObject:self.comments forKey:@"entryComments"];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    NSLog(@"SZDPoopEntry initWithCoder");
    self = [super init];
    if (self) {
        self.date = [coder decodeObjectForKey:@"entryDate"];
        self.type = [coder decodeIntegerForKey:@"entryType"];
        self.colour = [coder decodeIntegerForKey:@"entryColour"];
        self.execution = [coder decodeIntegerForKey:@"entryExecution"];
        self.amount = [coder decodeIntegerForKey:@"entryAmount"];
        self.tpCount = [coder decodeIntegerForKey:@"entryTpCount"];
        self.comments = [coder decodeObjectForKey:@"entryComments"];
    }
    return self;
}
*/



@end
