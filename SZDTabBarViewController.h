//
//  SZDTabBarViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-22.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDPoopEntry;

@interface SZDTabBarViewController : UITabBarController <UIViewControllerRestoration, NSCoding>

@property (nonatomic, strong) UIButton *button;

//@property (nonatomic, strong) SZDPoopEntry *entry;

- (UINavigationController *)navigationControllerWithClass:(Class)vcClass
                                                    title:(NSString *)title
                                                    image:(UIImage *)image;
/*
- (UIButton *) addCenterButtonWithImage:(UIImage *)buttonImage
                         highlightImage:(UIImage *)highlightImage
                            withSubmenu:(NSArray *)submenu
                    startAngleInRadians:(CGFloat)startAngle
                      endAngleInRadians:(CGFloat)endAngle;

- (UIButton *) addCenterButtonWithImage:(UIImage *)mainButtonImage
                       mainButtonRadius:(CGFloat)mainButtonRadius
                    submenuButtonRadius:(CGFloat)submenuButtonRadius
                          submenuRadius:(CGFloat)submenuRadius  // hypotenuse from mainButton center
                          submenuTitles:(NSArray *)submenuTitles
                          submenuImages:(NSArray *)submenuImages
                    startAngleInRadians:(CGFloat)startAngle
                      endAngleInRadians:(CGFloat)endAngle
                     buttonsOnEndpoints:(BOOL)buttonsOnEndpoints;

*/

@end
