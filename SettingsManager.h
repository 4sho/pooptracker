//
//  SettingsManager.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-27.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject

+ (SettingsManager *)sharedManager; 
+ (id)settingFor:(NSString *)key;
+ (BOOL)boolSettingFor:(NSString *)key; 

+ (void)setSettingFor:(NSString *)key withBool:(BOOL)boolValue;
+ (void)setSettingFor:(NSString *)key withObject:(id)object;


@end
