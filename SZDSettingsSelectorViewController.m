//
//  SZDSettingsSelectorViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-24.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDSettingsSelectorViewController.h"
#import "ThemeManager.h"

@interface SZDSettingsSelectorViewController ()

typedef NS_ENUM(NSInteger, SZDTheme)
{
    SZDThemeDefault = 0,
    SZDThemeMidnight
};

@end

@implementation SZDSettingsSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView
                       cellForRowAtIndexPath:indexPath];
    
    NSInteger row = indexPath.row;
    
    if (tableView.tag == 1) {
        NSString *themeName = [ThemeManager getTheme];
        
        if ([themeName isEqualToString:@"default"] && row == SZDThemeDefault) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else if ([themeName isEqualToString:@"theme_midnight"] && row == SZDThemeMidnight) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else if (tableView.tag == 2) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    if (tableView.tag == 1) {
        if (row == SZDThemeDefault) {
            [ThemeManager setTheme:@"default"];
        } else if (row == SZDThemeMidnight) {
            [ThemeManager setTheme:@"theme_midnight"];
        }
    }
    else if (tableView.tag == 2) {
        [ThemeManager setIconTheme:@"iconTheme_scribble"];
    }
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UILabel *footer = [[UILabel alloc] init];
    footer.textAlignment = NSTextAlignmentCenter;
    footer.numberOfLines = 5;
    footer.lineBreakMode = NSLineBreakByWordWrapping;
    footer.text = @"Please restart the app for the theme to take into effect.";
    footer.font = [UIFont systemFontOfSize:14.0f];
    return footer;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
