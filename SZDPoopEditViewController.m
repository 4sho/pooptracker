//
//  SZDPoopEditViewController2.m
//  PoopTracker
//
//  Created by Sandy House on 2016-03-01.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopEditViewController.h"
#import "SZDPoopEntry.h"
#import "SZDPoopStore.h"
#import "SZDPoopTypeViewController.h"
#import "SZDDateViewController.h"
#import "SZDPoopAttributes.h"
#import "SZDEditDateTableViewCell.h"
#import "ThemeManager.h"

@interface SZDPoopEditViewController ()

typedef NS_ENUM(NSInteger, editSectionIndex)
{
    editSectionIndexDate = 0,
    editSectionIndexTpCount,
    editSectionIndexTypeColour,
    editSectionIndexDetails,
    editSectionIndexComments,
    editSectionIndexDelete
};

typedef NS_ENUM(NSInteger, editSectionIndexDetailsControl)
{
    editSectionIndexDetailsControlExertion = 0,
    editSectionIndexDetailsControlAmount,
    editSectionIndexDetailsControlSmell,
    editSectionIndexDetailsControlFart,
    
    editSectionIndexDetailsControlCount
};

//@property (nonatomic, strong) SZDPoopEntry *editingEntry;

@property (nonatomic, strong) NSDictionary *sectionDictionary;

@property (nonatomic, strong) UITextField *tpCountField;
@property (nonatomic, strong) UISegmentedControl *exertionControl;
@property (nonatomic, strong) UISegmentedControl *amountControl;
@property (nonatomic, strong) UISegmentedControl *smellControl;
@property (nonatomic, strong) UISegmentedControl *fartControl;

@property (nonatomic, strong) UITextView *commentsField;

@property (nonatomic, strong) NSArray *detailControls;

@property (nonatomic, strong) NSDate *originalDate;
@property (nonatomic, strong) NSDate *originalEndDate;
@property (nonatomic, strong) NSDate *originalDuration;

@property (nonatomic, strong) NSDate *unsavedDate;
@property (nonatomic, strong) NSDate *unsavedEndDate;
@property (nonatomic) int32_t unsavedDuration;
@property (nonatomic) int32_t unsavedTPCount;
@property (nonatomic, strong) NSMutableArray *unsavedAttributes;
@property (nonatomic) NSString *unsavedComments;

@property (nonatomic) BOOL isNewEntry;

@property (nonatomic) BOOL dirty;

// View Controllers
@property (nonatomic, strong) SZDDateViewController *dvc;
@property (nonatomic, strong) SZDPoopTypeViewController *ptvc;
@property (nonatomic, strong) SZDPoopTypeViewController *pcvc;


@end

@implementation SZDPoopEditViewController
@synthesize sectionDictionary = _sectionDictionary;
@synthesize detailControls = _detailControls;
@synthesize unsavedAttributes = _unsavedAttributes;

- (NSMutableArray *)unsavedAttributes
{
    if (!_unsavedAttributes) {
        _unsavedAttributes = [[NSMutableArray alloc] init];
    }
    return _unsavedAttributes;
}

- (NSDictionary *)sectionDictionary
{
    if (!_sectionDictionary) {
        //NSLog(@"@@creating section dictionary");
        // Key: section index
        // Value: numberOfRows, header,
        _sectionDictionary =
        @{ [NSNumber numberWithInteger:editSectionIndexDate] : @[@1, @"Date"],
           [NSNumber numberWithInteger:editSectionIndexTpCount] : @[@1, @"Toilet Paper"],
           [NSNumber numberWithInteger:editSectionIndexTypeColour] : @[@2, @"Characteristics"],
           [NSNumber numberWithInteger:editSectionIndexDetails] : @[@(editSectionIndexDetailsControlCount), @"Details"],
           [NSNumber numberWithInteger:editSectionIndexComments] : @[@1, @"Comments"],
           [NSNumber numberWithInteger:editSectionIndexDelete] : @[@1, @""]
           };
        
    }
    
    return _sectionDictionary;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"EditVC didLoad");
    
    
    UINib *nib = [UINib nibWithNibName:@"SZDEditDateTableViewCell"
                                bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:@"SZDEditDateTableViewCell"];
    

    UIColor *barTintColour = [ThemeManager themeColorNamed:@"barTintColour"];
    UIColor *barTextColour = [ThemeManager themeColorNamed:@"barTextColour"];
    UIColor *backgroundColour = [ThemeManager themeColorNamed:@"backgroundColour"];
    UIColor *tableSeparatorColour = [ThemeManager themeColorNamed:@"tableSeparatorColour"];
    UIColor *tintColour = [ThemeManager themeColorNamed:@"tintColour"];
    
    self.navigationController.navigationBar.barTintColor = barTintColour;
    self.navigationController.navigationBar.tintColor = barTextColour;
    self.navigationController.view.backgroundColor = backgroundColour;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : barTextColour};

    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = tableSeparatorColour;
    
    self.tableView.tintColor = tintColour;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(done)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
}

/********************************************************************
 *  Initializers
 ********************************************************************/
- (instancetype)initWithEntry:(SZDPoopEntry *)entry isNewEntry:(BOOL)isNewEntry
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    
    self.navigationItem.title = @"Edit Poop";
    
    UIBarButtonItem *doneEntry = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                  target:self
                                  action:@selector(save:)];
    UIBarButtonItem *cancelEntry;
    
    cancelEntry = [[UIBarButtonItem alloc]
                   initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                   target:self
                   action:@selector(cancel)];
    /*
    if (isNewEntry) {   // remove object on cancel
        // Warn them before removing entry
        cancelEntry = [[UIBarButtonItem alloc]
                       initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                       target:self
                       action:@selector(cancelNewEntry:)];
        
    } else {    // do not remove object on cancel
        cancelEntry = [[UIBarButtonItem alloc]
                       initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                       target:self
                       action:@selector(cancel:)];
    }*/
    
    self.navigationItem.rightBarButtonItem = doneEntry;
    self.navigationItem.leftBarButtonItem = cancelEntry;
    
    self.entry = entry;
    self.isNewEntry = isNewEntry;
    
    // Initial unsaved data
    self.unsavedDate = entry.date;
    self.unsavedEndDate = entry.endDate;
    self.unsavedDuration = entry.duration;
    
    self.unsavedTPCount = entry.tpCount;
    
    for (NSInteger i = 0; i < SZDPoopAttributesCount; i++) {
        SEL selector = NSSelectorFromString([[SZDPoopAttributes sharedInstance] objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectKey forAttribute:i]);
        NSManagedObject *object = [entry performSelector:selector];
        [self.unsavedAttributes setObject:object atIndexedSubscript:i];
    }
    
    self.unsavedComments = entry.comments;

    self.dirty = NO;
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    @throw [NSException exceptionWithName:@"Wrong Initializer"
                                   reason:@"Use initWithEntry:isNewEntry:"
                                 userInfo:nil];
}

/********************************************************************
 *  Table Methods
 *  Methods for creating the EditViewController's table
 ********************************************************************/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionDictionary count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.sectionDictionary objectForKey:[NSNumber numberWithInteger:section]];
    NSInteger numberOfRows = [[sectionArray objectAtIndex:0] integerValue];
    return numberOfRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.sectionDictionary objectForKey:[NSNumber numberWithInteger:section]];
    NSString *headerTitle = [sectionArray objectAtIndex:1];
    
    return headerTitle;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UIColor *headerTextColour = [ThemeManager themeColorNamed:@"headerTextColour"];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:headerTextColour];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.section == editSectionIndexDate) {
        return 56.0;
    }
    if (indexPath.section == editSectionIndexComments) {
        return 100.0;
    }
    if (indexPath.section == editSectionIndexDetails) {
        return 56.0;
    }
    
    if (indexPath.section == editSectionIndexDelete) {
        return 50.0;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if (indexPath.section == editSectionIndexDate) {    // custom
        // Set date, time, duration
        SZDEditDateTableViewCell *dateCell = [tableView dequeueReusableCellWithIdentifier:@"SZDEditDateTableViewCell" forIndexPath:indexPath];
        
        dateCell.dateLabel.text = [NSDateFormatter localizedStringFromDate:self.unsavedDate
                                                                 dateStyle:NSDateFormatterLongStyle
                                                                 timeStyle:NSDateFormatterNoStyle];
        NSString *startTime = [NSDateFormatter localizedStringFromDate:self.unsavedDate
                                                             dateStyle:NSDateFormatterNoStyle
                                                             timeStyle:NSDateFormatterShortStyle];
        NSString *endTime;
        if (self.unsavedEndDate) {
            endTime = [NSDateFormatter localizedStringFromDate:self.unsavedEndDate
                                                               dateStyle:NSDateFormatterNoStyle
                                                               timeStyle:NSDateFormatterShortStyle];
            dateCell.timeLabel.text = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
            dateCell.durationLabel.text = [NSString stringWithFormat:@"Duration: %dm", self.unsavedDuration];
        } else {
            dateCell.timeLabel.text = [NSString stringWithFormat:@"%@", startTime];
            dateCell.durationLabel.text = @"No duration";
        }
        
        
        dateCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return dateCell;
    }
    else if (indexPath.section == editSectionIndexTpCount) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"tpCount"];
        cell.textLabel.text = @"Number of Squares";
        //cell.detailTextLabel.text = @"666"; // TP Count
        
        UITextField *tpCountTextField = [[UITextField alloc] init];
        tpCountTextField.frame = cell.frame;
        tpCountTextField.bounds = CGRectMake(tpCountTextField.frame.origin.x - 100, 0, 75, 40);
        tpCountTextField.placeholder = @"-";
        tpCountTextField.textAlignment = NSTextAlignmentRight;
        tpCountTextField.keyboardType = UIKeyboardTypeNumberPad;
        cell.accessoryView = tpCountTextField;
        
        NSArray *barbuttonItems = [self barButtonItemsWithClearSelector:@selector(clear:)
                                                           doneSelector:@selector(done)];
        tpCountTextField.inputAccessoryView = [self toolbarWithBarButtonItems:barbuttonItems];
        
        UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
        tpCountTextField.textColor = textColour;
        
        if (self.unsavedTPCount > 0) {
            tpCountTextField.text = [NSString stringWithFormat:@"%d", self.unsavedTPCount];
        }
        
        self.tpCountField = tpCountTextField;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == editSectionIndexTypeColour) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"TypeColour"];
        if(indexPath.row == 0) {    // type
            cell.textLabel.text = @"Type";
            // image
            UIImage *icon = [[[self unsavedAttribute:SZDPoopAttributesType] valueForKey:@"image"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:icon];
            imageView.frame = CGRectMake(0, 2, 40, 40);
            cell.accessoryView = imageView;
        } else if (indexPath.row == 1) {    // colour
            cell.textLabel.text = @"Colour";
            // image
            UIImage *icon = [[[self unsavedAttribute:SZDPoopAttributesColour] valueForKey:@"image"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:icon];
            imageView.frame = CGRectMake(0, 2, 40, 40);
            cell.accessoryView = imageView;
            
            imageView.tintColor = [[self unsavedAttribute:SZDPoopAttributesColour] valueForKey:@"colour"];
        }
    }
    
    else if (indexPath.section == editSectionIndexDetails) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISegmentedControl *segmentedControl = [self loadSegmentedControlForDetailAttribute:indexPath.row];
        [cell addSubview:segmentedControl];

        NSInteger index = indexPath.row + SZDPoopAttributesExertion;    // exclude Type, Colour
        NSString *title = [[[[SZDPoopAttributes sharedInstance] attributeDictionary] objectForKey:[NSNumber numberWithInteger:index]] objectAtIndex:SZDPoopAttributeDictionaryObjectTitle];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ Level", title];
    }
    
    else if (indexPath.section == editSectionIndexComments) {
        
        //www.howlin-interactive.com/2013/01/creating-a-self-sizing-uitextview-within-a-uitableviewcell-in-ios-6/
        UITextView *comments = [[UITextView alloc] init];
        comments.frame = CGRectMake(0, 0, self.view.frame.size.width, 100);
        comments.bounds = CGRectMake(16, 16, comments.frame.size.width - 32, comments.frame.size.height - 32);
        [cell addSubview:comments];
        
        comments.text = self.unsavedComments;
        
        NSArray *barbuttonItems = [self barButtonItemsWithClearSelector:@selector(clear:)
                                                           doneSelector:@selector(done)];
        comments.inputAccessoryView = [self toolbarWithBarButtonItems:barbuttonItems];
        
        self.commentsField = comments;
        
        comments.backgroundColor = [UIColor clearColor];
        comments.textColor = [ThemeManager themeColorNamed:@"textColour"];
        comments.layer.borderWidth = 1.0f;
        comments.layer.borderColor = [[ThemeManager themeColorNamed:@"textColour"] CGColor];
        comments.layer.cornerRadius = 5.0f;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    else if (indexPath.section == editSectionIndexDelete) {
        
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        deleteButton.frame = cell.frame;
        deleteButton.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
        deleteButton.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:59.0/255.0 blue:48.0/255.0 alpha:1.0];
        [deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
        deleteButton.titleLabel.textColor = [UIColor whiteColor];
        [deleteButton addTarget:self action:@selector(deleteEntryViaButton) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:deleteButton];
        
    }
    
    UIColor *textColour = [ThemeManager themeColorNamed:@"textColour"];
    cell.textLabel.textColor = textColour;
    cell.detailTextLabel.textColor = textColour;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (NSArray *)detailControls
{
    if (!_detailControls) {
        NSLog(@"Initializing detailControls");
        NSMutableArray *detailControls = [[NSMutableArray alloc] init];
        
        SZDPoopStore *ps = [SZDPoopStore sharedStore];
        
        for (int i = 0; i < editSectionIndexDetailsControlCount; i++) {
            UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] init];
            segmentedControl.frame = CGRectMake(self.view.frame.size.width - 170, 3, 150, 50);
            NSInteger attribute = i + SZDPoopAttributesExertion;
            [self setUpSegmentedControl:segmentedControl attributeTypesArray:[ps allTypesForAttribute:attribute]];
            
            [detailControls insertObject:segmentedControl atIndex:i];
        }
        _detailControls = detailControls;
    }
    
    return _detailControls;
}

- (UISegmentedControl *)loadSegmentedControlForDetailAttribute:(NSInteger)detailAttribute
{
    UISegmentedControl *segmentedControl = [self.detailControls objectAtIndex:detailAttribute];
    NSInteger attribute = detailAttribute + SZDPoopAttributesExertion;
    NSInteger selectedSegmentIndex = [self segmentedIndexFor:[self unsavedAttribute:attribute]];
    segmentedControl.selectedSegmentIndex = selectedSegmentIndex;
    return segmentedControl;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == editSectionIndexDate) {
        if (!_dvc) {
            NSLog(@"Initializing dvc.");
            SZDDateViewController *dvc = [[SZDDateViewController alloc] init];
            dvc.entry = self.entry;
            
            // Tell dvc the initial unsaved data
            dvc.unsavedDate = self.unsavedDate;
            dvc.unsavedEndDate = self.unsavedEndDate;
            dvc.unsavedDuration = self.unsavedDuration;
            
            dvc.dismissBlock = ^{
                // save dvc's modified unsaved data
                self.unsavedDate = _dvc.unsavedDate;
                self.unsavedEndDate = _dvc.unsavedEndDate;
                self.unsavedDuration = _dvc.unsavedDuration;
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:editSectionIndexDate] withRowAnimation:UITableViewRowAnimationNone];
            };
            _dvc = dvc;
        }
        
        [self.navigationController pushViewController:_dvc animated:YES];
    }
    else if (indexPath.section == editSectionIndexTpCount) {
        [self.tpCountField becomeFirstResponder];
    }
    else if (indexPath.section == editSectionIndexTypeColour) {
        if (indexPath.row == 0) {
            if (!_ptvc) {
                NSLog(@"Initializing ptvc");
                SZDPoopTypeViewController *ptvc = [[SZDPoopTypeViewController alloc] init];
                ptvc.entry = self.entry;
                ptvc.attribute = SZDPoopAttributesType;
                ptvc.unsavedType = [self unsavedAttribute:SZDPoopAttributesType];
                ptvc.dismissBlock = ^{
                    [self setUnsavedAttribute:_ptvc.unsavedType attribute:SZDPoopAttributesType];
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:editSectionIndexTypeColour] withRowAnimation:UITableViewRowAnimationNone];
                };
                _ptvc = ptvc;
            }
            
            [self.navigationController pushViewController:_ptvc animated:YES];
        } else if (indexPath.row == 1) {
            if (!_pcvc) {
                NSLog(@"Initializing pcvc");
                SZDPoopTypeViewController *pcvc = [[SZDPoopTypeViewController alloc] init];
                pcvc.entry = self.entry;
                pcvc.attribute = SZDPoopAttributesColour; 
                pcvc.unsavedType = [self unsavedAttribute:SZDPoopAttributesColour];
                pcvc.dismissBlock = ^{
                    [self setUnsavedAttribute:_pcvc.unsavedType attribute:SZDPoopAttributesColour];
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:editSectionIndexTypeColour] withRowAnimation:UITableViewRowAnimationNone];
                };
                
                _pcvc = pcvc;
            }
            
            [self.navigationController pushViewController:_pcvc animated:YES];
        }
    }
}

/********************************************************************
 *  Saving Operations
 *  Operations related to saving/cancelling via the EditViewController
 ********************************************************************/
- (void)closeWithChanges:(BOOL)withDismissBlock
{
    if (withDismissBlock) {
        NSLog(@"Calling dismiss block");
        [self.presentingViewController dismissViewControllerAnimated:YES completion:self.dismissBlock];
    } else {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)delete
{
    [[SZDPoopStore sharedStore] removeEntry:self.entry];
}


- (void)deleteEntryViaButton
{
    NSString *deleteTitle;
    NSString *deleteMessage;
    if (self.isNewEntry) {
        deleteTitle = @"Delete new entry?";
        deleteMessage = @"This entry will not be saved.";
    } else {
        deleteTitle = @"Delete entry?";
        deleteMessage = @"This entry will be deleted forever.";
    }
    
    // confirmation alert
    UIAlertController *deleteConfirm =
    [UIAlertController alertControllerWithTitle:deleteTitle
                                        message:deleteMessage
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self delete];
        if (self.isNewEntry) {
            [self closeWithChanges:NO];
        } else {
            [self closeWithChanges:YES];
        }
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}];
    
    [deleteConfirm addAction:delete];
    [deleteConfirm addAction:cancel];
    [self presentViewController:deleteConfirm animated:YES completion:nil];
}

- (void)cancelWarning:(void (^)(UIAlertAction *action))handler
{
    UIAlertController* alert =
    [UIAlertController alertControllerWithTitle:@"Are you sure you want to cancel?"
                                        message:@"Your changes will not be saved."
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* stay =
    [UIAlertAction actionWithTitle:@"Stay here" style:UIAlertActionStyleDefault
                           handler:nil];
    UIAlertAction* leave =
    [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:handler];
    
    [alert addAction:leave];
    [alert addAction:stay];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)cancel
{
    if ([self isDirty]) {
        [self cancelWarning:^(UIAlertAction *action) {
            // Remove the already created entry
            if (self.isNewEntry) {
                [self delete];
            }
            [self closeWithChanges:NO];
        }];
    } else {
        if (self.isNewEntry) {
            [self delete];
        }
        [self closeWithChanges:NO];
    }
}


- (BOOL)isDirty
{
    SZDPoopEntry *entry = self.entry;
    
    // update the unsaved attributes
    [self setUnsavedAttributesForAllDetailControls];
    
    // check if any of the values have changed
    if (![self isSameDateWithDate1:self.unsavedDate date2:entry.date] ||
        ![self isSameDateWithDate1:self.unsavedEndDate date2:entry.endDate] ||
        self.unsavedDuration != entry.duration ||
        self.unsavedTPCount != entry.tpCount ||
        ![[self unsavedAttribute:SZDPoopAttributesType] isEqual:entry.type] ||
        ![[self unsavedAttribute:SZDPoopAttributesColour] isEqual:entry.colour] ||
        ![[self unsavedAttribute:SZDPoopAttributesExertion] isEqual:entry.exertion] ||
        ![[self unsavedAttribute:SZDPoopAttributesAmount] isEqual:entry.amount] ||
        ![[self unsavedAttribute:SZDPoopAttributesSmell] isEqual:entry.smell] ||
        ![[self unsavedAttribute:SZDPoopAttributesFart] isEqual:entry.fart] ||
        self.unsavedComments != entry.comments)
    {
        return YES;
    }
    return NO;
}

- (void)save:(id)sender
{
    NSLog(@"Saving PoopEntry");
    
    SZDPoopStore *ps = [SZDPoopStore sharedStore];
    SZDPoopEntry *entry = self.entry;
    
    [self setUnsavedAttributesForAllDetailControls]; // values are not unsaved until now
    [self done];    // to resign keyboard and save what's in the textfield

    // one method to update them all
    [ps updateEntry:entry
         entryState:SZDEntryStateExisting
               date:self.unsavedDate
            endDate:self.unsavedEndDate
           duration:self.unsavedDuration
            tpCount:self.unsavedTPCount
           comments:self.unsavedComments
         attributes:self.unsavedAttributes];
    
    [self closeWithChanges:YES];
}

- (void)clear:(id)sender
{
    NSLog(@"clear sender: %@", sender);
    
    if ([self.commentsField isFirstResponder]) {
        self.commentsField.text = @"";
    } else if ([self.tpCountField isFirstResponder]) {
        self.tpCountField.text = @"";
    }
}

- (void)done
{
    if ([self.commentsField isFirstResponder]) {
        self.unsavedComments = self.commentsField.text;
    } else if ([self.tpCountField isFirstResponder]) {
        self.unsavedTPCount = [self.tpCountField.text intValue];
    }
    [self.view endEditing:YES];
}


/********************************************************************
 *  Helper functions
 ********************************************************************/
- (void)setUpSegmentedControl:(UISegmentedControl *)segmentedControl
          attributeTypesArray:(NSArray *)attributeTypesArray
{
    //NSLog(@"Setting up segmented Control");
    for(int i = 1; i < [attributeTypesArray count]; i++) {
        NSManagedObject *type = attributeTypesArray[i];
        UIImage *image = [[type valueForKey:@"image"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        NSString *title = [type valueForKey:@"title"];
        
        if (![image isEqual:[NSNull null]]) {
            [segmentedControl insertSegmentWithImage:image atIndex:i animated:NO];
        } else {
            [segmentedControl insertSegmentWithTitle:title atIndex:i animated:NO];
        }
    }
    
}

- (UIToolbar *)toolbarWithBarButtonItems:(NSArray *)barButtonItems
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolbar setItems:barButtonItems];
    return toolbar;
}

- (NSArray *)barButtonItemsWithClearSelector:(SEL)clearSelector
                                doneSelector:(SEL)doneSelector
{
    UIBarButtonItem *clear = [[UIBarButtonItem alloc]
                              initWithTitle:@"Clear"
                              style:UIBarButtonItemStyleDone
                              target:self action:clearSelector];
    UIBarButtonItem *flexBBI = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:self
                                action:nil];
    UIBarButtonItem *doneBBI = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                target:self
                                action:doneSelector];
    return @[clear, flexBBI, doneBBI];
    
}

- (NSInteger)segmentedIndexFor:(NSManagedObject *)object
{
    return [[object valueForKey:@"index"] integerValue] - 1;
}

- (NSManagedObject *)objectForSegmentedIndex:(NSInteger)segmentedIndex attribute:(NSInteger)attribute
{
    NSInteger objectIndex = segmentedIndex + 1;
    return [[[SZDPoopStore sharedStore] allTypesForAttribute:attribute] objectAtIndex:objectIndex];
}

- (void)setUnsavedAttribute:(NSManagedObject *)object attribute:(NSInteger)attribute
{
    [self.unsavedAttributes setObject:object atIndexedSubscript:attribute];
}

- (NSManagedObject *)unsavedAttribute:(NSInteger)attribute
{
    return [self.unsavedAttributes objectAtIndex:attribute];
}

- (void)setUnsavedAttributesForAllDetailControls
{
    // update segmentedControl attributes
    for (int i = SZDPoopAttributesExertion; i < SZDPoopAttributesCount; i++) {
        NSInteger detailControlIndex = i - SZDPoopAttributesExertion;
        NSInteger attribute = i;
        NSInteger segmentedIndex = [[self.detailControls objectAtIndex:detailControlIndex] selectedSegmentIndex];
        NSManagedObject *object = [self objectForSegmentedIndex:segmentedIndex attribute:attribute];
        [self setUnsavedAttribute:object attribute:attribute];
    }
}

// minute precision
- (BOOL)isSameDateWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    if (date1 == nil && date2 == nil) {
        return YES;
    }
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 minute] == [comp2 minute] &&
    [comp1 day] == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}



@end
