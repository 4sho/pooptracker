//
//  SZDPoopEditViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-12.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDPoopEntry;

@interface SZDPoopEditViewControllerGARBAGE : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) SZDPoopEntry *entry;    // the entry it is editing

@property (nonatomic, copy) void (^dismissBlock)(void); 

@property (weak, nonatomic) IBOutlet UITextField *dateField;
@property (weak, nonatomic) IBOutlet UITextField *endDateField; 
@property (weak, nonatomic) IBOutlet UITextField *durationField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeControl;
@property (weak, nonatomic) IBOutlet UILabel *typeDescription;
@property (weak, nonatomic) IBOutlet UISegmentedControl *colourControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *executionControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *amountControl;
@property (weak, nonatomic) IBOutlet UITextField *tpCountField;
@property (weak, nonatomic) IBOutlet UITextView *commentField;

//@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

- (instancetype)initWithEntry:(SZDPoopEntry *)entry isNewEntry:(BOOL)isNewEntry;

@end
