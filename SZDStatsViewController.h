//
//  SZDStatsViewController.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-17.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SZDFiguresViewController, SZDPieChartViewController, SZDPoopLogViewController;

@interface SZDStatsViewController : UIViewController

@property (nonatomic, strong) SZDFiguresViewController *figuresViewController;
@property (nonatomic, strong) SZDPieChartViewController *pieChartsViewController;

@property (nonatomic, strong) UISegmentedControl *segmentedControl; 

@property (nonatomic, strong) SZDPoopLogViewController *logVC; 

@end
