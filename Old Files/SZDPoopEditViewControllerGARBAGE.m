//
//  SZDPoopEditViewController.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-12.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopEditViewControllerGARBAGE.h"
#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "SZDPoopAttributes.h"

@interface SZDPoopEditViewControllerGARBAGE ()

// Record changes in current Edit, so "Cancel" can restore the last "Done"d change.
@property (strong, nonatomic) NSDate *unsavedDate;
@property (strong, nonatomic) NSDate *unsavedEndDate;
@property (nonatomic) int32_t unsavedDuration;
@property (nonatomic) int32_t unsavedTpCount;
@property (strong, nonatomic) NSString *unsavedComment;

@end


@implementation SZDPoopEditViewControllerGARBAGE

//@synthesize scrollView;

- (instancetype)initWithEntry:(SZDPoopEntry *)entry isNewEntry:(BOOL)isNewEntry
{
    self = [super initWithNibName:nil bundle:nil];
    
    self.navigationItem.title = @"Edit Poop";
    
    UIBarButtonItem *doneEntry = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                  target:self
                                  action:@selector(save:)];
    UIBarButtonItem *cancelEntry;
    
    if (isNewEntry) {   // remove object on cancel
        // Warn them before removing entry
        cancelEntry = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                        target:self
                                        action:@selector(cancelNewEntry:)];
        
        // Calculate duration
        if (entry.endDate) {
            entry.duration = [self calculateDurationFrom:entry.date to:entry.endDate];
        }
    } else {    // do not remove object on cancel
        cancelEntry = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                        target:self
                                        action:@selector(cancel:)];
    }
    
    self.navigationItem.rightBarButtonItem = doneEntry;
    self.navigationItem.leftBarButtonItem = cancelEntry;
    
    self.entry = entry;
    
    
    return self;
}

// Old designated initializer. Do not use this as we NEED to know if it is a new entry or not
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    @throw [NSException exceptionWithName:@"Wrong Initializer"
                                   reason:@"Use initWithEntry:isNewEntry:"
                                 userInfo:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // datePicker for dateField
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    UIDatePicker *endDatePicker = [[UIDatePicker alloc] init];
    datePicker.date = self.entry.date;
    if(self.entry.endDate) {
        endDatePicker.date = self.entry.endDate;
    } else {
        endDatePicker.date = self.entry.date;
    }
    self.dateField.inputView = datePicker;
    self.dateField.inputAccessoryView = [self toolbarWithCancelSelector:@selector(datePickerCancel:)
                                                        andDoneSelector:@selector(keyboardDone:)];
    self.endDateField.inputView = endDatePicker;
    self.endDateField.inputAccessoryView = [self toolbarWithCancelSelector:@selector(datePickerCancel:)
                                                           andDoneSelector:@selector(keyboardDone:)];
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [endDatePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    // tpCount NumberPad
    self.tpCountField.inputAccessoryView = [self toolbarWithCancelSelector:@selector(numberPadCancel:)
                                                           andDoneSelector:@selector(keyboardDone:)];
    
    // comment Keyboard
    self.commentField.inputAccessoryView = [self toolbarWithCancelSelector:@selector(keyboardCancel:)
                                                           andDoneSelector:@selector(keyboardDone:)];
    
    // Segmented Controls height
    CGRect frame = self.typeControl.frame;
    [self.typeControl setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 200)];
    
    // Segmented Controls - dynamically add the segments
    SZDPoopAttributes *pa = [SZDPoopAttributes sharedInstance];
    // call method to return array of strings xor images to set up segmentedcontrol
    
    [self setUpSegmentedControl:self.typeControl withArray:[pa objectsOfType:SZDAttributeObjectTypeImage forAttribute:SZDPoopAttributesType]];
    [self setUpSegmentedControl:self.colourControl withArray:[pa objectsOfType:SZDAttributeObjectTypeColour forAttribute:SZDPoopAttributesColour]];
    [self setUpSegmentedControl:self.executionControl withArray:[pa objectsOfType:SZDAttributeObjectTypeTitle forAttribute:SZDPoopAttributesExecution]];
    [self setUpSegmentedControl:self.amountControl withArray:[pa objectsOfType:SZDAttributeObjectTypeTitle forAttribute:SZDPoopAttributesAmount]];

    [self.typeControl addTarget:self
                         action:@selector(typeControlChanged:)
               forControlEvents:UIControlEventValueChanged];
    
    // Comment TextField - border
    self.commentField.layer.borderWidth = 0.5f;
    self.commentField.layer.borderColor = [[UIColor grayColor] CGColor];
    self.commentField.layer.cornerRadius = 8;
    
    
    SZDPoopEntry *entry = self.entry;
    self.unsavedDate = entry.date;
    self.unsavedEndDate = entry.endDate;
    self.unsavedDuration = entry.duration;
    self.unsavedTpCount = entry.tpCount;
    self.unsavedComment = entry.comments;
    
}


- (void)setUpSegmentedControl:(UISegmentedControl *)segmentedControl
                    withArray:(NSArray *)segmentArray
{
    [segmentedControl removeAllSegments];
    
    for (int i = 0; i < [segmentArray count]; i++) {
        if([segmentArray[i] isKindOfClass:[NSString class]]) {
            [segmentedControl insertSegmentWithTitle:segmentArray[i] atIndex:i animated:NO];
        } else if ([segmentArray[i] isKindOfClass:[UIImage class]]) {
            [segmentedControl insertSegmentWithImage:segmentArray[i] atIndex:i animated:NO];
        } else if ([segmentArray[i] isKindOfClass:[UIColor class]]) {
            [segmentedControl insertSegmentWithTitle:@"" atIndex:i animated:NO];
            segmentedControl.subviews[i].backgroundColor = segmentArray[i];
            segmentedControl.tintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        } else {
            @throw [NSException exceptionWithName:@"setUpSegmentedControl:withArray:"
                                           reason:@"Object in array of invalid class. Should be NSString or UIImage."
                                         userInfo:nil];
        }
    }
}

- (void)save:(id)sender
{
    SZDPoopEntry *entry = self.entry;
    
    // Update the entry with all of the user-set fields
    
    // if the date has changed, re-insert it into the sharedStore in the correct position
    NSDate *date = [self.dateFormatter dateFromString:self.dateField.text];
    NSDate *endDate = [self.dateFormatter dateFromString:self.endDateField.text];
    if (date != entry.date) {   // date has changed
        NSLog(@"Changed Date: %@", [self.dateFormatter dateFromString:self.dateField.text]);
        entry.date = date;
        
        [[SZDPoopStore sharedStore] reInsertEntry:entry];
    }
    entry.endDate = endDate;
    
    if([self.durationField.text length] == 0) {
        entry.duration = -1;
    } else {
        entry.duration = [self.durationField.text intValue];
    }
    
    entry.type = (int)self.typeControl.selectedSegmentIndex;
    entry.colour = (int)self.colourControl.selectedSegmentIndex;
    entry.execution = (int)self.executionControl.selectedSegmentIndex;
    entry.amount = (int)self.amountControl.selectedSegmentIndex;
    
    if([self.tpCountField.text length] == 0) {
        entry.tpCount = -1;
    } else {
        entry.tpCount = [self.tpCountField.text intValue];
    }
    entry.comments = self.commentField.text;
    
    [self.presentingViewController
     dismissViewControllerAnimated:YES
     completion:self.dismissBlock];
}

- (void)cancel:(id)sender
{
    [self.presentingViewController
     dismissViewControllerAnimated:YES
     completion:self.dismissBlock];
}

- (void)cancelNewEntry:(id)sender
{
    [[SZDPoopStore sharedStore] removeEntry:self.entry];
    
    [self cancel:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    SZDPoopEntry *entry = self.entry;
    
    // Update the fields with all of the entry's properties
    self.dateField.text = [NSDateFormatter localizedStringFromDate:entry.date
                                                         dateStyle:NSDateFormatterShortStyle
                                                         timeStyle:NSDateFormatterShortStyle];
    if (entry.endDate) {
        self.endDateField.text = [NSDateFormatter localizedStringFromDate:entry.endDate
                                                                dateStyle:NSDateFormatterShortStyle
                                                                timeStyle:NSDateFormatterShortStyle];
        
    }
    
    if (entry.duration != -1) {
        self.durationField.text = [@(entry.duration) stringValue];
    }
    
    self.typeControl.selectedSegmentIndex = entry.type;
    self.colourControl.selectedSegmentIndex = entry.colour;
    self.executionControl.selectedSegmentIndex = entry.execution;
    self.amountControl.selectedSegmentIndex = entry.amount;
    
    if(entry.tpCount != -1) {   // default value
        self.tpCountField.text = [@(entry.tpCount) stringValue];
    }
    self.commentField.text = entry.comments;

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}


- (void)datePickerValueChanged:(id)sender
{
    UIDatePicker *datePicker = (UIDatePicker *)sender;
    
    NSLog(@"datePickerValueChanged: %lu", (long)datePicker.tag);
    
    if([self.dateField isFirstResponder]) { // date
        self.dateField.text = [NSDateFormatter localizedStringFromDate:[datePicker date]
                                                             dateStyle:NSDateFormatterShortStyle
                                                             timeStyle:NSDateFormatterShortStyle];
        if(self.unsavedEndDate) {
            self.durationField.text = [NSString stringWithFormat:@"%d", [self calculateDurationFrom:[datePicker date] to:self.unsavedEndDate]];
        }
    } else if ([self.endDateField isFirstResponder]) { // endDate
        self.endDateField.text = [NSDateFormatter localizedStringFromDate:[datePicker date]
                                                             dateStyle:NSDateFormatterShortStyle
                                                            timeStyle:NSDateFormatterShortStyle];
        self.durationField.text = [NSString stringWithFormat:@"%d", [self calculateDurationFrom:self.unsavedDate to:[datePicker date]]];
    }
    // Duration
}
- (void)datePickerCancel:(id)sender
{
    if([self.dateField isFirstResponder]) { // date
        self.dateField.text = [NSDateFormatter localizedStringFromDate:self.unsavedDate
                                                             dateStyle:NSDateFormatterShortStyle
                                                             timeStyle:NSDateFormatterShortStyle];
    } else if ([self.endDateField isFirstResponder]) { // endDate
        self.endDateField.text = [NSDateFormatter localizedStringFromDate:self.unsavedEndDate
                                                             dateStyle:NSDateFormatterShortStyle
                                                             timeStyle:NSDateFormatterShortStyle];
    }
    // Duration
    self.durationField.text = [NSString stringWithFormat:@"%d",self.unsavedDuration];
    
    [self keyboardDone:sender];
}
- (void)numberPadCancel:(id)sender
{
    int32_t tpCount = self.unsavedTpCount;
    if (tpCount == -1) {
        self.tpCountField.text = @"";
    } else {
        self.tpCountField.text = [@(tpCount) stringValue];
    }
    [self keyboardDone:sender];
}
- (void)keyboardCancel:(id)sender
{
    self.commentField.text = self.unsavedComment;
    [self keyboardDone:sender];
}
- (void)keyboardDone:(id)sender
{
    // Save within the edit context
    self.unsavedDate = [self.dateFormatter dateFromString:self.dateField.text];
    if ([self.endDateField.text length] == 0) {
        self.unsavedEndDate = nil;
    } else {
        self.unsavedEndDate = [self.dateFormatter dateFromString:self.endDateField.text];
    }

    if ([self.durationField.text length] == 0) {
        self.unsavedDuration = -1;
    } else {
        self.unsavedDuration = [self.durationField.text intValue];
    }
    
    if([self.tpCountField.text length] == 0) {
        self.unsavedTpCount = -1;
    } else {
        self.unsavedTpCount = [self.tpCountField.text intValue];
    }
    self.unsavedComment = self.commentField.text;
    
    [self.view endEditing:YES];
}

- (NSDateFormatter *)dateFormatter
{
    if(!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [_dateFormatter setDateStyle:NSDateFormatterShortStyle]; 
    }
    return _dateFormatter;
}

/*
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, 1600);
}*/


// Create a UIToolbar with a cancelSelector and doneSelector
// CANCEL --- FLEX --- DONE
- (UIToolbar *)toolbarWithCancelSelector:(SEL)cancelSelector
                         andDoneSelector:(SEL)doneSelector
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIBarButtonItem *cancelBBI = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                  target:self
                                  action:cancelSelector];
    UIBarButtonItem *flexBBI = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                target:self
                                action:nil];
    UIBarButtonItem *doneBBI = [[UIBarButtonItem alloc]
                                initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                target:self
                                action:doneSelector];
    
    NSArray *barItems = @[cancelBBI, flexBBI, doneBBI];
    [toolbar setItems:barItems animated:YES];
    
    return toolbar;
}

- (void)typeControlChanged:(id)sender
{
    SZDPoopAttributes *pa = [SZDPoopAttributes sharedInstance];
    self.typeDescription.text = [pa forSegmentedIndex:self.typeControl.selectedSegmentIndex getObjectType:SZDAttributeObjectTypeDescription forAttribute:SZDPoopAttributesType];
}

- (int)calculateDurationFrom:(NSDate *)date to:(NSDate *)endDate
{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *components = [c components:NSMinuteCalendarUnit
                                        fromDate:date toDate:endDate options:0];
    NSInteger diff = components.minute;
    return (int)diff;
}

@end
