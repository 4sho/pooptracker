//
//  SZDPoopEntry.h
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SZDPoopEntry : NSObject


typedef NS_ENUM(NSInteger, PoopType)
{
    PoopTypeLog = 0,
    PoopTypePellet,
    PoopTypeStringBean,
    PoopTypeDiarrhea
    
};
@property (nonatomic, strong) NSDictionary *poopTypeDictionary;


//@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) NSDate *date;
//@property (nonatomic, strong) NSDate *endDate;
//@property (nonatomic) NSInteger *duration;

@property (nonatomic) NSInteger type;
//@property (nonatomic, strong) UIColor *color;
//@property (nonatomic, strong) NSNumber *size;
//@property (nonatomic, strong) NSString *feel;

@property (nonatomic, strong) NSString *comments;



@end
