//
//  SZDPoopEntry.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopEntry.h"

@implementation SZDPoopEntry

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        self.date = [NSDate date]; 
        self.type = 2;

        self.comments = @"no comment.";
 
    }
    
    return self; 
}

- (NSDictionary *)poopTypeDictionary
{
    if(!_poopTypeDictionary) {
        //_poopTypeDictionary = [[NSDictionary alloc] init];
        
        _poopTypeDictionary = @{ [NSNumber numberWithInt:0] : @"Log",
                                 [NSNumber numberWithInt:1] : @"Pellets",
                                 [NSNumber numberWithInt:2] : @"String Beans",
                                 [NSNumber numberWithInt:3] : @"Diarrhea" };
    }
    
    
    return _poopTypeDictionary;
}

@end
