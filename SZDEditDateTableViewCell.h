//
//  SZDEditDateTableViewCell.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-14.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZDEditDateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end
