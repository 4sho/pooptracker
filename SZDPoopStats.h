//
//  SZDPoopStats.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-04.
//  Copyright © 2016 sandzapps. All rights reserved.
//


#import <Foundation/Foundation.h>
/*
 // Total # of poops: Total # of poops | # | since: date
 NSInteger totalNumberOfPoopsInt = [[[SZDPoopStore sharedStore] allEntries] count];
 NSString totalNumberOfPoops = [NSString stringWithFormat:@"%lu", (long)totalNumberOfPoopsInt];
 
 // TYPE: Most pooped type | image | X% of the time
 NSString mostPoopedType =
 // COLOUR: Most pooped colour | coloured text | X % of the time
 // TYPE/COLOR Combination: Most pooped poop | image | X% of the time
 // EXECUTION: Execution scale: | # | X% of the time
 // AMOUNT: Amount scale: | # | X% of the time
 // Average TP Count: Average TP | # | across X poops
 // Average # of poops: Average # of poops | # | day/week/month/year
 
 NSArray *statsArray = @{
 @[@"Total Number of Poops", totalNumberOfPoops, "Since: date"],
 
 };
 
 */
@interface SZDPoopStats : NSObject

typedef NS_ENUM(NSInteger, SZDStatsCell) {
    SZDStatsCellDaysSinceLastPoop = 0,
    SZDStatsCellTotalNumber,
    SZDStatsCellAveragePerDay,
    SZDStatsCellAverageDaysBetween,
    SZDStatsCellTotalTP,
    SZDStatsCellAverageTP,
    SZDStatsCellMostCommonType,
    SZDStatsCellMostCommonColour,
    SZDStatsCellMostCommonExertion,
    SZDStatsCellMostCommonAmount,
    SZDStatsCellMostCommonSmell,
    SZDStatsCellMostCommonFart,
    SZDStatsCellMostCommonPartOfDay, 
    SZDStatsCellCount
};

+ (instancetype)sharedStats;

+ (NSArray *)statsDataForKey:(NSInteger)cellIndex; 
- (NSArray *)attributeCountsFor:(NSInteger)attribute;
- (NSInteger)totalNumberOfEntries;
- (void)refreshStatsDictionary;



@end
