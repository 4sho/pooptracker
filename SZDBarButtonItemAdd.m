//
//  SZDBarButtonItemAdd.m
//  PoopTracker
//
//  Created by Sandy House on 2016-04-06.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDBarButtonItemAdd.h"
#import "SZDPoopLogViewController.h"
#import "SZDPoopEntry.h"
#import "SZDPoopStore.h"
#import "SZDPoopEditViewController.h"

@interface SZDBarButtonItemAdd ()

@property (nonatomic, strong) SZDPoopLogViewController *logVC;
@property (nonatomic, strong) UIViewController *viewController;

@end

@implementation SZDBarButtonItemAdd


- (instancetype)initWithViewController:(UIViewController *)viewController
                     logViewController:(SZDPoopLogViewController *)logVC
{
    self = [super initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                       target:self
                                       action:@selector(addNewEntry:)];
    if (self) {
        self.viewController = viewController;
        self.logVC = logVC;
    }
    
    return self;
}

- (void)addNewEntry:(id)sender
{
    SZDPoopEntry *newEntry = [[SZDPoopStore sharedStore] createEntry];
    
    // Have Edit Controller pop up
    SZDPoopEditViewController *newEntryViewController = [[SZDPoopEditViewController alloc]
                                                         initWithEntry:newEntry
                                                         isNewEntry:YES];
    newEntryViewController.dismissBlock = ^{
        //[self.logVC setUpTable:[[SZDPoopStore sharedStore] allEntries]];
        [self.logVC.tableView reloadData];
    };
    
    UINavigationController *entryNavController = [[UINavigationController alloc]
                                                  initWithRootViewController:newEntryViewController];
    [self.viewController presentViewController:entryNavController animated:YES completion:nil];
}


@end
