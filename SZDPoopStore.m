//
//  SZDPoopStore.m
//  PoopTracker
//
//  Created by Sandy House on 2016-02-11.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import "SZDPoopStore.h"
#import "SZDPoopEntry.h"
#import "UIKit/UIKit.h"

#import "SZDPoopAttributes.h"
#import "SZDPoopStats.h"

@import CoreData;

@interface SZDPoopStore ()

@property (nonatomic) NSMutableArray *privateAllEntries;
//@property (nonatomic) NSMutableArray *tpCounts;

@property (nonatomic) NSArray *allCounts;

@property (nonatomic) NSMutableArray *typeCounts;
@property (nonatomic) NSMutableArray *colourCounts;
@property (nonatomic) NSMutableArray *amountCounts;
@property (nonatomic) NSMutableArray *exertionCounts;
@property (nonatomic) NSMutableArray *smellCounts;
@property (nonatomic) NSMutableArray *fartCounts;


@property (nonatomic) NSInteger validTPCounts;

@property (nonatomic, strong) NSMutableDictionary *allStats;

@property (nonatomic) NSInteger totalTPCount; 
@property (nonatomic) NSInteger validTPEntryCount;
@property (nonatomic) NSInteger minTimeBetweenPoops;
@property (nonatomic) NSInteger maxTimeBetweenPoops;


@property (nonatomic, strong) NSMutableArray *allPoopTypes;
@property (nonatomic, strong) NSMutableArray *allPoopColours;
@property (nonatomic, strong) NSMutableArray *allPoopExertions;
@property (nonatomic, strong) NSMutableArray *allPoopAmounts;
@property (nonatomic, strong) NSMutableArray *allPoopSmells;
@property (nonatomic, strong) NSMutableArray *allPoopFarts;

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSManagedObjectModel *model;

@end

@implementation SZDPoopStore

@synthesize allCounts = _allCounts;
@synthesize allStats = _allStats;
@synthesize privateAllEntries = _privateAllEntries;
@synthesize model = _model;
@synthesize context = _context;

- (NSArray *)allCounts
{
    if (!_allCounts) {
        NSArray *allCounts = @[_typeCounts, _colourCounts,
                               _amountCounts, _exertionCounts,
                               _smellCounts, _fartCounts];
        _allCounts = allCounts;
    }
    return _allCounts;
}

@synthesize tpCountAverage = _tpCountAverage;
@synthesize validTPCounts = _validTPCounts;

@synthesize totalTPCount = _totalTPCount;
@synthesize validTPEntryCount = _validTPEntryCount;
@synthesize minTimeBetweenPoops = _minTimeBetweenPoops;
@synthesize maxTimeBetweenPoops = _maxTimeBetweenPoops;

@synthesize typeCounts = _typeCounts;
@synthesize colourCounts = _colourCounts;
@synthesize exertionCounts = _exertionCounts;
@synthesize amountCounts = _amountCounts;
@synthesize smellCounts = _smellCounts;
@synthesize fartCounts = _fartCounts;

@synthesize allPoopTypes = _allPoopTypes;
@synthesize allPoopColours = _allPoopColours;
@synthesize allPoopExertions = _allPoopExertions;
@synthesize allPoopAmounts = _allPoopAmounts;
@synthesize allPoopSmells = _allPoopSmells;
@synthesize allPoopFarts = _allPoopFarts;

// This returns the singleton instance of privateAllEntries
+ (instancetype)sharedStore
{
    // Declared here, but initialized once
    static SZDPoopStore *sharedStore = nil;
    
    // Initialize the sharedStore once thread-safely.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStore = [[self alloc] initPrivate];
    });
    
    return sharedStore;
}

// Make this designated initializer unable to use.
- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[SZDPoopStore]sharedStore!!!"
                                 userInfo:nil];
    return nil;
}

// Our private initializer that will be called ONCE
- (instancetype)initPrivate
{
    
    self = [super init];
    
    if (self) {
        //self.privateAllEntries = [[NSMutableArray alloc] init];
        NSLog(@"Loading singleton instance of privateAllEntries");
        
        // Read in PoopTracker.xcdatamodeld
        _model = [NSManagedObjectModel mergedModelFromBundles:nil];
        
        // Set up the NSManagedObjectContext and NSPersistentStoreCoordinator
        NSPersistentStoreCoordinator *persistentStoreCoordinator =
        [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
        // Location of SQLite file
        NSString *path = self.coreDataPath;
        NSURL *storeURL = [NSURL fileURLWithPath:path];
        
        // For Light-weight Data Model Migration
        NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @YES,
                                   NSInferMappingModelAutomaticallyOption : @YES };
        
        NSError *error = nil;
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                      configuration:nil
                                                                URL:storeURL
                                                            options:options
                                                              error:&error]) {
            @throw [NSException exceptionWithName:@"OpenFailure"
                                           reason:[error localizedDescription]
                                         userInfo:nil];
        }
        
        _context = [[NSManagedObjectContext alloc] init];
        _context.persistentStoreCoordinator = persistentStoreCoordinator;
        NSLog(@"Context: %@ %@", _context, persistentStoreCoordinator);

        
        
        _typeCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesType]];
        _colourCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesColour]];
        _exertionCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesExertion]];
        _amountCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesAmount]];
        _smellCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesSmell]];
        _fartCounts = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:SZDPoopAttributesFart]];
        
        //_tpCountAverage = -1;
        
        _allStats = [NSKeyedUnarchiver unarchiveObjectWithFile:[self attributeCountArchivePath:1337]];
        
        [self loadAllEntries];
    }
    
    return self;
}


// Getter: allEntries
- (NSArray *)allEntries
{
    return self.privateAllEntries; // NSArray
}

- (SZDPoopEntry *)createEntryWithDate:(NSDate *)date endDate:(NSDate *)endDate
{
    NSLog(@"Creating a poop entry in ManagedObjectContext: %@ %@", self.context, self.context.persistentStoreCoordinator);
    SZDPoopEntry *entry = [NSEntityDescription insertNewObjectForEntityForName:@"SZDPoopEntry"
                                                        inManagedObjectContext:self.context]; // core data
    [self.privateAllEntries insertObject:entry atIndex:0];
    NSLog(@"Added to privateAllEntries");
    
    int32_t duration = 0;
    if (endDate) {
        duration = [self calculateDurationFrom:date to:endDate];
    }
    
    [self updateEntry:entry
           entryState:SZDEntryStateNew
                 date:date
              endDate:endDate
             duration:duration
              tpCount:entry.tpCount
             comments:entry.comments
           attributes:@[entry.type, entry.colour, entry.exertion,
                        entry.amount, entry.smell, entry.fart]];
    
    return entry;
}

- (SZDPoopEntry *)createEntry
{
    return [self createEntryWithDate:[NSDate date] endDate:nil];
}

- (void)removeEntry:(SZDPoopEntry *)entry
{
    NSLog(@"Removing a poop entry from both ManagedObjectContext and memory");
    
    [self updateEntry:entry
           entryState:SZDEntryStateDestroy
                 date:entry.date
              endDate:entry.endDate
             duration:0
              tpCount:0
             comments:entry.comments
           attributes:nil];
    
    [self.privateAllEntries removeObjectIdenticalTo:entry];
    [self.context deleteObject:entry]; // core data
    [self updateOldestNewestEntryDates];
    
    
}


- (void)reInsertEntry:(SZDPoopEntry *)entry
{
    SZDPoopEntry *myEntry = entry;
    
    // check if it needs to be reinserted first
    
    [self.privateAllEntries removeObjectIdenticalTo:entry]; // temporarily remove
    
    // change this to loop and find max/min
    
    NSComparator comparator = ^NSComparisonResult(SZDPoopEntry *entry1, SZDPoopEntry *entry2) {
        NSLog(@"Is %@ less than %@", entry1.date, entry2.date);
        // if entry1.date is smaller, NSOrderedAscending
        // if entry1.date is bigger, NSOrderedDescending
        NSComparisonResult result = [entry1.date compare:entry2.date];
        
        // Sort in reverse order
        if(result == NSOrderedAscending) {
            result = NSOrderedDescending;
        } else if(result == NSOrderedDescending) {
            result = NSOrderedAscending;
        }
        
        return result;
    };
    NSUInteger newIndex = [self.privateAllEntries indexOfObject:entry
                                                  inSortedRange:(NSRange){0, [self.privateAllEntries count]}
                                                        options:NSBinarySearchingInsertionIndex
                                                usingComparator:comparator];
    NSLog(@"Inserting at %lu", (unsigned long)newIndex);
    [self.privateAllEntries insertObject:myEntry atIndex:newIndex];
}



- (NSString *)coreDataPath
{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *documentDirectory = [documentDirectories firstObject]; // There will only be one
    
    return [documentDirectory stringByAppendingPathComponent:@"PoopStore.data"];
}

- (NSString *)attributeCountArchivePath:(NSInteger)attribute
{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *documentDirectory = [documentDirectories firstObject]; // There will only be one
    
    
    return [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"attributeCount%ld.archive", (long)attribute]];
}

- (BOOL)saveChanges
{
    NSLog(@"Saving changes to ManagedObjectContext");
    // Save changes to Core Data
    NSError *error;
    BOOL successful = [self.context save:&error];
    if (!successful) {
        NSLog(@"Error saving: %@", [error localizedDescription]);
    }
    
    [NSKeyedArchiver archiveRootObject:self.typeCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesType]];
    [NSKeyedArchiver archiveRootObject:self.colourCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesColour]];
    [NSKeyedArchiver archiveRootObject:self.exertionCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesExertion]];
    [NSKeyedArchiver archiveRootObject:self.amountCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesAmount]];
    [NSKeyedArchiver archiveRootObject:self.smellCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesSmell]];
    [NSKeyedArchiver archiveRootObject:self.fartCounts toFile:[self attributeCountArchivePath:SZDPoopAttributesFart]];

    [NSKeyedArchiver archiveRootObject:self.allStats toFile:[self attributeCountArchivePath:1337]];
    
    return successful;
}

- (void)loadAllEntries
{
    if (!_privateAllEntries) {
        NSLog(@"Fetching from ManagedObjectContext");
        // Fetch all SZDPoopEntrys from NSManagedObjectContext
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *poopEntryEntity = [NSEntityDescription entityForName:@"SZDPoopEntry"
                                                             inManagedObjectContext:self.context];
        request.entity = poopEntryEntity;
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date"
                                                                         ascending:NO];
        request.sortDescriptors = @[sortDescriptor];
        
        NSError *error;
        NSArray *result = [self.context executeFetchRequest:request error:&error];
        if (!result) {
            [NSException raise:@"Fetch failed"
                        format:@"Reason: %@", [error localizedDescription]];
        }
        
        _privateAllEntries = [[NSMutableArray alloc] initWithArray:result];
        
    }
}

/***********************************************************************
 *  CoreData fetching and counting
 ***********************************************************************/
- (NSMutableArray *)fetchAllForEntity:(NSString *)entity
                            context:(NSManagedObjectContext *)context
                  sortDescriptors:(NSArray *)sortDescriptors
                            predicate:(NSPredicate *)predicate
{
    //NSLog(@"Fetching array for entity: %@", entity);
    NSFetchRequest *request = [self setUpRequestForEntity:entity inContext:context sortDescriptors:sortDescriptors predicate:predicate];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:request error:&error];
    if (!result) {
        [NSException raise:@"Fetch failed" format:@"Reason: %@", [error localizedDescription]];
    }
    return [result mutableCopy];
}

- (NSUInteger)countAllForEntity:(NSString *)entity
                        context:(NSManagedObjectContext *)context
                sortDescriptors:(NSArray *)sortDescriptors
                      predicate:(NSPredicate *)predicate
{
    //NSLog(@"Fetching count for entity: %@", entity);
    NSFetchRequest *request = [self setUpRequestForEntity:entity inContext:context sortDescriptors:sortDescriptors predicate:predicate];
    NSError *error = nil;
    NSUInteger result = [context countForFetchRequest:request error:&error];
    return result;
}

- (NSFetchRequest *)setUpRequestForEntity:(NSString *)entity
                                inContext:(NSManagedObjectContext *)context
                          sortDescriptors:(NSArray *)sortDescriptors
                                predicate:(NSPredicate *)predicate
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:entity inManagedObjectContext:context];
    request.sortDescriptors = sortDescriptors;
    request.predicate = predicate;
    return request;
}

/***********************************************************************
 *  Methods for getting all the attribute types of an attribute
 ***********************************************************************/

// Getting all the attribute types - for general use
- (NSArray *)allTypesForAttribute:(NSInteger)attribute
{
    NSString *entityName = [[SZDPoopAttributes sharedInstance] objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectEntity forAttribute:attribute];
    NSMutableArray *allTypes;
    if (attribute == SZDPoopAttributesType) {
        allTypes = _allPoopTypes;
    } else if (attribute == SZDPoopAttributesColour) {
        allTypes = _allPoopColours;
    }else if (attribute == SZDPoopAttributesExertion) {
        allTypes = _allPoopExertions;
    } else if (attribute == SZDPoopAttributesAmount) {
        allTypes = _allPoopAmounts;
    } else if (attribute == SZDPoopAttributesSmell) {
        allTypes = _allPoopSmells;
    } else if (attribute == SZDPoopAttributesFart) {
        allTypes = _allPoopFarts;
    }
    
    // the first time on app's run, we re-setup all attribute types
    // this is so if anything changes to the attribute types, it will take into effect
    // like changing the icon images
    if (!allTypes) {
        allTypes = [self allPoopAttributeTypesFor:attribute
                                       entityName:entityName];
        if (attribute == SZDPoopAttributesType) {
            _allPoopTypes = allTypes;
        } else if (attribute == SZDPoopAttributesColour) {
            _allPoopColours = allTypes;
        } else if (attribute == SZDPoopAttributesExertion) {
            _allPoopExertions = allTypes;
        } else if (attribute == SZDPoopAttributesAmount) {
            _allPoopAmounts = allTypes;
        } else if (attribute == SZDPoopAttributesSmell) {
            _allPoopSmells = allTypes;
        } else if (attribute == SZDPoopAttributesFart) {
            _allPoopFarts = allTypes;
        }
    }
    return allTypes;
}

// Fetching all the attribute types from ManagedObjectContext
// And updating it with the current dictionary values
- (NSMutableArray *)allPoopAttributeTypesFor:(NSInteger)attribute
                                  entityName:(NSString *)entityName
{
    //NSLog(@"Initializing attribute types %@.", entityName);
    // first fetch the objects
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    NSMutableArray *attributeArray = [self fetchAllForEntity:entityName
                                                     context:self.context
                                             sortDescriptors:@[sd]
                                                   predicate:nil];
    // then update them
    NSDictionary *dictionary = [[SZDPoopAttributes sharedInstance] dictionaryForAttribute:attribute];
    for (int i = 0; i < [dictionary count]; i++) {
        NSArray *array = [dictionary objectForKey:[NSNumber numberWithInt:i]];
        [self manageAttributeTypeWithAttributeArray:attributeArray
                                             entity:entityName
                             inManagedObjectContext:self.context
                                              index:i
                                         valueArray:array];
    }
    return attributeArray;
}

// Update or add the individual attribute type
- (void)manageAttributeTypeWithAttributeArray:(NSMutableArray *)attributeArray
                                   entity:(NSString *)entity
                   inManagedObjectContext:(NSManagedObjectContext *)context
                                    index:(int32_t)index
                                   valueArray:(NSArray *)valueArray
{
    // find the object with the index if it exists
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"index = %d", index];
    NSArray *filteredArray = [attributeArray filteredArrayUsingPredicate:predicate];
    
    NSManagedObject *attributeType = [filteredArray firstObject];
    if (attributeType) {
        //NSLog(@"Updating object with index: %d", index);
    } else {
        //NSLog(@"Creating object with index: %d", index);
        attributeType = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
    }
    
    [attributeType setValue:[NSNumber numberWithInt:index] forKey:@"index"];
    [attributeType setValue:valueArray[SZDAttributeObjectTypeTitle] forKey:@"title"];
    [attributeType setValue:valueArray[SZDAttributeObjectTypeImage] forKey:@"image"];
    [attributeType setValue:valueArray[SZDAttributeObjectTypeColour] forKey:@"colour"];
    [attributeType setValue:valueArray[SZDAttributeObjectTypeDescription] forKey:@"desc"];
    
    if ([filteredArray count] == 0) {   // did not previously exist before
        [attributeArray addObject:attributeType];
    }
}



/***********************************************************************
 *  Methods for STATS DATA
 ***********************************************************************/


// Attribute Counts: Individual arrays for each attribute to count how many attribute types we have
// We only do a CoreData fetch once throughout the entire application existence
// Then we update the counts when adding/deleting/editing entries for efficiency
// The arrays are archived
- (NSMutableArray *)attributeCount:(NSInteger)attribute
{
    NSMutableArray *countArray;
    NSString *typeName = [[SZDPoopAttributes sharedInstance] objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectKey forAttribute:attribute];
    if(attribute == SZDPoopAttributesType) {
        countArray = _typeCounts;
    } else if (attribute == SZDPoopAttributesColour) {
        countArray = _colourCounts;
    } else if (attribute == SZDPoopAttributesAmount) {
        countArray = _amountCounts;
    } else if (attribute  == SZDPoopAttributesExertion) {
        countArray = _exertionCounts;
    } else if (attribute  == SZDPoopAttributesSmell) {
        countArray = _smellCounts;
    } else if (attribute  == SZDPoopAttributesFart) {
        countArray = _fartCounts;
    }
    
    if (!countArray) {
        //NSLog(@"Initializing array for attribute: %@", typeName);
        countArray = [[NSMutableArray alloc] init];
        
        NSDictionary *dictionary = [[SZDPoopAttributes sharedInstance] dictionaryForAttribute:attribute];
        for (int i = 0; i < [dictionary count]; i++) {
            NSString *pString = [NSString stringWithFormat:@"%@.index = %d", typeName, i];
            NSPredicate *p = [NSPredicate predicateWithFormat:pString];
            NSUInteger result = [self countAllForEntity:@"SZDPoopEntry"
                                                context:self.context
                                        sortDescriptors:nil
                                              predicate:p];
            [countArray insertObject:@(result) atIndex:i];
        }
        
        if (attribute == SZDPoopAttributesType) {
            _typeCounts = countArray;
        } else if (attribute  == SZDPoopAttributesColour) {
            _colourCounts = countArray;
        } else if (attribute == SZDPoopAttributesAmount) {
            _amountCounts = countArray;
        } else if (attribute  == SZDPoopAttributesExertion) {
            _exertionCounts = countArray;
        } else if (attribute  == SZDPoopAttributesSmell) {
            _smellCounts = countArray;
        } else if (attribute  == SZDPoopAttributesFart) {
            _fartCounts = countArray;
        }
    }
    
    return countArray;
}

- (void)refreshStats
{
    _allStats = nil;
    self.allStats; 
}

- (NSMutableDictionary *)allStats
{
    if (!_allStats) {
        _allStats = [self initializeStats];
    }
    return _allStats;
}

// keep track of all time in minutes
// avg tp
// total tp
// shortest time without pooping
// longest time without pooping
// most often pooped time of day -- needs array

- (NSMutableDictionary *)initializeStats
{
    NSLog(@"initializeStats");
    NSMutableDictionary *allStats = [[NSMutableDictionary alloc] init];
    
    NSInteger totalTPCount = 0;
    NSInteger validTPEntryCount = 0;
    NSInteger minutesBetweenPoops = 0;
    NSInteger minTimeBetweenPoops = -1;
    NSInteger maxTimeBetweenPoops = -1;
    NSMutableDictionary *partsOfDay = [[NSMutableDictionary alloc] init];
    
    // Loop through all entries beginning with the newest entry
    // last entry is: self.allEntries - 1
    for (int i = 0; i < [self.allEntries count]; i++) {
        SZDPoopEntry *entry = [self.allEntries objectAtIndex:i];
        
        // tpCount
        if (entry.tpCount > 0) {
            totalTPCount += entry.tpCount;
            ++validTPEntryCount;
        }
        
        // minutesBetweenPoops - if there is < 2 entries, values will be -1
        if (i < [self.allEntries count] - 1) {    // if not last one
            SZDPoopEntry *nextEntry = [self.allEntries objectAtIndex:(i + 1)];  // chronologically previous
            NSDate *date1 = [nextEntry date];
            NSDate *date2 = [entry date];
            
            minutesBetweenPoops = [self timeBetweenDate1:date1 andDate2:date2];
            
            if (i == 0) {   // first one and there is a second
                minTimeBetweenPoops = minutesBetweenPoops;
                maxTimeBetweenPoops = minTimeBetweenPoops;
            }
            minTimeBetweenPoops = MIN(minutesBetweenPoops, minTimeBetweenPoops);
            maxTimeBetweenPoops = MAX(minutesBetweenPoops, maxTimeBetweenPoops);
        }
        
        // partOfDay
        // check what part of day the date is, then increment the count
        NSString *partOfDayKey = [self partOfDay:entry.date];
        NSNumber *oldValue = [partsOfDay objectForKey:partOfDayKey];
        if (!oldValue) {
            [partsOfDay setObject:@0 forKey:partOfDayKey];
        }
        NSNumber *newValue = [NSNumber numberWithInteger:[[partsOfDay objectForKey:partOfDayKey] integerValue] + 1] ;
        [partsOfDay setObject:newValue forKey:partOfDayKey];        
    }
    
    [allStats setObject:@([self.allEntries count]) forKey:[NSNumber numberWithInteger:SZDStatTotalNumberOfEntries]];
    if ([self.allEntries count] > 0) {
        [allStats setObject:[[self.allEntries lastObject] date] forKey:[NSNumber numberWithInteger:SZDStatOldestEntryDate]];
        [allStats setObject:[[self.allEntries firstObject] date] forKey:[NSNumber numberWithInteger:SZDStatNewestEntryDate]];
    }
    [allStats setObject:@(totalTPCount) forKey:[NSNumber numberWithInteger:SZDStatTotalTPCount]];
    [allStats setObject:@(validTPEntryCount) forKey:[NSNumber numberWithInteger:SZDStatValidTPEntryCount]];
    [allStats setObject:@(minTimeBetweenPoops) forKey:[NSNumber numberWithInteger:SZDStatMinTimeBetween]];
    [allStats setObject:@(maxTimeBetweenPoops) forKey:[NSNumber numberWithInteger:SZDStatMaxTimeBetween]];
    [allStats setObject:partsOfDay forKey:[NSNumber numberWithInteger:SZDStatPartsOfDay]];

    
    return allStats;
    //NSLog(@"totalTPCount: %ld\n validTPEntryCount: %ld\n minTimebetweenPoops: %lu\n maxTimebwtweenPoops: %lu\n partsOfDay: %@", _totalTPCount, _validTPEntryCount, _minTimeBetweenPoops, _maxTimeBetweenPoops, partsOfDay);
}

- (id)statForKey:(NSInteger)key
{
    id stat = [self.allStats objectForKey:[NSNumber numberWithInteger:key]];
    return stat;
}

- (NSString *)partOfDay:(NSDate *)date
{
    // 0:00 - 5:59 - Early Morning
    // 6:00 - 11:59 - Morning
    // 12:00 - 5:59 - Afternoon
    // 6:00 - 11:59 - Evening
    NSString *partOfDay = @"Timeless";
    
    NSUInteger units = NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date];
    if (components.hour >= 0 && components.hour < 6) {
        partOfDay = @"Early Morning";
    } else if (components.hour >= 6 && components.hour < 12) {
        partOfDay = @"Morning";
    } else if (components.hour >= 12 && components.hour < 18) {
        partOfDay = @"Afternoon";
    } else if (components.hour >= 18 && components.hour <= 24) {
        partOfDay = @"Evening";
    }

    return partOfDay;
}

- (NSInteger)timeBetweenDate1:(NSDate *)date1 andDate2:(NSDate *)date2
{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *components = [c components:NSMinuteCalendarUnit fromDate:date1 toDate:date2 options:0];
    NSInteger diff = components.minute;
    return diff;
}

// if deleting, must be called after deletion
- (void)updateOldestNewestEntryDates
{
    if ([self.allEntries count] > 0) {
        [self.allStats setObject:[[self.allEntries lastObject] date] forKey:@(SZDStatOldestEntryDate)];
        [self.allStats setObject:[[self.allEntries firstObject] date] forKey:@(SZDStatNewestEntryDate)];
    } else {
        [self.allStats removeObjectForKey:@(SZDStatOldestEntryDate)];
        [self.allStats removeObjectForKey:@(SZDStatNewestEntryDate)];
    }
}

- (void)updateEntry:(SZDPoopEntry *)entry
         entryState:(NSInteger)entryState
               date:(NSDate *)date
            endDate:(NSDate *)endDate
           duration:(int32_t)duration
            tpCount:(int32_t)tpCount
           comments:(NSString *)comments
         attributes:(NSArray *)attributes
{
    // if date changes, reinsert
    if (entry.date != date) {
        entry.date = date;
        [self reInsertEntry:entry];
    } else if (entryState == SZDEntryStateNew) {
        [self reInsertEntry:entry];
    }
    if (entry.endDate != endDate) {
        entry.endDate = endDate;
        entry.duration = duration;
    }
    
    // Set these every time
    NSInteger totalNumberOfEntries = [[self.allStats objectForKey:@(SZDStatTotalNumberOfEntries)] integerValue];
    if(entryState == SZDEntryStateDestroy) {
        --totalNumberOfEntries;
    }
    if (entryState == SZDEntryStateNew) {
        ++totalNumberOfEntries;
    }
    [self.allStats setObject:@(totalNumberOfEntries) forKey:@(SZDStatTotalNumberOfEntries)];
    
    [self updateOldestNewestEntryDates];
    
    // PART OF DAY
    if (entry.date != date || entryState == SZDEntryStateDestroy || entryState == SZDEntryStateNew) {
        NSMutableDictionary *partsOfDay = [self.allStats objectForKey:@(SZDStatPartsOfDay)];
        NSString *oldPartOfDay = [self partOfDay:entry.date];
        NSString *newPartOfDay = [self partOfDay:date];
        //NSNumber *oldPartOfDayCount = [partsOfDay objectForKey:oldPartOfDay];
        NSNumber *newPartOfDayCount = [partsOfDay objectForKey:newPartOfDay];
        if (!newPartOfDayCount) {  // no count previously
            newPartOfDayCount = @(0);
        }
        
        // update partOfDay counts
        if (entryState != SZDEntryStateNew) {   // decrement
            NSNumber *decrement = @([[partsOfDay objectForKey:oldPartOfDay] integerValue] - 1);
            [partsOfDay setObject:decrement forKey:oldPartOfDay];
        }
        if (entryState != SZDEntryStateDestroy) {   // increment
            NSNumber *increment = @([[partsOfDay objectForKey:newPartOfDay] integerValue] + 1) ;
            [partsOfDay setObject:increment forKey:newPartOfDay];
        }
    }

    
    if (entry.tpCount != tpCount) {
        NSInteger totalTPCount = [[self.allStats objectForKey:@(SZDStatTotalTPCount)] integerValue];
        NSInteger validTPEntryCount = [[self.allStats objectForKey:@(SZDStatValidTPEntryCount)] integerValue];
        if (entry.tpCount == 0) {   // old tpCount was not valid or entry is new
            validTPEntryCount++;
        }
        if (tpCount == 0) {         // new tpCount is not valid
            validTPEntryCount--;
        }
        totalTPCount = totalTPCount - entry.tpCount + tpCount;
        [self.allStats setObject:@(totalTPCount) forKey:@(SZDStatTotalTPCount)];
        [self.allStats setObject:@(validTPEntryCount) forKey:@(SZDStatValidTPEntryCount)];
        
        entry.tpCount = tpCount;
    }

    entry.comments = comments;
    
    // ATTRIBUTE UPDATE
    SZDPoopAttributes *pa = [SZDPoopAttributes sharedInstance];
    for (NSInteger i = 0; i < SZDPoopAttributesCount; i++) {
        NSString *key = [pa objectInAttributeDictionaryAtIndex:SZDPoopAttributeDictionaryObjectKey forAttribute:i];
        NSInteger oldIndex = [[[entry valueForKey:key] valueForKey:@"index"] integerValue];
        NSMutableArray *countArray = [self attributeCount:i];
        
        if (entryState != SZDEntryStateDestroy) {
            NSArray *allTypes = [self allTypesForAttribute:i];
            NSInteger newIndex = [allTypes indexOfObject:attributes[i]];
            
            // update entry
            if (oldIndex != newIndex) {
                [entry setValue:[allTypes objectAtIndex:newIndex] forKey:key];
            }
            // update count
            countArray[newIndex] = @([countArray[newIndex] integerValue] + 1);
        }
        
        if (entryState != SZDEntryStateNew) {   // decrement
            countArray[oldIndex] = @([countArray[oldIndex] integerValue] - 1);
        }
    }
    
    // refresh the stats
    [[SZDPoopStats sharedStats] refreshStatsDictionary]; 
}


// improve this:
// can do this the first time, and then whenever we edit an entry,
// it will remove from dictionary and then add to the appropriate keyday.
// if there is no keyday, it will create one
// if there is a keyday, it will add to the array in the appropriate spot
// a small update to an array with 0-2 entries is better than redoing this whole thing every single entry edit
- (NSDictionary *)allEntriesByDate
{
    NSMutableDictionary *allEntriesByDate = [[NSMutableDictionary alloc] init];
    NSDate *keyDay;
    for (SZDPoopEntry *entry in self.privateAllEntries) {
        NSDate *day = [self dateAtBeginningOfDayForDate:entry.date];
        
        if (!keyDay) {  // first entry of the day
            keyDay = day;
            NSMutableArray *array = [[NSMutableArray alloc] init];
            [array insertObject:entry atIndex:0];
            [allEntriesByDate setObject:array forKey:keyDay];
            continue;
        }
        
        if ([keyDay isEqual:day]) {   // if they are on the same day
            [[allEntriesByDate objectForKey:keyDay] insertObject:entry atIndex:0];  // insert to get ascending order
        }
        else {
            keyDay = day;
            NSMutableArray *array = [[NSMutableArray alloc] init];
            [array insertObject:entry atIndex:0];
            [allEntriesByDate setObject:array forKey:keyDay];
        }
    }
    return allEntriesByDate;
}


- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:inputDate];

    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (int)calculateDurationFrom:(NSDate *)date to:(NSDate *)endDate
{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDateComponents *components = [c components:NSMinuteCalendarUnit
                                        fromDate:date toDate:endDate options:0];
    NSInteger diff = components.minute;
    return (int)diff;
}

@end
