//
//  ThemeManager.h
//  PoopTracker
//
//  Created by Sandy House on 2016-03-23.
//  Copyright © 2016 sandzapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface ThemeManager : NSObject



+ (ThemeManager *)sharedManager;
+ (UIColor *)themeColorNamed:(NSString *)key;
+ (UIColor *)colorFromHexString:(NSString *)hexString; 
+ (void)setTheme:(NSString *)themeName;
+ (NSString *)getTheme;
+ (UIImage *)iconNamed:(NSString *)key; 
+ (void)setIconTheme:(NSString *)iconThemeName; 


@property (nonatomic, strong) NSDictionary *styles; 
@property (nonatomic, strong) NSDictionary *icons; 

@end
